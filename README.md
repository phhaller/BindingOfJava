# Binding of Java




## Description
Binding of Java is a 2D top-down RPG-like game that has its focus on exploring, fighting, building, and collecting stuff. 
With each new game world being based on Perlin Noise, and therefore unique, you'll never know where to find those gems, chests, and bears lurking behind trees.
So make sure to have the right tools and weapons available!

(Hint: This game is still in development and nowhere near finished!)


## Usage
The project can be executed by running the class Launcher located in the launcher package or by running the executable jar file located at the /build/libs folder.

Once you successfully started it you can:


**MAIN MENU**: 
- New Game: create a new world
- Load Game: load a previously saved world (TO DO)
- Help: have a look at the controls and goals of the game (TO DO)
- Quit: exit the game


**GAME**:
- Controls
  - **W, A, S, D**: Move around
  - **E**: pick up item, open chest, or interest with NPC
  - **I**: open inventory
  - **O**: open equipment
  - **B**: open building window / disable building mode
  - **SPACE**: use tool, sword, or bow
  - **ESC**: open menu, and close open window
  - **MOUSE CLICK**: place building while in building mode

- Inventory:
  - Have a look at all your valuables
  - Craft items based on what you have (WILL BE MOVED TO INDIVIDUAL MACHINES ONCE AVAILABLE)

- Equipment window:
  - Check and select your tools and weapons, and their stats

- Building window: (IN PROGRESS)
  - Select building type or machine to build
  - Once selected:
    - Click with the mouse on the ground and the building will be placed. A green rectangle means placeable, red means not placeable

- NPC:
  - Buy or sell items, tools, or weapons based on NPC type

- Enemies:
  - All animals can be considered "enemies", their movement is not finalized. The bear is currently the only one attacking you, all other ones run away once you've hurt them.
  - Kill by using a tool, sword, or bow. Or get killed by colliding with them.

- Vegetation:
  - Everything (except for grass and flowers at the moment) can be destroyed and collected.
    - Pickaxe: Stone, ores
    - Axe: Trees, bushes
    - Shovel: Shrooms
      
  

## Roadmap
There is still way too much to do, to list everything here..


## Installation
The project is set up to run on Java 11 or later in a MacOS or Windows environment.
To change the used Java version simply edit the corresponding line (sourceCompatibility) in the build.gradle file.
To run the project on a Linux environment, simply add the missing dependencies in the build.gradle file (You can copy and paste the "mac" or "win" compiles and change mac or win to linux).
