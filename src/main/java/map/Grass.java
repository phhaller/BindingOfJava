package map;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

import java.util.Random;

public class Grass extends Vegetation {

  public Grass(int x, int y, GameWindowController gwc) {

    super(gwc);

    worldX = x * tileSize;
    worldY = y * tileSize;
    width = tileSize;
    height = tileSize;
    type = "GRASS";
    life = 5;
    maxLife = life;

    createGrass();

  }


  private void createGrass() {

    rec.setWidth(width);
    rec.setHeight(height);
    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);

    Random r = new Random();
    int random = r.nextInt(16);

    image = gwc.imageManager.vegetationMap.get("GRASS").get(random);
    rec.setFill(new ImagePattern(image));

  }

}
