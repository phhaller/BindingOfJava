package map;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

import java.util.Random;

public class Flower extends Vegetation {

  public Flower(int x, int y, GameWindowController gwc) {

    super(gwc);

    worldX = x * tileSize + 4;
    worldY = y * tileSize + 4;
    width = 40;
    height = 40;
    type = "FLOWER";
    life = 5;
    maxLife = life;

    createFlower();

  }


  private void createFlower() {

    rec.setWidth(width);
    rec.setHeight(height);
    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);

    Random r = new Random();
    int random = r.nextInt(5);

    image = gwc.imageManager.vegetationMap.get("FLOWER").get(random);
    rec.setFill(new ImagePattern(image));

  }

}
