package map;

public class Tile {

  private int x;
  private int y;
  private int size;
  private float noiseHeight;
  private boolean occupied = false;

  // Used for pathfinding
  private Tile parent;
  private double gCost;
  private double hCost;
  private int pathX;
  private int pathY;


  public Tile(int x, int y, int size, float noiseHeight) {

    this.x = x;
    this.y = y;
    this.size = size;
    this.noiseHeight = noiseHeight;

  }


  // GETTERS AND SETTERS
  public int getX() {
    return x/size;
  }
  public int getY() {
    return y/size;
  }
  public int getSize() {
    return size;
  }
  public float getNoiseHeight() {
    return noiseHeight;
  }
  public boolean getOccupied() {
    return occupied;
  }
  public Tile getParent() {
    return parent;
  }
  public double getgCost() {
    return gCost;
  }
  public double gethCost() {
    return hCost;
  }
  public double getfCost() {
    return gCost + hCost;
  }
  public int getPathX() {
    return pathX/size;
  }
  public int getPathY() {
    return pathY/size;
  }

  public void setX(int worldX) {
    x = worldX;
  }
  public void setY(int worldY) {
    y = worldY;
  }
  public void setSize(int size) {
    this.size = size;
  }
  public void setNoiseHeight(float noiseHeight) {
    this.noiseHeight = noiseHeight;
  }
  public void setOccupied(boolean b) {
    occupied = b;
  }
  public void setParent(Tile parent) {
    this.parent = parent;
  }
  public void setgCost(double gCost) {
    this.gCost = gCost;
  }
  public void sethCost(double hCost) {
    this.hCost = hCost;
  }
  public void setPathX(int pathX) {
    this.pathX = pathX;
  }
  public void setPathY(int pathY) {
    this.pathY = pathY;
  }

  public void clear() {
    parent = null;
    gCost = 0;
    hCost = 0;
  }

}
