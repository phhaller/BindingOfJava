package map;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

import java.util.Random;

public class Shroom extends Vegetation {

  private int variant;

  public Shroom(int x, int y, GameWindowController gwc) {

    super(gwc);

    worldX = x * tileSize;
    worldY = y * tileSize;
    width = tileSize;
    height = tileSize;
    type = "SHROOM";
    life = 5;
    variant = 1;
    maxLife = life;

    createShroom();

  }


  private void createShroom() {

    rec.setWidth(width);
    rec.setHeight(height);
    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);

    Random r = new Random();
    int random = r.nextInt(2);

    switch (random) {
      case 0:
        image = gwc.imageManager.vegetationMap.get("SHROOM").get(0);
        name = "SHROOMRED";
        break;
      case 1:
        image = gwc.imageManager.vegetationMap.get("SHROOM").get(1);
        name = "SHROOMBROWN";
        break;
    }

    rec.setFill(new ImagePattern(image));

  }

}
