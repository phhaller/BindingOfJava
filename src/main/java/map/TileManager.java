package map;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import util.NoiseManager;
import view.GameWindowController;

public class TileManager {

  private GameWindowController gwc;
  private int worldWidth;
  private int worldHeight;
  private int screenWidth;
  private int screenHeight;
  private int tileSize;

  private Image[] images;
  private Canvas canvas;
  private GraphicsContext gc;

  private Tile[][] tiles;


  public TileManager(GameWindowController gwc, Tile[][] tiles) {

    this.gwc = gwc;
    worldWidth = gwc.worldWidth;
    worldHeight = gwc.worldHeight;
    screenWidth = gwc.screenWidth;
    screenHeight = gwc.screenHeight;
    tileSize = gwc.tileSize;

    canvas = new Canvas(screenWidth, screenHeight);
    gc = canvas.getGraphicsContext2D();

    loadImages();

    if (tiles == null) {
      createTiles();
    } else {
      this.tiles = tiles;
    }

  }


  private void loadImages() {

    images = new Image[4];
    images[0] = new Image("/images/ground/ground1.png");
    images[1] = new Image("/images/ground/ground2.png");
    images[2] = new Image("/images/ground/ground3.png");
    images[3] = new Image("/images/ground/ground4.png");

  }


  private void createTiles() {

    tiles = new Tile[worldWidth/tileSize][worldHeight/tileSize];
    float[][] noiseMap = NoiseManager.createNoiseMap(worldWidth, worldHeight, tileSize);

    for (int y = 0; y < worldHeight/tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        Tile tile = null;

        if (noiseMap[x][y] < .3) {
          tile = new Tile(x * tileSize, y * tileSize, tileSize, noiseMap[x][y]);
        } else if (noiseMap[x][y] < .4) {
          tile = new Tile(x * tileSize, y * tileSize, tileSize, noiseMap[x][y]);
        } else if (noiseMap[x][y] < .6) {
          tile = new Tile(x * tileSize, y * tileSize, tileSize, noiseMap[x][y]);
        } else {
          tile = new Tile(x * tileSize, y * tileSize, tileSize, noiseMap[x][y]);
        }

        tiles[x][y] = tile;

      }
    }

  }


  public void draw() {

    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

    for (int y = 0; y < worldHeight / tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        Tile tile = tiles[x][y];

        int worldX = x * tileSize;
        int worldY = y * tileSize;
        int screenX = worldX - gwc.player.worldX + gwc.player.screenX;
        int screenY = worldY - gwc.player.worldY + gwc.player.screenY;

        if (worldX + tileSize > gwc.player.worldX - gwc.player.screenX &&
            worldX - tileSize * 2 < gwc.player.worldX + gwc.player.screenX &&
            worldY + tileSize > gwc.player.worldY - gwc.player.screenY &&
            worldY - tileSize * 2 < gwc.player.worldY + gwc.player.screenY) {

          if (tile.getNoiseHeight() < .3) {
            gc.drawImage(images[0], screenX, screenY);
          } else if (tile.getNoiseHeight() < .4) {
            gc.drawImage(images[1], screenX, screenY);
          } else if (tile.getNoiseHeight() < .6) {
            gc.drawImage(images[2], screenX, screenY);
          } else {
            gc.drawImage(images[3], screenX, screenY);
          }

        }

      }
    }

  }


  public Canvas getCanvas() {
    return canvas;
  }

  public GraphicsContext getGraphicContext() {
    return gc;
  }

  public Tile[][] getTiles() {
    return tiles;
  }

}
