package map;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

import java.util.Random;

public class Rock extends Vegetation {

  public Rock(int x, int y, GameWindowController gwc) {

    super(gwc);

    worldX = x * tileSize;
    worldY = y * tileSize;
    type = "ROCK";
    hasIngot = false;

    createRock();

  }


  private void createRock() {

    Random r = new Random();
    int random = r.nextInt(8 - 1) + 1;

    switch (random) {
      case 1:
        width = tileSize * 3;
        height = tileSize * 2;
        int r1 = r.nextInt(100 - 1) + 1;
        if (r1 <= 80) {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(0);
        } else if (r1 <= 90) {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(7);
          name = "ROCKIRON";
          hasIngot = true;
        } else {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(11);
          name = "ROCKGOLD";
          hasIngot = true;
        }
        life = 25;
        maxLife = life;
        break;
      case 2:
        width = tileSize * 2;
        height = tileSize * 2;
        int r2 = r.nextInt(100 - 1) + 1;
        if (r2 <= 80) {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(1);
        } else if (r2 <= 90) {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(8);
          name = "ROCKIRON";
          hasIngot = true;
        } else {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(12);
          name = "ROCKGOLD";
          hasIngot = true;
        }
        life = 20;
        maxLife = life;
        break;
      case 3:
        width = tileSize * 2;
        height = tileSize;
        int r3 = r.nextInt(100 - 1) + 1;
        if (r3 <= 80) {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(2);
        } else if (r3 <= 90) {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(9);
          name = "ROCKIRON";
          hasIngot = true;
        } else {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(13);
          name = "ROCKGOLD";
          hasIngot = true;
        }
        life = 20;
        maxLife = life;
        break;
      case 4:
        width = tileSize * 2;
        height = tileSize;
        image = gwc.imageManager.vegetationMap.get("ROCK").get(3);
        life = 15;
        maxLife = life;
        break;
      case 5:
        width = tileSize * 2;
        height = tileSize;
        image = gwc.imageManager.vegetationMap.get("ROCK").get(4);
        life = 15;
        maxLife = life;
        break;
      case 6:
        width = tileSize;
        height = tileSize;
        int r4 = r.nextInt(100 - 1) + 1;
        if (r4 <= 80) {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(5);
        } else if (r4 <= 90) {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(10);
          name = "ROCKIRON";
          hasIngot = true;
        } else {
          image = gwc.imageManager.vegetationMap.get("ROCK").get(14);
          name = "ROCKGOLD";
          hasIngot = true;
        }
        life = 10;
        maxLife = life;
        break;
      case 7:
        width = tileSize;
        height = tileSize;
        image = gwc.imageManager.vegetationMap.get("ROCK").get(6);
        life = 5;
        maxLife = life;
        break;
    }

    rec.setWidth(width);
    rec.setHeight(height);
    rec.setTranslateX(worldX - (width - tileSize) / 2);
    rec.setTranslateY(worldY - (height - tileSize));
    rec.setFill(new ImagePattern(image));

  }

}
