package map;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

import java.util.Random;

public class Tree extends Vegetation {

  public Tree(int x, int y, GameWindowController gwc) {

    super(gwc);

    worldX = x * tileSize;
    worldY = y * tileSize;
    width = tileSize * 2;
    height = tileSize * 3;
    type = "TREE";
    life = 15;
    maxLife = life;

    createTree();

  }


  private void createTree() {

    rec.setWidth(width);
    rec.setHeight(height);
    rec.setTranslateX(worldX - (width - tileSize) / 2);
    rec.setTranslateY(worldY - tileSize * 2);

    Random r = new Random();
    int random = r.nextInt(3);

    image = gwc.imageManager.vegetationMap.get("TREE").get(random);
    rec.setFill(new ImagePattern(image));

  }

}
