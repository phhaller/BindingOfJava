package map;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import view.GameWindowController;

public class Vegetation {

  public GameWindowController gwc;
  public int tileSize;
  public int worldX, worldY;
  public int width, height;
  public String type = "";
  public String name = "";
  public Rectangle rec;
  public Image image;
  public int life;
  public int maxLife;

  // ROCK
  public boolean hasIngot;

  // BUSH
  public boolean hasBerry;



  public Vegetation(GameWindowController gwc) {
    this.gwc = gwc;
    this.tileSize = gwc.tileSize;
    rec = new Rectangle(tileSize, tileSize);
  }

}
