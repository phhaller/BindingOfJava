package map;

import javafx.scene.shape.Rectangle;
import util.NoiseManager;
import view.GameWindowController;

import java.util.ArrayList;

public class VegetationManager {

  private GameWindowController gwc;
  private int worldWidth;
  private int worldHeight;
  private int tileSize;
  private Tile[][] tiles;
  private Vegetation[][] vegetation;
  private ArrayList<Rectangle> vegetationRecs;


  public VegetationManager(GameWindowController gwc, Tile[][] tiles) {

    this.gwc = gwc;
    this.tiles = tiles;
    worldWidth = gwc.worldWidth;
    worldHeight = gwc.worldHeight;
    tileSize = gwc.tileSize;
    vegetation = new Vegetation[worldWidth / tileSize][worldHeight / tileSize];
    vegetationRecs = new ArrayList<>();

    createTrees();
    createBushes();
    createRocks();
    createShrooms();
    createFlowers();
    createGrass();

  }


  private void createTrees() {

    float[][] noiseMap = NoiseManager.createNoiseMap(worldWidth, worldHeight, tileSize);

    int treeCounter = 0;
    for (int y = 0; y < worldHeight / tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        if (tiles[x][y].getNoiseHeight() >= .6 && noiseMap[x][y] > .6) {

          tiles[x][y].setOccupied(true);
          Vegetation tree = new Tree(x, y, gwc);
          vegetation[x][y] = tree;
          vegetationRecs.add(tree.rec);
          treeCounter++;

        }

      }
    }

  }


  private void createBushes() {

    float[][] noiseMap = NoiseManager.createNoiseMap(worldWidth, worldHeight, tileSize);

    int bushCounter = 0;
    for (int y = 0; y < worldHeight / tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        if (tiles[x][y].getNoiseHeight() < .6 && tiles[x][y].getNoiseHeight() >= .55 && noiseMap[x][y] > .6) {

          tiles[x][y].setOccupied(true);
          Vegetation bush = new Bush(x, y, gwc);
          vegetation[x][y] = bush;
          vegetationRecs.add(bush.rec);
          bushCounter++;

        }

      }
    }

  }


  private void createRocks() {

    float[][] noiseMap = NoiseManager.createNoiseMap(worldWidth, worldHeight, tileSize);

    int rockCounter = 0;
    for (int y = 0; y < worldHeight / tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        if (tiles[x][y].getNoiseHeight() < .3 && noiseMap[x][y] > .75) {

          tiles[x][y].setOccupied(true);
          Vegetation rock = new Rock(x, y, gwc);
          vegetation[x][y] = rock;
          vegetationRecs.add(rock.rec);
          rockCounter++;

        }

      }
    }

  }


  private void createShrooms() {

    float[][] noiseMap = NoiseManager.createNoiseMap(worldWidth, worldHeight, tileSize);

    int shroomCounter = 0;
    for (int y = 0; y < worldHeight / tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        if (tiles[x][y].getNoiseHeight() < .85 && tiles[x][y].getNoiseHeight() >= .8 && noiseMap[x][y] < .3  && vegetation[x][y] == null) {

          // tiles[x][y].setOccupied(true);
          Vegetation shroom = new Shroom(x, y, gwc);
          vegetation[x][y] = shroom;
          vegetationRecs.add(shroom.rec);
          shroomCounter++;

        }

      }
    }

  }


  private void createFlowers() {

    float[][] noiseMap = NoiseManager.createNoiseMap(worldWidth, worldHeight, tileSize);

    int flowerCounter = 0;
    for (int y = 0; y < worldHeight / tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        if (tiles[x][y].getNoiseHeight() < .2 && noiseMap[x][y] < .2  && vegetation[x][y] == null) {

          // tiles[x][y].setOccupied(true);
          Vegetation flower = new Flower(x, y, gwc);
          vegetation[x][y] = flower;
          vegetationRecs.add(flower.rec);
          flowerCounter++;

        }

      }
    }

  }


  private void createGrass() {

    float[][] noiseMap = NoiseManager.createNoiseMap(worldWidth, worldHeight, tileSize);

    int grassCounter = 0;
    for (int y = 0; y < worldHeight / tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        if ((noiseMap[x][y] < .1 || noiseMap[x][y] > .9) && vegetation[x][y] == null) {

          // tiles[x][y].setOccupied(true);
          Vegetation grass = new Grass(x, y, gwc);
          vegetation[x][y] = grass;
          vegetationRecs.add(grass.rec);
          grassCounter++;

        }

      }
    }

  }


  public void draw() {

    gwc.getBackground().getChildren().removeAll(vegetationRecs);

    for (int y = 0; y < worldHeight / tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        Vegetation v = vegetation[x][y];

        int worldX = x * tileSize;
        int worldY = y * tileSize;
        int screenX = worldX - gwc.player.worldX + gwc.player.screenX;
        int screenY = worldY - gwc.player.worldY + gwc.player.screenY;

        if (worldX + tileSize > gwc.player.worldX - gwc.player.screenX &&
            worldX - tileSize * 2 < gwc.player.worldX + gwc.player.screenX &&
            worldY + tileSize > gwc.player.worldY - gwc.player.screenY &&
            worldY - tileSize * 2 < gwc.player.worldY + gwc.player.screenY) {



          if (v != null) {
            if (v.type.equals("BUSH")) {
              v.rec.setTranslateX(screenX - (v.width - tileSize) / 2);
              v.rec.setTranslateY(screenY);
              gwc.getBackground().getChildren().add(v.rec);
            }

            if (v.type.equals("ROCK")) {
              v.rec.setTranslateX(screenX - (v.width - tileSize) / 2);
              v.rec.setTranslateY(screenY - (v.height - tileSize));
              gwc.getBackground().getChildren().add(v.rec);
            }

            if (v.type.equals("FLOWER")) {
              v.rec.setTranslateX(screenX + 4);
              v.rec.setTranslateY(screenY + 4);
              gwc.getBackground().getChildren().add(v.rec);
            }

            if (v.type.equals("SHROOM") || v.type.equals("GRASS")) {
              v.rec.setTranslateX(screenX);
              v.rec.setTranslateY(screenY);
              gwc.getBackground().getChildren().add(v.rec);
            }
          }

        }

      }
    }

  }


  // Draw trees after the rest of the vegetation so they can be placed a layer above the entity
  public void drawTress() {

    for (int y = 0; y < worldHeight / tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        Vegetation v = vegetation[x][y];

        int worldX = x * tileSize;
        int worldY = y * tileSize;
        int screenX = worldX - gwc.player.worldX + gwc.player.screenX;
        int screenY = worldY - gwc.player.worldY + gwc.player.screenY;

        if (worldX + tileSize > gwc.player.worldX - gwc.player.screenX &&
            worldX - tileSize * 2 < gwc.player.worldX + gwc.player.screenX &&
            worldY + tileSize > gwc.player.worldY - gwc.player.screenY &&
            worldY - tileSize * 3 < gwc.player.worldY + gwc.player.screenY) {

          if (v != null && v.type.equals("TREE")) {
            v.rec.setTranslateX(screenX - (v.width - tileSize) / 2);
            v.rec.setTranslateY(screenY - tileSize * 2);
            gwc.getBackground().getChildren().add(v.rec);
          }

        }

      }
    }

  }


  public void removeVegetation(int x, int y) {
    vegetationRecs.remove(vegetation[x][y].rec);
    gwc.getBackground().getChildren().remove(vegetation[x][y].rec);
    vegetation[x][y] = null;
  }


  public Vegetation[][] getVegetation() {
    return vegetation;
  }

}
