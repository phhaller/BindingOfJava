package map;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

import java.util.Random;

public class Bush extends Vegetation {

  public Bush(int x, int y, GameWindowController gwc) {

    super(gwc);

    worldX = x * tileSize;
    worldY = y * tileSize;
    width = tileSize * 2;
    height = tileSize;
    type = "BUSH";
    life = 10;
    maxLife = life;
    hasBerry = false;

    createBush();

  }


  private void createBush() {

    rec.setWidth(width);
    rec.setHeight(height);
    rec.setTranslateX(worldX - (width - tileSize) / 2);
    rec.setTranslateY(worldY);

    Random r = new Random();
    int random = r.nextInt(117 - 1) + 1;

    if (random <= 100) {
      int tmp = r.nextInt(5 - 1) + 1;
      switch (tmp) {
        case 1:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(0);
          break;
        case 2:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(1);
          break;
        case 3:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(2);
          break;
        case 4:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(3);
          break;
      }
    }

    if (random > 100 && random <= 110) {
      int tmp = r.nextInt(4 - 1) + 1;
      switch (tmp) {
        case 1:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(4);
          name = "BUSHRED";
          hasBerry = true;
          break;
        case 2:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(5);
          name = "BUSHRED";
          hasBerry = true;
          break;
        case 3:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(6);
          name = "BUSHRED";
          hasBerry = true;
          break;
      }
    }

    if (random > 110 && random <= 112) {
      int tmp = r.nextInt(4 - 1) + 1;
      switch (tmp) {
        case 1:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(7);
          name = "BUSHPURPLE";
          hasBerry = true;
          break;
        case 2:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(8);
          name = "BUSHPURPLE";
          hasBerry = true;
          break;
        case 3:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(9);
          name = "BUSHPURPLE";
          hasBerry = true;
          break;
      }
    }

    if (random > 112 && random <= 114) {
      int tmp = r.nextInt(4 - 1) + 1;
      switch (tmp) {
        case 1:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(10);
          name = "BUSHBLUE";
          hasBerry = true;
          break;
        case 2:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(11);
          name = "BUSHBLUE";
          hasBerry = true;
          break;
        case 3:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(12);
          name = "BUSHBLUE";
          hasBerry = true;
          break;
      }
    }

    if (random > 114 && random <= 116) {
      int tmp = r.nextInt(4 - 1) + 1;
      switch (tmp) {
        case 1:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(13);
          name = "BUSHYELLOW";
          hasBerry = true;
          break;
        case 2:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(14);
          name = "BUSHYELLOW";
          hasBerry = true;
          break;
        case 3:
          image = gwc.imageManager.vegetationMap.get("BUSH").get(15);
          name = "BUSHYELLOW";
          hasBerry = true;
          break;
      }
    }

    rec.setFill(new ImagePattern(image));

  }

}
