package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import map.Tile;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONManager {

  private String os = System.getProperty("os.name");
  private String userName = System.getProperty("user.name");
  private String path;

  private Tile[][] tiles;


  public JSONManager() {

    if (os.startsWith("Mac")) {
      path = "/Users/" + userName + "/Documents/BindingOfJava/Worlds";
      boolean f = new File(path).mkdirs();
    }

    if (os.startsWith("Windows")) {
      path = "C:\\Users\\" + userName + "\\Documents\\BindingOfJava\\Worlds";
      boolean f = new File(path).mkdirs();
    }

  }



  public void createJson(String fileName, Tile[][] tiles) {

    /*JSONObject parentObject = new JSONObject();

    JSONArray tilesArray = new JSONArray();
    for (int y = 0; y < tiles[0].length; y++) {
      JSONArray objectArray = new JSONArray();
      for (int x = 0; x < tiles.length; x++) {
        JSONObject object = new JSONObject(tiles[x][y]);
        objectArray.put(object);
        // objectArray.put(tiles[x][y]);
      }
      tilesArray.put(objectArray);
    }

    parentObject.put("tiles", tilesArray);
    createFile(fileName, parentObject.toString());*/

    try {

      GsonBuilder builder = new GsonBuilder();
      builder.setPrettyPrinting();

      Gson gson = builder.create();
      Writer writer = Files.newBufferedWriter(Paths.get(path + "/" + fileName + ".json"));
      Map<String, ArrayList> map = new HashMap<>();
      ArrayList<Tile> tmpList = new ArrayList<>();

      for (int y = 0; y < tiles[0].length; y++) {
        for (int x = 0; x < tiles.length; x++) {
          tmpList.add(tiles[x][y]);
        }
      }

      map.put("tiles", tmpList);
      gson.toJson(map, writer);

      writer.close();
      System.out.println("SAVED");

    } catch (Exception e) {
      e.printStackTrace();
    }

  }


  private void createFile(String fileName, String json) {

    try {

      FileWriter fw = new FileWriter(path + "/" + fileName + ".json");
      fw.write(json);
      fw.flush();
      fw.close();

      System.out.println("SAVED");

    } catch (IOException e) {
      e.printStackTrace();
    }

  }


  public void loadFile(String filePath, int cols, int rows) {

    tiles = new Tile[cols][rows];

    try {

      System.out.println("LOADING FILE");
      GsonBuilder builder = new GsonBuilder();
      Gson gson = builder.create();
      BufferedReader br = new BufferedReader(new FileReader(filePath));

      Type mapType = new TypeToken<Map<String, List>>(){}.getType();
      Map<String, List> map = gson.fromJson(br, mapType);

      List tiles = map.get("tiles");



      System.out.println((Tile) tiles.get(2));

    } catch (Exception e) {
      e.printStackTrace();
    }

  }





  public Tile[][] getTiles() {
    return tiles;
  }

}
