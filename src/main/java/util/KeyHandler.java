package util;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import view.GameWindowController;

public class KeyHandler {

  private GameWindowController gwc;
  public boolean W = false;
  public boolean A = false;
  public boolean S = false;
  public boolean D = false;
  public boolean E = false;


  public KeyHandler(GameWindowController gwc) {

    this.gwc = gwc;
    createHandlers();

  }


  private void createHandlers() {

    gwc.scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        // PLAY STATE
        if (gwc.gameState == gwc.playState) {

          switch (event.getCode()) {
            case W:
              W = true;
              break;
            case A:
              A = true;
              break;
            case S:
              S = true;
              break;
            case D:
              D = true;
              break;
            case E:
              E = true;
              break;
            case B:
              if (gwc.player.building) {
                gwc.player.building = false;
                gwc.buildingManager.placeable = false; // Used in case placeable is true when exiting build mode
              } else {
                gwc.gameState = gwc.buildingState;
              }
              break;
            case I:
              gwc.gameState = gwc.inventoryState;
              gwc.uiManager.inventoryWindow.update();
              gwc.buildingManager.placeable = false;
              gwc.player.building = false;
              break;
            case O:
              gwc.gameState = gwc.equipmentState;
              gwc.uiManager.equipmentWindow.update();
              gwc.buildingManager.placeable = false;
              gwc.player.building = false;
              break;
            case SPACE:
              gwc.player.createAction();
              break;
            case DIGIT1:
              if (!gwc.player.usingTool && !gwc.player.usingSword && !gwc.player.usingBow) {
                gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentTool);
              }
              break;
            case DIGIT2:
              if (!gwc.player.usingTool && !gwc.player.usingSword && !gwc.player.usingBow) {
                gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentSword);
              }
              break;
            case DIGIT3:
              if (!gwc.player.usingTool && !gwc.player.usingSword && !gwc.player.usingBow) {
                gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentBow);
              }
              break;
            case DIGIT4:
              if (!gwc.player.usingTool && !gwc.player.usingSword && !gwc.player.usingBow) {
                gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentArmour);
              }
              break;
            case ESCAPE:
              if (!gwc.player.usingTool && !gwc.player.usingSword && !gwc.player.usingBow) {
                gwc.gameState = gwc.pauseState;
                gwc.buildingManager.placeable = false;
                gwc.player.building = false;
              }
              break;
          }

        }

        // PAUSE STATE
        if (gwc.gameState == gwc.pauseState) {

        }

        // DIALOG STATE
        if (gwc.gameState == gwc.dialogState) {
          if (event.getCode() == KeyCode.ESCAPE) {
            gwc.uiManager.dialogWindow.dialogManager.buying = true;
            gwc.uiManager.dialogWindow.dialogManager.selling = false;
            gwc.gameState = gwc.playState;
          }
        }

        // EQUIPMENT STATE
        if (gwc.gameState == gwc.equipmentState) {
          if (event.getCode() == KeyCode.ESCAPE) {
            gwc.gameState = gwc.playState;
          }
        }

        // INVENTORY STATE
        if (gwc.gameState == gwc.inventoryState) {
          if (event.getCode() == KeyCode.ESCAPE) {
            gwc.gameState = gwc.playState;
          }
        }

        // CRAFTING STATE
        if (gwc.gameState == gwc.craftingState) {
          if (event.getCode() == KeyCode.ESCAPE) {
            gwc.gameState = gwc.playState;
          }
        }

        // BUILDING STATE
        if (gwc.gameState == gwc.buildingState) {
          if (event.getCode() == KeyCode.ESCAPE) {
            gwc.gameState = gwc.playState;
          }
        }


      }
    });


    gwc.scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        switch (event.getCode()) {
          case W:
            W = false;
            break;
          case A:
            A = false;
            break;
          case S:
            S = false;
            break;
          case D:
            D = false;
            break;
          case E:
            E = false;
            break;
        }

      }
    });

  }


}
