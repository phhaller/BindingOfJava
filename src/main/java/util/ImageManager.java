package util;

import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.HashMap;

public class ImageManager {

  public HashMap<String, ArrayList<Image>> vegetationMap;
  public HashMap<String, ArrayList<Image>> toolMap;
  public HashMap<String, ArrayList<Image>> swordMap;
  public HashMap<String, ArrayList<Image>> bowMap;
  public HashMap<String, ArrayList<Image>> projectileMap;
  public HashMap<String, ArrayList<Image>> containerMap;
  public HashMap<String, ArrayList<Image>> itemMap;
  public HashMap<String, ArrayList<Image>> inventoryMap;
  public HashMap<String, ArrayList<Image>> wallMap;
  public HashMap<String, ArrayList<Image>> machineMap;


  public ImageManager() {

    createVegetationImages();
    createToolImages();
    createSwordImages();
    createBowImages();
    createProjectileImages();
    createContainers();
    createItemImages();
    createInventoryImages();
    createWallImages();
    createMachineImages();

  }


  private void createVegetationImages() {

    vegetationMap = new HashMap<>();

    // TREES
    ArrayList<Image> treeImgs = new ArrayList<>();
    treeImgs.add(new Image("/images/vegetation/tree1.png"));
    treeImgs.add(new Image("/images/vegetation/tree2.png"));
    treeImgs.add(new Image("/images/vegetation/tree3.png"));
    vegetationMap.put("TREE", treeImgs);

    // BUSHES
    ArrayList<Image> bushImgs = new ArrayList<>();
    bushImgs.add(new Image("/images/vegetation/bush1.png"));
    bushImgs.add(new Image("/images/vegetation/bush2.png"));
    bushImgs.add(new Image("/images/vegetation/bush3.png"));
    bushImgs.add(new Image("/images/vegetation/bush4.png"));
    bushImgs.add(new Image("/images/vegetation/bush5.png"));
    bushImgs.add(new Image("/images/vegetation/bush6.png"));
    bushImgs.add(new Image("/images/vegetation/bush7.png"));
    bushImgs.add(new Image("/images/vegetation/bush8.png"));
    bushImgs.add(new Image("/images/vegetation/bush9.png"));
    bushImgs.add(new Image("/images/vegetation/bush10.png"));
    bushImgs.add(new Image("/images/vegetation/bush11.png"));
    bushImgs.add(new Image("/images/vegetation/bush12.png"));
    bushImgs.add(new Image("/images/vegetation/bush13.png"));
    bushImgs.add(new Image("/images/vegetation/bush14.png"));
    bushImgs.add(new Image("/images/vegetation/bush15.png"));
    bushImgs.add(new Image("/images/vegetation/bush16.png"));
    vegetationMap.put("BUSH", bushImgs);

    // GRASS
    ArrayList<Image> grassImgs = new ArrayList<>();
    grassImgs.add(new Image("/images/vegetation/grass1.png"));
    grassImgs.add(new Image("/images/vegetation/grass2.png"));
    grassImgs.add(new Image("/images/vegetation/grass3.png"));
    grassImgs.add(new Image("/images/vegetation/grass4.png"));
    grassImgs.add(new Image("/images/vegetation/grass5.png"));
    grassImgs.add(new Image("/images/vegetation/grass6.png"));
    grassImgs.add(new Image("/images/vegetation/grass7.png"));
    grassImgs.add(new Image("/images/vegetation/grass8.png"));
    grassImgs.add(new Image("/images/vegetation/grass9.png"));
    grassImgs.add(new Image("/images/vegetation/grass10.png"));
    grassImgs.add(new Image("/images/vegetation/grass11.png"));
    grassImgs.add(new Image("/images/vegetation/grass12.png"));
    grassImgs.add(new Image("/images/vegetation/grass13.png"));
    grassImgs.add(new Image("/images/vegetation/grass14.png"));
    grassImgs.add(new Image("/images/vegetation/grass15.png"));
    grassImgs.add(new Image("/images/vegetation/grass16.png"));
    vegetationMap.put("GRASS", grassImgs);

    // FLOWERS
    ArrayList<Image> flowerImgs = new ArrayList<>();
    flowerImgs.add(new Image("/images/vegetation/flower1.png"));
    flowerImgs.add(new Image("/images/vegetation/flower2.png"));
    flowerImgs.add(new Image("/images/vegetation/flower3.png"));
    flowerImgs.add(new Image("/images/vegetation/flower4.png"));
    flowerImgs.add(new Image("/images/vegetation/flower5.png"));
    vegetationMap.put("FLOWER", flowerImgs);

    // SHROOMS
    ArrayList<Image> shroomImgs = new ArrayList<>();
    shroomImgs.add(new Image("/images/vegetation/shroom1.png"));
    shroomImgs.add(new Image("/images/vegetation/shroom2.png"));
    vegetationMap.put("SHROOM", shroomImgs);

    // ROCKS
    ArrayList<Image> rockImgs = new ArrayList<>();
    rockImgs.add(new Image("/images/vegetation/rock1.png"));
    rockImgs.add(new Image("/images/vegetation/rock2.png"));
    rockImgs.add(new Image("/images/vegetation/rock3.png"));
    rockImgs.add(new Image("/images/vegetation/rock4.png"));
    rockImgs.add(new Image("/images/vegetation/rock5.png"));
    rockImgs.add(new Image("/images/vegetation/rock6.png"));
    rockImgs.add(new Image("/images/vegetation/rock7.png"));
    rockImgs.add(new Image("/images/vegetation/rock101.png"));
    rockImgs.add(new Image("/images/vegetation/rock102.png"));
    rockImgs.add(new Image("/images/vegetation/rock103.png"));
    rockImgs.add(new Image("/images/vegetation/rock106.png"));
    rockImgs.add(new Image("/images/vegetation/rock201.png"));
    rockImgs.add(new Image("/images/vegetation/rock202.png"));
    rockImgs.add(new Image("/images/vegetation/rock203.png"));
    rockImgs.add(new Image("/images/vegetation/rock206.png"));
    vegetationMap.put("ROCK", rockImgs);

  }


  private void createToolImages() {

    toolMap = new HashMap<>();

    // AXE
    ArrayList<Image> axeImgs = new ArrayList<>();
    axeImgs.add(new Image("/images/equipment/axe.png"));
    axeImgs.add(new Image("/images/equipment/axeWood.png"));
    axeImgs.add(new Image("/images/equipment/axeStone.png"));
    axeImgs.add(new Image("/images/equipment/axeIron.png"));
    toolMap.put("AXE", axeImgs);

    // PICKAXE
    ArrayList<Image> pickaxeImgs = new ArrayList<>();
    pickaxeImgs.add(new Image("/images/equipment/pickaxe.png"));
    pickaxeImgs.add(new Image("/images/equipment/pickaxeWood.png"));
    pickaxeImgs.add(new Image("/images/equipment/pickaxeStone.png"));
    pickaxeImgs.add(new Image("/images/equipment/pickaxeIron.png"));
    toolMap.put("PICKAXE", pickaxeImgs);

    // SHOVEL
    ArrayList<Image> shovelImgs = new ArrayList<>();
    shovelImgs.add(new Image("/images/equipment/shovel.png"));
    shovelImgs.add(new Image("/images/equipment/shovelWood.png"));
    shovelImgs.add(new Image("/images/equipment/shovelStone.png"));
    shovelImgs.add(new Image("/images/equipment/shovelIron.png"));
    toolMap.put("SHOVEL", shovelImgs);

  }


  private void createSwordImages() {

    swordMap = new HashMap<>();

    // SWORDS
    ArrayList<Image> swordImgs = new ArrayList<>();
    swordImgs.add(new Image("/images/equipment/swordWood.png"));
    swordImgs.add(new Image("/images/equipment/swordStone.png"));
    swordImgs.add(new Image("/images/equipment/swordIron.png"));
    swordMap.put("SWORD", swordImgs);

    // SABER
    ArrayList<Image> saberImgs = new ArrayList<>();
    saberImgs.add(new Image("/images/equipment/saber.png"));
    swordMap.put("SABER", saberImgs);

    // LONGSWORD
    ArrayList<Image> longswordImgs = new ArrayList<>();
    longswordImgs.add(new Image("/images/equipment/longsword.png"));
    swordMap.put("LONGSWORD", longswordImgs);

  }


  private void createBowImages() {

    bowMap = new HashMap<>();

    // NORMALBOW
    ArrayList<Image> bowImgs = new ArrayList<>();
    bowImgs.add(new Image("/images/equipment/bow.png"));
    bowMap.put("NORMALBOW", bowImgs);

  }


  private void createProjectileImages() {

    projectileMap = new HashMap<>();

    // ARROWS
    ArrayList<Image> arrowImgs = new ArrayList<>();
    arrowImgs.add(new Image("/images/equipment/arrowFire.png"));
    arrowImgs.add(new Image("/images/equipment/arrowIce.png"));
    arrowImgs.add(new Image("/images/equipment/arrowMana.png"));
    arrowImgs.add(new Image("/images/equipment/arrowRich.png"));
    arrowImgs.add(new Image("/images/equipment/arrowRoot.png"));
    projectileMap.put("ARROW", arrowImgs);

  }


  private void createContainers() {

    containerMap = new HashMap<>();

    // CHEST
    ArrayList<Image> chestImgs = new ArrayList<>();
    chestImgs.add(new Image("/images/objects/chest_single.png"));
    chestImgs.add(new Image("/images/objects/chest_double.png"));
    containerMap.put("CHEST", chestImgs);

    // BARREL
    ArrayList<Image> barrelImgs = new ArrayList<>();
    barrelImgs.add(new Image("/images/objects/barrel_up.png"));
    barrelImgs.add(new Image("/images/objects/barrel_down.png"));
    containerMap.put("BARREL", barrelImgs);

  }


  private void createItemImages() {

    itemMap = new HashMap<>();

    // LOG
    ArrayList<Image> logImgs = new ArrayList<>();
    logImgs.add(new Image("/images/objects/log.png"));
    itemMap.put("LOG", logImgs);

    // ROCK
    ArrayList<Image> rockImgs = new ArrayList<>();
    rockImgs.add(new Image("/images/objects/rock.png"));
    itemMap.put("ROCK", rockImgs);

    // SHROOM
    ArrayList<Image> shroomImgs = new ArrayList<>();
    shroomImgs.add(new Image("/images/objects/shroomRed.png"));
    shroomImgs.add(new Image("/images/objects/shroomBrown.png"));
    itemMap.put("SHROOM", shroomImgs);

    // KEY
    ArrayList<Image> keyImgs = new ArrayList<>();
    keyImgs.add(new Image("/images/objects/key.png"));
    itemMap.put("KEY", keyImgs);

    // COIN
    ArrayList<Image> coinImgs = new ArrayList<>();
    coinImgs.add(new Image("/images/objects/coin.png"));
    itemMap.put("COIN", coinImgs);

    // BERRY
    ArrayList<Image> berryImgs = new ArrayList<>();
    berryImgs.add(new Image("/images/objects/berryRed.png"));
    berryImgs.add(new Image("/images/objects/berryPurple.png"));
    berryImgs.add(new Image("/images/objects/berryBlue.png"));
    berryImgs.add(new Image("/images/objects/berryYellow.png"));
    itemMap.put("BERRY", berryImgs);

    // POTION
    ArrayList<Image> potionImgs = new ArrayList<>();
    potionImgs.add(new Image("/images/objects/potionRed.png"));
    potionImgs.add(new Image("/images/objects/potionPurple.png"));
    potionImgs.add(new Image("/images/objects/potionBlue.png"));
    potionImgs.add(new Image("/images/objects/potionYellow.png"));
    itemMap.put("POTION", potionImgs);

    // INGOT
    ArrayList<Image> ingotImgs = new ArrayList<>();
    ingotImgs.add(new Image("/images/objects/ingotIron.png"));
    ingotImgs.add(new Image("/images/objects/ingotGold.png"));
    itemMap.put("INGOT", ingotImgs);

    // CHICKEN LEG
    ArrayList<Image> chickenLegImgs = new ArrayList<>();
    chickenLegImgs.add(new Image("/images/objects/chickenLeg_raw.png"));
    chickenLegImgs.add(new Image("/images/objects/chickenLeg_cooked.png"));
    itemMap.put("CHICKENLEG", chickenLegImgs);

    // PORK CHOP
    ArrayList<Image> porkChopImgs = new ArrayList<>();
    porkChopImgs.add(new Image("/images/objects/porkChop_raw.png"));
    porkChopImgs.add(new Image("/images/objects/porkChop_cooked.png"));
    itemMap.put("PORKCHOP", porkChopImgs);

    // RIBS
    ArrayList<Image> ribsImgs = new ArrayList<>();
    ribsImgs.add(new Image("/images/objects/ribs_raw.png"));
    ribsImgs.add(new Image("/images/objects/ribs_cooked.png"));
    itemMap.put("RIBS", ribsImgs);

    // STEAK
    ArrayList<Image> steakImgs = new ArrayList<>();
    steakImgs.add(new Image("/images/objects/steak_raw.png"));
    steakImgs.add(new Image("/images/objects/steak_cooked.png"));
    itemMap.put("STEAK", steakImgs);

    // FEATHER
    ArrayList<Image> featherImgs = new ArrayList<>();
    featherImgs.add(new Image("/images/objects/feather.png"));
    itemMap.put("FEATHER", featherImgs);

    // BONE
    ArrayList<Image> boneImgs = new ArrayList<>();
    boneImgs.add(new Image("/images/objects/bone.png"));
    itemMap.put("BONE", boneImgs);

    // ACORN
    ArrayList<Image> acornImgs = new ArrayList<>();
    acornImgs.add(new Image("/images/objects/acorn.png"));
    itemMap.put("ACORN", acornImgs);

  }


  private void createInventoryImages() {

    inventoryMap = new HashMap<>();

    // LOG
    ArrayList<Image> logImgs = new ArrayList<>();
    logImgs.add(new Image("/images/inventory/log.png"));
    inventoryMap.put("LOG", logImgs);

    // ROCK
    ArrayList<Image> rockImgs = new ArrayList<>();
    rockImgs.add(new Image("/images/inventory/rock.png"));
    inventoryMap.put("ROCK", rockImgs);

    // SHROOM
    ArrayList<Image> shroomImgs = new ArrayList<>();
    shroomImgs.add(new Image("/images/inventory/shroomRed.png"));
    shroomImgs.add(new Image("/images/inventory/shroomBrown.png"));
    inventoryMap.put("SHROOM", shroomImgs);

    // KEY
    ArrayList<Image> keyImgs = new ArrayList<>();
    keyImgs.add(new Image("/images/inventory/key.png"));
    inventoryMap.put("KEY", keyImgs);

    // COIN
    ArrayList<Image> coinImgs = new ArrayList<>();
    coinImgs.add(new Image("/images/inventory/coin.png"));
    inventoryMap.put("COIN", coinImgs);

    // BERRY
    ArrayList<Image> berryImgs = new ArrayList<>();
    berryImgs.add(new Image("/images/inventory/berryRed.png"));
    berryImgs.add(new Image("/images/inventory/berryPurple.png"));
    berryImgs.add(new Image("/images/inventory/berryBlue.png"));
    berryImgs.add(new Image("/images/inventory/berryYellow.png"));
    inventoryMap.put("BERRY", berryImgs);

    // POTION
    ArrayList<Image> potionImgs = new ArrayList<>();
    potionImgs.add(new Image("/images/inventory/potionRed.png"));
    potionImgs.add(new Image("/images/inventory/potionPurple.png"));
    potionImgs.add(new Image("/images/inventory/potionBlue.png"));
    potionImgs.add(new Image("/images/inventory/potionYellow.png"));
    inventoryMap.put("POTION", potionImgs);

    // INGOT
    ArrayList<Image> ingotImgs = new ArrayList<>();
    ingotImgs.add(new Image("/images/inventory/ingotIron.png"));
    ingotImgs.add(new Image("/images/inventory/ingotGold.png"));
    inventoryMap.put("INGOT", ingotImgs);

    // CHICKEN LEG
    ArrayList<Image> chickenLegImgs = new ArrayList<>();
    chickenLegImgs.add(new Image("/images/inventory/chickenLeg_raw.png"));
    chickenLegImgs.add(new Image("/images/inventory/chickenLeg_cooked.png"));
    inventoryMap.put("CHICKENLEG", chickenLegImgs);

    // PORK CHOP
    ArrayList<Image> porkChopImgs = new ArrayList<>();
    porkChopImgs.add(new Image("/images/inventory/porkChop_raw.png"));
    porkChopImgs.add(new Image("/images/inventory/porkChop_cooked.png"));
    inventoryMap.put("PORKCHOP", porkChopImgs);

    // RIBS
    ArrayList<Image> ribsImgs = new ArrayList<>();
    ribsImgs.add(new Image("/images/inventory/ribs_raw.png"));
    ribsImgs.add(new Image("/images/inventory/ribs_cooked.png"));
    inventoryMap.put("RIBS", ribsImgs);

    // STEAK
    ArrayList<Image> steakImgs = new ArrayList<>();
    steakImgs.add(new Image("/images/inventory/steak_raw.png"));
    steakImgs.add(new Image("/images/inventory/steak_cooked.png"));
    inventoryMap.put("STEAK", steakImgs);

    // FEATHER
    ArrayList<Image> featherImgs = new ArrayList<>();
    featherImgs.add(new Image("/images/inventory/feather.png"));
    inventoryMap.put("FEATHER", featherImgs);

    // BONE
    ArrayList<Image> boneImgs = new ArrayList<>();
    boneImgs.add(new Image("/images/inventory/bone.png"));
    inventoryMap.put("BONE", boneImgs);

    // ACORN
    ArrayList<Image> acornImgs = new ArrayList<>();
    acornImgs.add(new Image("/images/inventory/acorn.png"));
    inventoryMap.put("ACORN", acornImgs);

    // WALL
    ArrayList<Image> wallImgs = new ArrayList<>();
    wallImgs.add(new Image("/images/inventory/wall.png"));
    inventoryMap.put("WALL", wallImgs);

    // MACHINE
    ArrayList<Image> machineImgs = new ArrayList<>();
    machineImgs.add(new Image("/images/inventory/anvil.png"));
    machineImgs.add(new Image("/images/inventory/lumber.png"));
    machineImgs.add(new Image("/images/inventory/brewer.png"));
    machineImgs.add(new Image("/images/inventory/campfire.png"));
    inventoryMap.put("MACHINE", machineImgs);

  }


  private void createWallImages() {

    wallMap = new HashMap<>();

    // WOOD
    ArrayList<Image> woodWallImgs = new ArrayList<>();
    woodWallImgs.add(new Image("/images/building/wallBottomLeft.png"));
    woodWallImgs.add(new Image("/images/building/wallBottomCenter.png"));
    woodWallImgs.add(new Image("/images/building/wallBottomRight.png"));
    woodWallImgs.add(new Image("/images/building/wallSide.png"));
    woodWallImgs.add(new Image("/images/building/wallTopLeft.png"));
    woodWallImgs.add(new Image("/images/building/wallTopRight.png"));
    wallMap.put("WOOD", woodWallImgs);

  }


  private void createMachineImages() {

    machineMap = new HashMap<>();

    // ANVIL
    ArrayList<Image> anvilImgs = new ArrayList<>();
    anvilImgs.add(new Image("/images/machine/anvil.png"));
    machineMap.put("ANVIL", anvilImgs);

    // LUMBER
    ArrayList<Image> lumberImgs = new ArrayList<>();
    lumberImgs.add(new Image("/images/machine/lumber.png"));
    machineMap.put("LUMBER", lumberImgs);

    // BREWER
    ArrayList<Image> brewerImgs = new ArrayList<>();
    brewerImgs.add(new Image("/images/machine/brewer.png"));
    machineMap.put("BREWER", brewerImgs);

    // CAMPFIRE
    ArrayList<Image> campfireImgs = new ArrayList<>();
    campfireImgs.add(new Image("/images/machine/campfire.png"));
    machineMap.put("CAMPFIRE", campfireImgs);

  }

}
