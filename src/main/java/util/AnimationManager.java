package util;

import entity.Entity;
import entity.EntityAnimation;
import entity.Player;
import javafx.animation.Animation;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import objects.SuperObject;

public class AnimationManager {

  private Entity entity;

  // PLAYER
  private EntityAnimation walkAnimation;
  private EntityAnimation toolAnimation;
  private EntityAnimation swordAnimation;
  private EntityAnimation bowAnimation;

  public String sheetType = "";
  public String toolType = "";
  public int walkOffsetY = 0;
  public int offsetY = 0;


  public AnimationManager(Entity entity) {

    this.entity = entity;

    setUp();

  }


  private void setUp() {

    if (entity.type == 0) {
      createPlayerAnimations();
    }

  }


  private void createPlayerAnimations() {

    Player player = (Player) entity;

    ImageView imageView1 = new ImageView(new Image("/sprites/player/walk.png"));
    walkAnimation = new EntityAnimation(imageView1, Animation.INDEFINITE, 64, 64, 9, 550);

    ImageView imageView2 = new ImageView(new Image("/sprites/player/useTool.png"));
    toolAnimation = new EntityAnimation(imageView2, 1, 64, 64, 6, 600);
    toolAnimation.animation.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        player.gwc.getBackground().getChildren().remove(player.entityAnimation);
        player.entityAnimation = walkAnimation;
        player.usingTool = false;
        player.cutting = false;
        player.mining = false;
        player.digging = false;

        if (player.gwc.equipmentManager.current.durability <= 0) {
          player.gwc.equipmentManager.removeTool(player.gwc.equipmentManager.current);
          player.gwc.equipmentManager.setCurrentTool(null);
          player.gwc.equipmentManager.setCurrent(null);
        }
      }
    });

    ImageView imageView3 = new ImageView(new Image("/sprites/player/attackSword.png"));
    swordAnimation = new EntityAnimation(imageView3, 1, 192, 192, 6, 300);
    swordAnimation.animation.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        player.gwc.getBackground().getChildren().remove(player.entityAnimation);
        player.entityAnimation = walkAnimation;
        player.usingSword = false;

        if (player.gwc.equipmentManager.current.durability <= 0) {
          player.gwc.equipmentManager.removeSword(player.gwc.equipmentManager.current);
          player.gwc.equipmentManager.setCurrentSword(null);
          player.gwc.equipmentManager.setCurrent(null);
        }
      }
    });

    ImageView imageView4 = new ImageView(new Image("/sprites/player/attackBow.png"));
    bowAnimation = new EntityAnimation(imageView4, 1, 64, 64, 13, 600);
    bowAnimation.animation.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        player.gwc.getBackground().getChildren().remove(player.entityAnimation);
        player.entityAnimation = walkAnimation;
        player.usingBow = false;
        player.shooting = false;

        createBowAction(player);

        if (player.gwc.equipmentManager.current.durability <= 0) {
          player.gwc.equipmentManager.removeBow(player.gwc.equipmentManager.current);
          player.gwc.equipmentManager.setCurrentBow(null);
          player.gwc.equipmentManager.setCurrent(null);
        }
      }
    });

  }


  private void createBowAction(Player player) {

    SuperObject current = player.gwc.equipmentManager.current;

    if (current.name.equals("NORMALBOW")) {
      player.gwc.projectileManager.createProjectile("ARROW", player.worldX, player.worldY, player.direction,
          player.damage);
    }

  }


  public void update(String sheetType, String toolType) {

    this.sheetType = sheetType;
    this.toolType = toolType;

    if (entity.type == 0) {

      Player player = (Player) entity;

      switch (sheetType) {

        case "WALK":
          switch (toolType) {
            case "BASIC":
              walkOffsetY = 0;
              break;
            case "AXE":
              walkOffsetY = 256;
              break;
            case "PICKAXE":
              walkOffsetY = 512;
              break;
            case "SHOVEL":
              walkOffsetY = 768;
              break;
            case "SABER":
              walkOffsetY = 1024;
              break;
            case "LONGSWORD":
              walkOffsetY = 1280;
              break;
            case "GLOWSWORD":
              walkOffsetY = 1536;
              break;
          }
          player.entityAnimation = walkAnimation;
          break;
        case "TOOL":
          switch (toolType) {
            case "AXE":
              offsetY = 0;
              break;
            case "PICKAXE":
              offsetY = 256;
              break;
            case "SHOVEL":
              offsetY = 512;
              break;
          }
          player.usingTool = true;
          player.entityAnimation = toolAnimation;
          play();
          break;
        case "SWORD":
          switch (toolType) {
            case "SABER":
              offsetY = 0;
              break;
            case "LONGSWORD":
              offsetY = 768;
              break;
            case "GLOWSWORD":
              offsetY = 1536;
              break;
          }
          player.usingSword = true;
          player.entityAnimation = swordAnimation;
          play();
          break;
        case "BOW":
          switch (toolType) {
            case "NORMALBOW":
              offsetY = 0;
              break;
            case "RECURVEBOW":
              offsetY = 256;
              break;
          }
          player.usingBow = true;
          player.entityAnimation = bowAnimation;
          play();
          break;

      }

    }

  }


  private void play() {

    switch (entity.direction) {
      case "W":
        entity.entityAnimation.animation.play();
        entity.entityAnimation.animation.setOffsetY(offsetY);
        break;
      case "A":
        entity.entityAnimation.animation.play();
        entity.entityAnimation.animation.setOffsetY(offsetY + entity.entityAnimation.getSpriteHeight());
        break;
      case "S":
        entity.entityAnimation.animation.play();
        entity.entityAnimation.animation.setOffsetY(offsetY + entity.entityAnimation.getSpriteHeight() * 2);
        break;
      case "D":
        entity.entityAnimation.animation.play();
        entity.entityAnimation.animation.setOffsetY(offsetY + entity.entityAnimation.getSpriteHeight() * 3);
        break;
    }

  }


}
