package util;

import java.util.ArrayList;
import java.util.Random;

public class NoiseManager {

  public static float[][] createNoiseMap(int width, int height, int tileSize) {

    float scale = 17;
    int octaves = 20;
    float persistance = .75f;
    float lacunarity = 3.1f;

    float[][] noiseMap = new float[width/tileSize][height/tileSize];

    Random r = new Random();
    ArrayList<float[]> offsetList = new ArrayList<>();
    for (int n = 0; n < octaves; n++) {
      float offsetX = -100000 + r.nextFloat() * (100000 - (-100000));
      float offsetY = -100000 + r.nextFloat() * (100000 - (-100000));
      offsetList.add(new float[] {offsetX, offsetY});
    }

    for (int y = 0; y < height/tileSize; y++) {
      for (int x = 0; x < width/tileSize; x++) {

        float amplitude = 1;
        float frequency = 1;
        float noiseHeight = 0;

        for (int i = 0; i < octaves; i++) {

          float dx = x / scale * frequency + offsetList.get(i)[0];
          float dy = y / scale * frequency + offsetList.get(i)[1];

          float noise = (float) PerlinNoise2D.noise(dx, dy);
          noiseHeight += noise * amplitude;

          amplitude *= persistance;
          frequency *= lacunarity;

        }

        noiseMap[x][y] = (float) mapRange(-1, 1, 0, 1, noiseHeight);

        if (noiseMap[x][y] < 0) {
          noiseMap[x][y] = 0.0f;
        }

        if (noiseMap[x][y] > 255) {
          noiseMap[x][y] = 255f;
        }

      }
    }

    return noiseMap;

  }


  private static double mapRange(double a1, double a2, double b1, double b2, double s) {
    return b1 + ((s - a1)*(b2 - b1))/(a2 - a1);
  }

}
