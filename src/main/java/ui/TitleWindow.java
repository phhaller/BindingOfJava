package ui;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import view.GameWindowController;

public class TitleWindow extends AnchorPane {

  private GameWindowController gwc;
  private Label newGameLabel;
  private Label loadGameLabel;
  private Label quitLabel;


  public TitleWindow(GameWindowController gwc) {

    this.gwc = gwc;
    createWindow();
    createHandler();

  }


  private void createWindow() {

    setPrefSize(1280, 800);
    setTranslateX(0);
    setTranslateY(0);
    getStyleClass().add("titleWindow");
    setFocusTraversable(true);

    Rectangle imageRec = new Rectangle(0, 0, 1280, 800);
    imageRec.setFill(new ImagePattern(new Image("/images/util/titleBG.png")));

    Label titleLabel = new Label("BINDING OF JAVA");
    titleLabel.setPrefSize(800, 50);
    titleLabel.setTranslateX(240);
    titleLabel.setTranslateY(120);
    titleLabel.getStyleClass().add("titleTitleLabel");

    newGameLabel = new Label("NEW GAME");
    newGameLabel.setPrefSize(600, 50);
    newGameLabel.setTranslateX(340);
    newGameLabel.setTranslateY(470);
    newGameLabel.getStyleClass().add("titlePlayLabel");

    loadGameLabel = new Label("LOAD GAME");
    loadGameLabel.setPrefSize(600, 50);
    loadGameLabel.setTranslateX(340);
    loadGameLabel.setTranslateY(550);
    loadGameLabel.getStyleClass().add("titlePlayLabel");

    quitLabel = new Label("QUIT");
    quitLabel.setPrefSize(600, 50);
    quitLabel.setTranslateX(340);
    quitLabel.setTranslateY(630);
    quitLabel.getStyleClass().add("titlePlayLabel");


    getChildren().addAll(imageRec, titleLabel, newGameLabel, loadGameLabel, quitLabel);

  }


  private void createHandler() {

    newGameLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.gameState = gwc.playState;
      }
    });

    loadGameLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        // NOTHING YET
      }
    });

    quitLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        Platform.exit();
      }
    });

  }

}
