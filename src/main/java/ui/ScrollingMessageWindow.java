package ui;

import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import view.GameWindowController;


public class ScrollingMessageWindow extends AnchorPane {

  private GameWindowController gwc;


  public ScrollingMessageWindow(GameWindowController gwc) {
    this.gwc = gwc;
    createWindow();
  }


  private void createWindow() {
    setPrefSize(500, 50);
    setTranslateX(0);
    setTranslateY(gwc.screenHeight - 50);
    getStyleClass().add("scrollingMessageWindow");
    setFocusTraversable(true);
  }


  public void update() {

    int size = 40;
    int offsetY = 2;
    int messageY = 0;
    setTranslateY(messageY);
    setHeight(0);

    if (getChildren().size() > 0) {
      getChildren().clear();
    }

    for (int i = 0; i < gwc.scrollingMessageManager.messages.size(); i++) {

      String message = gwc.scrollingMessageManager.messages.get(i);
      if (message != null) {

        Label label = new Label(message);
        label.setPrefSize(400, size);
        label.setTranslateX(20);
        label.setTranslateY(messageY - 20 - offsetY);
        label.getStyleClass().add("scrollingLabel");

        int counter = gwc.scrollingMessageManager.messagesCounter.get(i) + 1;
        gwc.scrollingMessageManager.messagesCounter.set(i, counter);
        messageY += (size + offsetY);

        if (!getChildren().contains(label)) {
          getChildren().add(label);
        }

        if (gwc.scrollingMessageManager.messagesCounter.get(i) > 150) {
          gwc.scrollingMessageManager.messages.remove(i);
          gwc.scrollingMessageManager.messagesCounter.remove(i);
          i -= 1;
        }

      }

    }

    int height = getChildren().size() * size;
    setHeight(height);
    setTranslateY(gwc.screenHeight - height);

  }

}
