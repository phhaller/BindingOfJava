package ui;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import uiControllers.CraftingManager;
import view.GameWindowController;

import java.util.ArrayList;

public class InventoryWindow extends AnchorPane {

  private CraftingManager craftingManager;

  private GameWindowController gwc;
  private ScrollPane scrollPane;
  private ArrayList<Rectangle> itemRecs;
  private ArrayList<Label> amountLabels;


  public InventoryWindow(GameWindowController gwc) {
    this.gwc = gwc;

    setup();
    createWindow();

  }


  private void setup() {
    craftingManager = new CraftingManager(gwc);
    itemRecs = new ArrayList<>();
    amountLabels = new ArrayList<>();
  }


  private void createWindow() {

    setPrefSize(900, 450);
    setTranslateX(gwc.screenWidth / 2 - 450);
    setTranslateY(gwc.screenHeight / 2 - 225);
    getStyleClass().add("equipmentWindow");
    setFocusTraversable(true);

    Label craftLabel = new Label("Craft");
    craftLabel.setPrefSize(150, 30);
    craftLabel.setTranslateX(90);
    craftLabel.setTranslateY(50);
    craftLabel.getStyleClass().add("equipmentTitle");

    scrollPane = new ScrollPane();
    scrollPane.setPrefSize(220, 270);
    scrollPane.setTranslateX(90);
    scrollPane.setTranslateY(110);
    scrollPane.getStyleClass().add("scrollPane");

    Line dividerLine = new Line();
    dividerLine.setStartX(320);
    dividerLine.setStartY(99);
    dividerLine.setEndX(320);
    dividerLine.setEndY(390);
    dividerLine.getStyleClass().add("dividerLine");

    Label inventoryLabel = new Label("Inventory");
    inventoryLabel.setPrefSize(150, 30);
    inventoryLabel.setTranslateX(340);
    inventoryLabel.setTranslateY(50);
    inventoryLabel.getStyleClass().add("equipmentTitle");

    for (int j = 0; j < 4; j++) {
      for (int i = 0; i < 9; i++) {
        Rectangle rec = new Rectangle(48, 48);
        rec.setTranslateX(340 + i * 48 + i * 5);
        rec.setTranslateY(110 + j * 48 + j * 5);
        rec.getStyleClass().add("equipmentRec");
        rec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
          @Override
          public void handle(MouseEvent event) {

            // CREATE HANDLER TO USE THE CLICKED ITEM (WHERE ITEM IS USABLE)
            String type = (String) gwc.inventoryManager.getInventoryMap().keySet().toArray()[itemRecs.indexOf(rec)];
            if (!type.equals("KEY")) {
              gwc.inventoryManager.removeItem(type, true);
              update();
            }

          }
        });

        itemRecs.add(rec);

      }
    }

    getChildren().addAll(craftLabel, scrollPane, dividerLine, inventoryLabel);

  }


  public void update() {

    getChildren().removeAll(itemRecs);
    getChildren().removeAll(amountLabels);

    scrollPane.setContent(null);

    // ITEMS
    int counter = 0;
    for (String type : gwc.inventoryManager.getInventoryMap().keySet()) {

      itemRecs.get(counter).setFill(new ImagePattern(gwc.inventoryManager.getInventoryMap().get(type).get(0).inventoryImage));

      Label amountLabel = new Label();
      amountLabel.setPrefSize(30, 20);
      amountLabel.translateXProperty().bind(itemRecs.get(counter).translateXProperty().add(23));
      amountLabel.translateYProperty().bind(itemRecs.get(counter).translateYProperty().add(30));
      amountLabel.getStyleClass().add("inventoryAmountLabel");
      amountLabel.setText(String.valueOf(gwc.inventoryManager.getInventoryMap().get(type).size()));

      amountLabels.add(amountLabel);

      getChildren().addAll(itemRecs.get(counter), amountLabel);
      counter++;
    }

    int logs = gwc.inventoryManager.getInventoryMap().containsKey("LOG") ? gwc.inventoryManager.getInventoryMap().get("LOG").size() : 0;
    int rocks = gwc.inventoryManager.getInventoryMap().containsKey("ROCK") ? gwc.inventoryManager.getInventoryMap().get("ROCK").size() : 0;
    int berryReds = gwc.inventoryManager.getInventoryMap().containsKey("BERRYRED") ? gwc.inventoryManager.getInventoryMap().get("BERRYRED").size() : 0;
    int berryPurples = gwc.inventoryManager.getInventoryMap().containsKey("BERRYPURPLE") ? gwc.inventoryManager.getInventoryMap().get("BERRYPURPLE").size() : 0;
    int berryBlues = gwc.inventoryManager.getInventoryMap().containsKey("BERRYBLUE") ? gwc.inventoryManager.getInventoryMap().get("BERRYBLUE").size() : 0;
    int berryYellows = gwc.inventoryManager.getInventoryMap().containsKey("BERRYYELLOW") ? gwc.inventoryManager.getInventoryMap().get("BERRYYELLOW").size() : 0;
    int chickenLegs = gwc.inventoryManager.getInventoryMap().containsKey("CHICKENLEG_RAW") ? gwc.inventoryManager.getInventoryMap().get("CHICKENLEG_RAW").size() : 0;
    int porkChops = gwc.inventoryManager.getInventoryMap().containsKey("PORKCHOP_RAW") ? gwc.inventoryManager.getInventoryMap().get("PORKCHOP_RAW").size() : 0;
    int ribs = gwc.inventoryManager.getInventoryMap().containsKey("RIBS_RAW") ? gwc.inventoryManager.getInventoryMap().get("RIBS_RAW").size() : 0;
    int steaks = gwc.inventoryManager.getInventoryMap().containsKey("STEAK_RAW") ? gwc.inventoryManager.getInventoryMap().get("STEAK_RAW").size() : 0;


    craftingManager.update(logs, rocks, berryReds, berryPurples, berryBlues, berryYellows, chickenLegs, porkChops, ribs, steaks);

    int counter1 = 0;
    AnchorPane anchorPane = new AnchorPane();
    anchorPane.setPrefSize(200, craftingManager.getPanes().size() * 50 + craftingManager.getPanes().size() * 5);
    for (AnchorPane pane : craftingManager.getPanes()) {

      pane.setTranslateX(0);
      pane.setTranslateY(counter1 * 50);

      anchorPane.getChildren().add(pane);

      counter1++;
    }

    scrollPane.setContent(anchorPane);

  }


}
