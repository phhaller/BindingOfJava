package ui;

import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import view.GameWindowController;

public class EnergyBar extends AnchorPane {

  private GameWindowController gwc;
  public int maxEnergy = 100;
  public int energy = maxEnergy;
  private ProgressBar energyBar;


  public EnergyBar(GameWindowController gwc) {

    this.gwc = gwc;

    setPrefSize(220, 20);
    setTranslateX(10);
    setTranslateY(43);

    createEnergyBar();

  }


  private void createEnergyBar() {

    energyBar = new ProgressBar();
    energyBar.setPrefSize(220, 20);
    energyBar.setTranslateX(0);
    energyBar.setTranslateY(0);
    energyBar.setProgress(1);
    energyBar.getStyleClass().add("energyBar");

    getChildren().addAll(energyBar);

  }


  public void use(int amount) {

    energy -= amount;
    energyBar.setProgress(1.0 / maxEnergy * energy);

    if (energyBar.getProgress() <= 0) {
      energyBar.setProgress(0);
    }

  }


  public void fill(int amount) {

    if(energy < maxEnergy) {
      energy += amount;
      energyBar.setProgress(1.0 / maxEnergy * energy);
    }

    if (energyBar.getProgress() >= 1) {
      energyBar.setProgress(1);
    }

  }

}
