package ui;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import view.GameWindowController;

public class BuildingWindow extends AnchorPane {

  private GameWindowController gwc;

  private Label buildingsLabel;
  private Label machinesLabel;
  private Label decorationsLabel;


  private Rectangle wallRec;
  private Rectangle anvilRec;
  private Rectangle lumberRec;
  private Rectangle brewerRec;
  private Rectangle campfireRec;

  private Label wallAmountLabel;
  private Label anvilAmountLabel;
  private Label stompAmountLabel;
  private Label brewingstandAmountLabel;
  private Label campfireAmountLabel;


  public BuildingWindow(GameWindowController gwc) {

    this.gwc = gwc;
    createWindow();
    createHandler();

  }


  private void createWindow() {

    setPrefSize(900, 600);
    setTranslateX(gwc.screenWidth / 2 - 450);
    setTranslateY(gwc.screenHeight / 2 - 300);
    getStyleClass().add("buildingWindow");
    setFocusTraversable(true);


    // BUILDINGS

    buildingsLabel = new Label("Building");
    buildingsLabel.setPrefSize(150, 20);
    buildingsLabel.setTranslateX(90);
    buildingsLabel.setTranslateY(50);
    buildingsLabel.getStyleClass().add("buildingTitle");

    wallRec = new Rectangle(48, 48);
    wallRec.setTranslateX(90);
    wallRec.setTranslateY(90);
    wallRec.getStyleClass().add("buildingRec");
    wallRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("WALL").get(0)));
    wallRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.player.building = true;
        gwc.buildingManager.currentType = "WALL";
        gwc.gameState = gwc.playState;
      }
    });



    // MACHINES

    machinesLabel = new Label("Machines");
    machinesLabel.setPrefSize(150, 20);
    machinesLabel.setTranslateX(90);
    machinesLabel.setTranslateY(178);
    machinesLabel.getStyleClass().add("buildingTitle");

    anvilRec = new Rectangle(48, 48);
    anvilRec.setTranslateX(90);
    anvilRec.setTranslateY(218);
    anvilRec.getStyleClass().add("buildingRec");
    anvilRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("MACHINE").get(0)));
    anvilRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.player.building = true;
        gwc.buildingManager.currentType = "ANVIL";
        gwc.gameState = gwc.playState;
      }
    });

    lumberRec = new Rectangle(48, 48);
    lumberRec.setTranslateX(143);
    lumberRec.setTranslateY(218);
    lumberRec.getStyleClass().add("buildingRec");
    lumberRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("MACHINE").get(1)));
    lumberRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.player.building = true;
        gwc.buildingManager.currentType = "LUMBER";
        gwc.gameState = gwc.playState;
      }
    });

    brewerRec = new Rectangle(48, 48);
    brewerRec.setTranslateX(196);
    brewerRec.setTranslateY(218);
    brewerRec.getStyleClass().add("buildingRec");
    brewerRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("MACHINE").get(2)));
    brewerRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.player.building = true;
        gwc.buildingManager.currentType = "BREWER";
        gwc.gameState = gwc.playState;
      }
    });

    campfireRec = new Rectangle(48, 48);
    campfireRec.setTranslateX(249);
    campfireRec.setTranslateY(218);
    campfireRec.getStyleClass().add("buildingRec");
    campfireRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("MACHINE").get(3)));
    campfireRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.player.building = true;
        gwc.buildingManager.currentType = "CAMPFIRE";
        gwc.gameState = gwc.playState;
      }
    });




    getChildren().addAll(buildingsLabel, wallRec, machinesLabel, anvilRec, lumberRec, brewerRec, campfireRec);

  }


  private void createHandler() {



  }

}
