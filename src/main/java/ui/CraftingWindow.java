package ui;

import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import uiControllers.CraftingManager;
import view.GameWindowController;


public class CraftingWindow extends AnchorPane {

  private CraftingManager craftingManager;

  private GameWindowController gwc;
  private ScrollPane scrollPane;
  private Label machineLabel;

  public CraftingWindow(GameWindowController gwc) {
    this.gwc = gwc;

    setup();
    createWindow();

  }


  private void setup() {
    craftingManager = new CraftingManager(gwc);
  }


  private void createWindow() {

    setPrefSize(380, 440);
    setTranslateX(gwc.screenWidth / 2 - 190);
    setTranslateY(gwc.screenHeight / 2 - 220);
    getStyleClass().add("craftingWindow");
    setFocusTraversable(true);

    machineLabel = new Label("Anvil");
    machineLabel.setPrefSize(150, 30);
    machineLabel.setTranslateX(90);
    machineLabel.setTranslateY(40);
    machineLabel.getStyleClass().add("craftingWindowTitle");

    scrollPane = new ScrollPane();
    scrollPane.setPrefSize(210, 280);
    scrollPane.setTranslateX(90);
    scrollPane.setTranslateY(90);
    scrollPane.getStyleClass().add("scrollPane");
    scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);



    getChildren().addAll(machineLabel, scrollPane);

  }


  public void update() {

    int logs = gwc.inventoryManager.getInventoryMap().containsKey("LOG") ? gwc.inventoryManager.getInventoryMap().get("LOG").size() : 0;
    int rocks = gwc.inventoryManager.getInventoryMap().containsKey("ROCK") ? gwc.inventoryManager.getInventoryMap().get("ROCK").size() : 0;
    int berryReds = gwc.inventoryManager.getInventoryMap().containsKey("BERRYRED") ? gwc.inventoryManager.getInventoryMap().get("BERRYRED").size() : 0;
    int berryPurples = gwc.inventoryManager.getInventoryMap().containsKey("BERRYPURPLE") ? gwc.inventoryManager.getInventoryMap().get("BERRYPURPLE").size() : 0;
    int berryBlues = gwc.inventoryManager.getInventoryMap().containsKey("BERRYBLUE") ? gwc.inventoryManager.getInventoryMap().get("BERRYBLUE").size() : 0;
    int berryYellows = gwc.inventoryManager.getInventoryMap().containsKey("BERRYYELLOW") ? gwc.inventoryManager.getInventoryMap().get("BERRYYELLOW").size() : 0;
    int chickenLegs = gwc.inventoryManager.getInventoryMap().containsKey("CHICKENLEG_RAW") ? gwc.inventoryManager.getInventoryMap().get("CHICKENLEG_RAW").size() : 0;
    int porkChops = gwc.inventoryManager.getInventoryMap().containsKey("PORKCHOP_RAW") ? gwc.inventoryManager.getInventoryMap().get("PORKCHOP_RAW").size() : 0;
    int ribs = gwc.inventoryManager.getInventoryMap().containsKey("RIBS_RAW") ? gwc.inventoryManager.getInventoryMap().get("RIBS_RAW").size() : 0;
    int steaks = gwc.inventoryManager.getInventoryMap().containsKey("STEAK_RAW") ? gwc.inventoryManager.getInventoryMap().get("STEAK_RAW").size() : 0;

    switch (gwc.machineState) {

      case 0:
        machineLabel.setText("Anvil");
        break;
      case 1:
        machineLabel.setText("Lumber");
        break;
      case 2:
        machineLabel.setText("Brewer");
        break;
      case 3:
        machineLabel.setText("Campfire");
        break;

    }

    craftingManager.update(machineLabel.getText().toUpperCase(), logs, rocks, berryReds, berryPurples, berryBlues, berryYellows,
                          chickenLegs, porkChops, ribs, steaks);

    int counter = 0;
    AnchorPane anchorPane = new AnchorPane();
    anchorPane.setPrefSize(200, craftingManager.getPanes().size() * 50 + craftingManager.getPanes().size() * 5);
    for (AnchorPane pane : craftingManager.getPanes()) {

      pane.setTranslateX(0);
      pane.setTranslateY(counter * 50);

      anchorPane.getChildren().add(pane);

      counter++;
    }

    scrollPane.setContent(anchorPane);

  }

}
