package ui;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import view.GameWindowController;
import view.StartWindowController;

import java.io.IOException;

public class PauseWindow extends AnchorPane {

  private GameWindowController gwc;
  private Label continueLabel;
  private Label saveLabel;
  private Label settingsLabel;
  private Label quitLabel;


  public PauseWindow(GameWindowController gwc) {

    this.gwc = gwc;
    createWindow();
    createHandler();

  }


  private void createWindow() {

    setPrefSize(450, 355);
    setTranslateX(gwc.screenWidth / 2 - 225);
    setTranslateY(gwc.screenHeight / 2 - 150);
    getStyleClass().add("pauseWindow");
    setFocusTraversable(true);

    continueLabel = new Label("CONTINUE");
    continueLabel.setPrefSize(400, 50);
    continueLabel.setTranslateX(25);
    continueLabel.setTranslateY(70);
    continueLabel.getStyleClass().add("pauseLabel");

    saveLabel = new Label("SAVE");
    saveLabel.setPrefSize(400, 50);
    saveLabel.setTranslateX(25);
    saveLabel.setTranslateY(125);
    saveLabel.getStyleClass().add("pauseLabel");

    settingsLabel = new Label("SETTINGS");
    settingsLabel.setPrefSize(400, 50);
    settingsLabel.setTranslateX(25);
    settingsLabel.setTranslateY(180);
    settingsLabel.getStyleClass().add("pauseLabel");

    quitLabel = new Label("QUIT");
    quitLabel.setPrefSize(400, 50);
    quitLabel.setTranslateX(25);
    quitLabel.setTranslateY(235);
    quitLabel.getStyleClass().add("pauseLabel");

    getChildren().addAll(continueLabel, saveLabel, settingsLabel, quitLabel);

  }


  private void createHandler() {

    continueLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.gameState = gwc.playState;
        System.out.println("CONTINUE clicked");
      }
    });

    saveLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("SAVE clicked");
        gwc.jsonManager.createJson(gwc.worldName, gwc.tileManager.getTiles());

      }
    });

    settingsLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("SETTINGS clicked");
      }
    });

    quitLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("QUIT clicked");
        gwc.stopCurrentGame();
        switchStage();
      }
    });

  }


  private void switchStage() {

    try {

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/startWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) gwc.getBackground().getScene().getWindow();

      StartWindowController controller = loader.getController();
      controller.initialize(gwc.scene);

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }


}
