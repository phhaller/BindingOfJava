package ui;

import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import view.GameWindowController;

public class UIManager {

  private GameWindowController gwc;
  public HealthBar healthBar;
  public EnergyBar energyBar;
  public AnchorPane currentEquipmentDisplay;
  private Rectangle currentEquipmentRec;
  private Label currentEquipmentAmountLabel;
  private int energyFillCounter = 0;

  public PauseWindow pauseWindow;
  public DialogWindow dialogWindow;
  public TitleWindow titleWindow;
  public EquipmentWindow equipmentWindow;
  public InventoryWindow inventoryWindow;
  public ScrollingMessageWindow scrollingMessageWindow;
  public CraftingWindow craftingWindow;
  public BuildingWindow buildingWindow;


  public UIManager(GameWindowController gwc) {
    this.gwc = gwc;
    createWindows();
  }


  private void createWindows() {

    createCurrentEquipmentDisplay();
    healthBar = new HealthBar(gwc, 10, 10, 250, 30, gwc.player.maxLife, true);
    energyBar = new EnergyBar(gwc);
    pauseWindow = new PauseWindow(gwc);
    dialogWindow = new DialogWindow(gwc);
    titleWindow = new TitleWindow(gwc);
    equipmentWindow = new EquipmentWindow(gwc);
    inventoryWindow = new InventoryWindow(gwc);
    scrollingMessageWindow = new ScrollingMessageWindow(gwc);
    craftingWindow = new CraftingWindow(gwc);
    buildingWindow = new BuildingWindow(gwc);

  }


  private void createCurrentEquipmentDisplay() {
    currentEquipmentDisplay = new AnchorPane();
    currentEquipmentDisplay.setPrefSize(64, 64);
    currentEquipmentDisplay.setTranslateX(1196);
    currentEquipmentDisplay.setTranslateY(20);

    currentEquipmentRec = new Rectangle();
    currentEquipmentRec.setWidth(64);
    currentEquipmentRec.setHeight(64);
    currentEquipmentRec.setTranslateX(0);
    currentEquipmentRec.setTranslateY(0);
    currentEquipmentRec.getStyleClass().add("currentEquipmentRec");

    currentEquipmentAmountLabel = new Label();
    currentEquipmentAmountLabel.setPrefSize(20, 10);
    currentEquipmentAmountLabel.setTranslateX(45);
    currentEquipmentAmountLabel.setTranslateY(45);
    currentEquipmentAmountLabel.getStyleClass().add("currentEquipmentLabel");

    currentEquipmentDisplay.getChildren().addAll(currentEquipmentRec, currentEquipmentAmountLabel);
  }


  public void update() {

    energyFillCounter++;

    if (energyFillCounter > 60) {
      energyBar.fill(2);
      energyFillCounter = 0;
    }

  }


  public void draw() {

    // TITLE STATE
    if (gwc.gameState == gwc.titleState) {
      if (!gwc.getBackground().getChildren().contains(titleWindow)) {
        gwc.getBackground().getChildren().add(titleWindow);
      }
    }

    // PLAY STATE
    if (gwc.gameState == gwc.playState) {
      gwc.getBackground().getChildren().removeAll(titleWindow, pauseWindow, dialogWindow, equipmentWindow, inventoryWindow,
          craftingWindow, buildingWindow);

      if (!gwc.getBackground().getChildren().contains(currentEquipmentDisplay)) {
        gwc.getBackground().getChildren().add(currentEquipmentDisplay);
      }

      if (gwc.equipmentManager.current == null) {
        currentEquipmentRec.setFill(Color.TRANSPARENT);
        currentEquipmentAmountLabel.setVisible(false);
      } else {
        currentEquipmentRec.setFill(new ImagePattern(gwc.equipmentManager.current.image));
        currentEquipmentAmountLabel.setVisible(true);
        currentEquipmentAmountLabel.setText(String.valueOf(gwc.equipmentManager.current.durability));
      }

      if (!gwc.getBackground().getChildren().contains(healthBar)) {
        gwc.getBackground().getChildren().add(healthBar);
      }

      if (!gwc.getBackground().getChildren().contains(energyBar)) {
        gwc.getBackground().getChildren().add(energyBar);
      }
    }

    // PAUSE STATE
    if (gwc.gameState == gwc.pauseState) {
      if (!gwc.getBackground().getChildren().contains(pauseWindow)) {
        gwc.getBackground().getChildren().add(pauseWindow);
      }
    }

    // DIALOG STATE
    if (gwc.gameState == gwc.dialogState) {
      if (!gwc.getBackground().getChildren().contains(dialogWindow)) {
        gwc.getBackground().getChildren().add(dialogWindow);
      }
    }

    // EQUIPMENT STATE
    if (gwc.gameState == gwc.equipmentState) {
      if (!gwc.getBackground().getChildren().contains(equipmentWindow)) {
        gwc.getBackground().getChildren().add(equipmentWindow);
      }
    }

    // INVENTORY STATE
    if (gwc.gameState == gwc.inventoryState) {
      if (!gwc.getBackground().getChildren().contains(inventoryWindow)) {
        gwc.getBackground().getChildren().add(inventoryWindow);
      }
    }

    // CRAFTING STATE
    if (gwc.gameState == gwc.craftingState) {
      if (!gwc.getBackground().getChildren().contains(craftingWindow)) {
        gwc.getBackground().getChildren().add(craftingWindow);
      }
    }

    // BUILDING STATE
    if (gwc.gameState == gwc.buildingState) {
      if (!gwc.getBackground().getChildren().contains(buildingWindow)) {
        gwc.getBackground().getChildren().add(buildingWindow);
      }
    }


    if (!gwc.getBackground().getChildren().contains(scrollingMessageWindow)) {
      gwc.getBackground().getChildren().add(scrollingMessageWindow);
    }

    scrollingMessageWindow.update();

  }


}
