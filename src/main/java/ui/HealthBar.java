package ui;

import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import view.GameWindowController;

public class HealthBar extends AnchorPane {

  private GameWindowController gwc;
  private int width;
  private int height;
  private int maxHealth;
  public ProgressBar healthBar;
  private Label statusLabel;
  public boolean hasLabel;


  public HealthBar(GameWindowController gwc, int x, int y, int width, int height, int maxHealth, boolean hasLabel) {

    this.gwc = gwc;
    this.width = width;
    this.height = height;
    this.maxHealth = maxHealth;
    this.hasLabel = hasLabel;

    setPrefSize(width, height);
    setTranslateX(x);
    setTranslateY(y);

    createHealthBar();

    if (hasLabel) {
      createLabel();
    }

  }


  private void createHealthBar() {

    healthBar = new ProgressBar();
    healthBar.setPrefSize(width, height);
    healthBar.setTranslateX(0);
    healthBar.setTranslateY(0);
    healthBar.setProgress(1);
    healthBar.getStyleClass().add("healthBar");

    getChildren().add(healthBar);

  }


  private void createLabel() {

    statusLabel = new Label(String.valueOf(maxHealth));
    statusLabel.setPrefSize(width - 7, height);
    statusLabel.setTranslateX(7);
    statusLabel.setTranslateY(0);
    statusLabel.getStyleClass().add("statusLabel");

    getChildren().add(statusLabel);

  }


  public void hit(int currentHealth) {

    healthBar.setProgress(1.0 / maxHealth * currentHealth);
    if (healthBar.getProgress() <= 0) {
      healthBar.setProgress(0);
    }

    if (hasLabel) {
      statusLabel.setText(String.valueOf((int) (healthBar.getProgress() * 100)));
    }

  }


  public void heal(int currentHealth) {

    healthBar.setProgress(1.0 / maxHealth * currentHealth);
    if (healthBar.getProgress() >= 1) {
      healthBar.setProgress(1);
    }

    if (hasLabel) {
      statusLabel.setText(String.valueOf((int) (healthBar.getProgress() * 100)));
    }

  }

}
