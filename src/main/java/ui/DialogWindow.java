package ui;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import uiControllers.CraftingManager;
import uiControllers.DialogManager;
import view.GameWindowController;

public class DialogWindow extends AnchorPane {

  private GameWindowController gwc;
  public DialogManager dialogManager;

  private Label npcLabel;

  private Label buyLabel;
  private ScrollPane buyScrollPane;
  private Label sellLabel;
  private ScrollPane sellScrollPane;


  public DialogWindow(GameWindowController gwc) {

    this.gwc = gwc;
    setup();
    createWindow();

  }


  private void setup() {
    dialogManager = new DialogManager(gwc);
  }


  private void createWindow() {

    setPrefSize(900, 520);
    setTranslateX(gwc.screenWidth / 2 - 450);
    setTranslateY(gwc.screenHeight / 2 - 260);
    getStyleClass().add("dialogWindow");
    setFocusTraversable(true);

    npcLabel = new Label("");
    npcLabel.setPrefSize(150, 30);
    npcLabel.setTranslateX(90);
    npcLabel.setTranslateY(40);
    npcLabel.getStyleClass().add("craftingWindowTitle");

    buyLabel = new Label("Buy");
    buyLabel.setPrefSize(200, 30);
    buyLabel.setTranslateX(90);
    buyLabel.setTranslateY(85);
    buyLabel.getStyleClass().add("dialogWindowSubTitle");

    buyScrollPane = new ScrollPane();
    buyScrollPane.setPrefSize(340, 340);
    buyScrollPane.setTranslateX(90);
    buyScrollPane.setTranslateY(120);
    buyScrollPane.getStyleClass().add("scrollPane");
    buyScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

    Line dividerLine = new Line();
    dividerLine.setStartX(450);
    dividerLine.setStartY(99);
    dividerLine.setEndX(450);
    dividerLine.setEndY(450);
    dividerLine.getStyleClass().add("dividerLine");

    sellLabel = new Label("Sell");
    sellLabel.setPrefSize(200, 30);
    sellLabel.setTranslateX(470);
    sellLabel.setTranslateY(85);
    sellLabel.getStyleClass().add("dialogWindowSubTitle");

    sellScrollPane = new ScrollPane();
    sellScrollPane.setPrefSize(340, 340);
    sellScrollPane.setTranslateX(470);
    sellScrollPane.setTranslateY(120);
    sellScrollPane.getStyleClass().add("scrollPane");
    sellScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

    getChildren().addAll(npcLabel, buyLabel, buyScrollPane, dividerLine, sellLabel, sellScrollPane);

  }


  public void update() {

    switch (gwc.npcState) {

      case 0:
        npcLabel.setText("Merchant");
        break;
      case 1:
        npcLabel.setText("Blacksmith");
        break;

    }

    dialogManager.updateBuy(npcLabel.getText().toUpperCase());
    dialogManager.updateSell(npcLabel.getText().toUpperCase());

    int buyCounter = 0;
    int sellCounter = 0;

    AnchorPane buyPane = new AnchorPane();
    buyPane.setPrefSize(320, dialogManager.getBuyPanes().size() * 50);
    for (AnchorPane pane : dialogManager.getBuyPanes()) {

      pane.setTranslateX(0);
      pane.setTranslateY(buyCounter * 50);

      buyPane.getChildren().add(pane);

      buyCounter++;
    }

    AnchorPane sellPane = new AnchorPane();
    sellPane.setPrefSize(320, dialogManager.getSellPanes().size() * 50);
    for (AnchorPane pane : dialogManager.getSellPanes()) {

      pane.setTranslateX(0);
      pane.setTranslateY(sellCounter * 50);

      sellPane.getChildren().add(pane);

      sellCounter++;
    }


    buyScrollPane.setContent(buyPane);
    sellScrollPane.setContent(sellPane);

  }

}
