package ui;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import objects.SuperObject;
import view.GameWindowController;

import java.util.ArrayList;

public class EquipmentWindow extends AnchorPane {

  private GameWindowController gwc;

  private Rectangle playerImgRec;

  private Image playerBasicImg;
  private Image playerAxeImg;
  private Image playerPickaxeImg;
  private Image playerShovelImg;
  private Image playerSaberImg;
  private Image playerLongswordImg;

  private Label currentTypeLabel;
  private Label playerDamageStats;
  private Label playerStrengthStats;
  private Label playerDexterityStats;
  private Label playerDefenseStats;
  private Label currentDurabilityStats;

  private Rectangle currentTool;
  private Rectangle currentSword;
  private Rectangle currentBow;
  private Rectangle currentArmour;

  private Rectangle currentToolCancel;
  private Rectangle currentSwordCancel;
  private Rectangle currentBowCancel;
  private Rectangle currentArmourCancel;

  private ArrayList<Rectangle> currentRecs;

  private ArrayList<Rectangle> toolRecs;
  private ArrayList<Rectangle> swordRecs;
  private ArrayList<Rectangle> bowRecs;
  private ArrayList<Rectangle> armourRecs;

  private AnchorPane toolStatsWindow;
  private Label toolStatsName;
  private Label toolDamageStats;
  private Label toolDefenseStats;
  private Label toolDurabilityStats;
  private Label toolEnergyUseStats;


  private EventHandler<MouseEvent> selectHandler;


  public EquipmentWindow(GameWindowController gwc) {

    this.gwc = gwc;

    setup();
    createWindow();
    createStatsWindow();

  }


  private void setup() {

    toolRecs = new ArrayList<>();
    swordRecs = new ArrayList<>();
    bowRecs = new ArrayList<>();
    armourRecs = new ArrayList<>();
    currentRecs = new ArrayList<>();

  }


  private void createWindow() {

    setPrefSize(900, 520);
    setTranslateX(gwc.screenWidth / 2 - 450);
    setTranslateY(gwc.screenHeight / 2 - 260);
    getStyleClass().add("equipmentWindow");
    setFocusTraversable(true);

    Line dividerLine1 = new Line();
    dividerLine1.setStartX(270);
    dividerLine1.setStartY(99);
    dividerLine1.setEndX(270);
    dividerLine1.setEndY(450);
    dividerLine1.getStyleClass().add("dividerLine");

    Label equipmentLabel = new Label("Equipment");
    equipmentLabel.setPrefSize(150, 30);
    equipmentLabel.setTranslateX(290);
    equipmentLabel.setTranslateY(50);
    equipmentLabel.getStyleClass().add("equipmentTitle");

    Line dividerLine2 = new Line();
    dividerLine2.setStartX(450);
    dividerLine2.setStartY(99);
    dividerLine2.setEndX(450);
    dividerLine2.setEndY(450);
    dividerLine2.getStyleClass().add("dividerLine");

    Label inventoryLabel = new Label("Inventory");
    inventoryLabel.setPrefSize(150, 30);
    inventoryLabel.setTranslateX(470);
    inventoryLabel.setTranslateY(50);
    inventoryLabel.getStyleClass().add("equipmentTitle");


    // PLAYER STATS
    Label playerStatsLabel = new Label("Stats");
    playerStatsLabel.setPrefSize(150, 30);
    playerStatsLabel.setTranslateX(90);
    playerStatsLabel.setTranslateY(50);
    playerStatsLabel.getStyleClass().add("equipmentTitle");

    playerBasicImg = new Image("/images/util/playerBasic.png");
    playerAxeImg = new Image("/images/util/playerAxe.png");
    playerPickaxeImg = new Image("/images/util/playerPickaxe.png");
    playerShovelImg = new Image("/images/util/playerShovel.png");
    playerSaberImg = new Image("/images/util/playerSaber.png");
    playerLongswordImg = new Image("/images/util/playerLongsword.png");

    playerImgRec = new Rectangle(90, 100, 128, 128);
    playerImgRec.setFill(new ImagePattern(playerBasicImg));

    currentTypeLabel = new Label("TYPE");
    currentTypeLabel.setPrefSize(100, 30);
    currentTypeLabel.setTranslateX(90);
    currentTypeLabel.setTranslateY(258);
    currentTypeLabel.getStyleClass().add("statsTitle");

    Label playerDamageLabel = new Label("Damage:");
    playerDamageLabel.setPrefSize(100, 20);
    playerDamageLabel.setTranslateX(90);
    playerDamageLabel.setTranslateY(298);
    playerDamageLabel.getStyleClass().add("statsLabel");

    playerDamageStats = new Label("");
    playerDamageStats.setPrefSize(40, 20);
    playerDamageStats.setTranslateX(210);
    playerDamageStats.setTranslateY(298);
    playerDamageStats.getStyleClass().add("statsLabel");

    Label playerStrengthLabel = new Label("Strength:");
    playerStrengthLabel.setPrefSize(100, 20);
    playerStrengthLabel.setTranslateX(90);
    playerStrengthLabel.setTranslateY(328);
    playerStrengthLabel.getStyleClass().add("statsLabel");

    playerStrengthStats = new Label("");
    playerStrengthStats.setPrefSize(40, 20);
    playerStrengthStats.setTranslateX(210);
    playerStrengthStats.setTranslateY(328);
    playerStrengthStats.getStyleClass().add("statsLabel");

    Label playerDexterityLabel = new Label("Dexterity:");
    playerDexterityLabel.setPrefSize(100, 20);
    playerDexterityLabel.setTranslateX(90);
    playerDexterityLabel.setTranslateY(358);
    playerDexterityLabel.getStyleClass().add("statsLabel");

    playerDexterityStats = new Label("");
    playerDexterityStats.setPrefSize(40, 20);
    playerDexterityStats.setTranslateX(210);
    playerDexterityStats.setTranslateY(358);
    playerDexterityStats.getStyleClass().add("statsLabel");

    Label playerDefenseLabel = new Label("Defense:");
    playerDefenseLabel.setPrefSize(100, 20);
    playerDefenseLabel.setTranslateX(90);
    playerDefenseLabel.setTranslateY(388);
    playerDefenseLabel.getStyleClass().add("statsLabel");

    playerDefenseStats = new Label("");
    playerDefenseStats.setPrefSize(40, 20);
    playerDefenseStats.setTranslateX(210);
    playerDefenseStats.setTranslateY(388);
    playerDefenseStats.getStyleClass().add("statsLabel");

    Label currentDurabilityLabel = new Label("Durability:");
    currentDurabilityLabel.setPrefSize(100, 20);
    currentDurabilityLabel.setTranslateX(90);
    currentDurabilityLabel.setTranslateY(418);
    currentDurabilityLabel.getStyleClass().add("statsLabel");

    currentDurabilityStats = new Label("");
    currentDurabilityStats.setPrefSize(40, 20);
    currentDurabilityStats.setTranslateX(210);
    currentDurabilityStats.setTranslateY(418);
    currentDurabilityStats.getStyleClass().add("statsLabel");


    // TOOLS
    Label toolsLabel = new Label("Tool");
    toolsLabel.setPrefSize(150, 20);
    toolsLabel.setTranslateX(290);
    toolsLabel.setTranslateY(99);
    toolsLabel.getStyleClass().add("equipmentLabel");

    currentTool = new Rectangle(290, 125, 48, 48);
    currentTool.getStyleClass().add("equipmentCurrentRec");
    currentTool.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentTool);
        update();
      }
    });

    currentToolCancel = new Rectangle(380, 139, 20, 20);
    currentToolCancel.getStyleClass().add("cancelRec");
    currentToolCancel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.equipmentManager.setCurrentTool(null);
        gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentTool);
        update();
      }
    });

    for (int i = 0; i < 10; i++) {
      Rectangle rec = new Rectangle(470 + i * 48 + i * 5, 125, 48, 48);
      rec.getStyleClass().add("equipmentRec");
      rec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          gwc.equipmentManager.setCurrentTool(gwc.equipmentManager.getTools().get(toolRecs.indexOf(rec)));
          gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentTool);
          getChildren().remove(toolStatsWindow);
          update();
        }
      });

      rec.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          updateStats(rec.getX() + 58, rec.getY(), gwc.equipmentManager.getTools().get(toolRecs.indexOf(rec)));
          if (!getChildren().contains(toolStatsWindow)) {
            getChildren().add(toolStatsWindow);
          }
        }
      });

      rec.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          getChildren().remove(toolStatsWindow);
        }
      });
      toolRecs.add(rec);
    }


    // SWORDS
    Label swordsLabel = new Label("Sword");
    swordsLabel.setPrefSize(150, 20);
    swordsLabel.setTranslateX(290);
    swordsLabel.setTranslateY(188);
    swordsLabel.getStyleClass().add("equipmentLabel");

    currentSword = new Rectangle(290, 214, 48, 48);
    currentSword.getStyleClass().add("equipmentCurrentRec");
    currentSword.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentSword);
        update();
      }
    });

    currentSwordCancel = new Rectangle(380, 228, 20, 20);
    currentSwordCancel.getStyleClass().add("cancelRec");
    currentSwordCancel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.equipmentManager.setCurrentSword(null);
        gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentSword);
        update();
      }
    });

    for (int i = 0; i < 10; i++) {
      Rectangle rec = new Rectangle(470 + i * 48 + i * 5, 214, 48, 48);
      rec.getStyleClass().add("equipmentRec");
      rec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          gwc.equipmentManager.setCurrentSword(gwc.equipmentManager.getSwords().get(swordRecs.indexOf(rec)));
          gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentSword);
          getChildren().remove(toolStatsWindow);
          update();
        }
      });

      rec.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          updateStats(rec.getX() + 58, rec.getY(), gwc.equipmentManager.getSwords().get(swordRecs.indexOf(rec)));
          if (!getChildren().contains(toolStatsWindow)) {
            getChildren().add(toolStatsWindow);
          }
        }
      });

      rec.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          getChildren().remove(toolStatsWindow);
        }
      });
      swordRecs.add(rec);
    }


    // BOWS
    Label bowsLabel = new Label("Bow");
    bowsLabel.setPrefSize(150, 20);
    bowsLabel.setTranslateX(290);
    bowsLabel.setTranslateY(277);
    bowsLabel.getStyleClass().add("equipmentLabel");

    currentBow = new Rectangle(290, 303, 48, 48);
    currentBow.getStyleClass().add("equipmentCurrentRec");
    currentBow.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentBow);
        update();
      }
    });

    currentBowCancel = new Rectangle(380, 317, 20, 20);
    currentBowCancel.getStyleClass().add("cancelRec");
    currentBowCancel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.equipmentManager.setCurrentBow(null);
        gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentBow);
        update();
      }
    });

    for (int i = 0; i < 10; i++) {
      Rectangle rec = new Rectangle(470 + i * 48 + i * 5, 303, 48, 48);
      rec.getStyleClass().add("equipmentRec");
      rec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          gwc.equipmentManager.setCurrentBow(gwc.equipmentManager.getBows().get(bowRecs.indexOf(rec)));
          gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentBow);
          getChildren().remove(toolStatsWindow);
          update();
        }
      });

      rec.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          updateStats(rec.getX() + 58, rec.getY(), gwc.equipmentManager.getBows().get(bowRecs.indexOf(rec)));
          if (!getChildren().contains(toolStatsWindow)) {
            getChildren().add(toolStatsWindow);
          }
        }
      });

      rec.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          getChildren().remove(toolStatsWindow);
        }
      });
      bowRecs.add(rec);
    }


    // ARMOUR
    Label armoursLabel = new Label("Shield");
    armoursLabel.setPrefSize(150, 20);
    armoursLabel.setTranslateX(290);
    armoursLabel.setTranslateY(366);
    armoursLabel.getStyleClass().add("equipmentLabel");

    currentArmour = new Rectangle(290, 392, 48, 48);
    currentArmour.getStyleClass().add("equipmentCurrentRec");
    currentArmour.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentArmour);
        update();
      }
    });

    currentArmourCancel = new Rectangle(380, 406, 20, 20);
    currentArmourCancel.getStyleClass().add("cancelRec");
    currentArmourCancel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gwc.equipmentManager.setCurrentArmour(null);
        gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentArmour);
        update();
      }
    });

    for (int i = 0; i < 10; i++) {
      Rectangle rec = new Rectangle(470 + i * 48 + i * 5, 392, 48, 48);
      rec.getStyleClass().add("equipmentRec");
      rec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          gwc.equipmentManager.setCurrentArmour(gwc.equipmentManager.getArmour().get(armourRecs.indexOf(rec)));
          gwc.equipmentManager.setCurrent(gwc.equipmentManager.currentArmour);
          getChildren().remove(toolStatsWindow);
          update();
        }
      });

      rec.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          updateStats(rec.getX() + 58, rec.getY(), gwc.equipmentManager.getArmour().get(armourRecs.indexOf(rec)));
          if (!getChildren().contains(toolStatsWindow)) {
            getChildren().add(toolStatsWindow);
          }
        }
      });

      rec.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
          getChildren().remove(toolStatsWindow);
        }
      });
      armourRecs.add(rec);
    }

    currentRecs.add(currentTool);
    currentRecs.add(currentSword);
    currentRecs.add(currentBow);
    currentRecs.add(currentArmour);
    currentRecs.add(currentToolCancel);
    currentRecs.add(currentSwordCancel);
    currentRecs.add(currentBowCancel);
    currentRecs.add(currentArmourCancel);

    getChildren().addAll(playerStatsLabel, currentTypeLabel, playerImgRec, playerDamageLabel, playerDamageStats, playerStrengthLabel,
        playerStrengthStats, playerDexterityLabel, playerDexterityStats, playerDefenseLabel, playerDefenseStats,
        currentDurabilityLabel, currentDurabilityStats, equipmentLabel, dividerLine1, inventoryLabel, dividerLine2,
        toolsLabel, swordsLabel, bowsLabel, armoursLabel);

  }


  private void createStatsWindow() {

    toolStatsWindow = new AnchorPane();
    toolStatsWindow.setPrefSize(170, 135);
    toolStatsWindow.getStyleClass().add("equipmentStatsWindow");

    toolStatsName = new Label("");
    toolStatsName.setPrefSize(100, 20);
    toolStatsName.setTranslateX(20);
    toolStatsName.setTranslateY(10);
    toolStatsName.getStyleClass().add("statsTitle");

    Line dividerLine = new Line();
    dividerLine.setStartX(20);
    dividerLine.setStartY(35);
    dividerLine.setEndX(150);
    dividerLine.setEndY(35);
    dividerLine.getStyleClass().add("dividerLine");

    Label damageStatsLabel = new Label("Damage:");
    damageStatsLabel.setPrefSize(70, 20);
    damageStatsLabel.setTranslateX(20);
    damageStatsLabel.setTranslateY(40);
    damageStatsLabel.getStyleClass().add("statsLabel");

    toolDamageStats = new Label("");
    toolDamageStats.setPrefSize(40, 20);
    toolDamageStats.setTranslateX(110);
    toolDamageStats.setTranslateY(40);
    toolDamageStats.getStyleClass().add("statsLabel");

    Label defenseStatsLabel = new Label("Defense:");
    defenseStatsLabel.setPrefSize(70, 20);
    defenseStatsLabel.setTranslateX(20);
    defenseStatsLabel.setTranslateY(60);
    defenseStatsLabel.getStyleClass().add("statsLabel");

    toolDefenseStats = new Label("");
    toolDefenseStats.setPrefSize(40, 20);
    toolDefenseStats.setTranslateX(110);
    toolDefenseStats.setTranslateY(60);
    toolDefenseStats.getStyleClass().add("statsLabel");

    Label energyUseLabel = new Label("Energy use:");
    energyUseLabel.setPrefSize(70, 20);
    energyUseLabel.setTranslateX(20);
    energyUseLabel.setTranslateY(80);
    energyUseLabel.getStyleClass().add("statsLabel");

    toolEnergyUseStats = new Label("");
    toolEnergyUseStats.setPrefSize(40, 20);
    toolEnergyUseStats.setTranslateX(110);
    toolEnergyUseStats.setTranslateY(80);
    toolEnergyUseStats.getStyleClass().add("statsLabel");

    Label durabilityLabel = new Label("Durability:");
    durabilityLabel.setPrefSize(70, 20);
    durabilityLabel.setTranslateX(20);
    durabilityLabel.setTranslateY(100);
    durabilityLabel.getStyleClass().add("statsLabel");

    toolDurabilityStats = new Label("");
    toolDurabilityStats.setPrefSize(40, 20);
    toolDurabilityStats.setTranslateX(110);
    toolDurabilityStats.setTranslateY(100);
    toolDurabilityStats.getStyleClass().add("statsLabel");


    toolStatsWindow.getChildren().addAll(toolStatsName, dividerLine, damageStatsLabel, toolDamageStats, defenseStatsLabel,
        toolDefenseStats, energyUseLabel, toolEnergyUseStats, durabilityLabel, toolDurabilityStats);

  }


  private void updateStats(double x, double y, SuperObject object) {
    toolStatsWindow.setTranslateX(x);
    toolStatsWindow.setTranslateY(y);
    toolStatsName.setText(object.name);
    toolDamageStats.setText(String.valueOf(object.damage));
    toolDefenseStats.setText(String.valueOf(object.defense));
    toolEnergyUseStats.setText(String.valueOf(object.energyUse));
    toolDurabilityStats.setText(object.durability + " / " + object.maxDurability);
  }


  public void update() {

    getChildren().removeAll(playerImgRec);
    getChildren().removeAll(toolRecs);
    getChildren().removeAll(swordRecs);
    getChildren().removeAll(bowRecs);
    getChildren().removeAll(armourRecs);
    getChildren().removeAll(currentRecs);

    if (gwc.equipmentManager.current == null) {
      currentTypeLabel.setText("BASIC");
      currentDurabilityStats.setText("0 / 0");
    } else {
      currentTypeLabel.setText(gwc.equipmentManager.current.name);
      currentDurabilityStats.setText(gwc.equipmentManager.current.durability + " / " + gwc.equipmentManager.current.maxDurability);
    }

    playerDamageStats.setText(String.valueOf(gwc.player.damage));
    playerStrengthStats.setText(String.valueOf(gwc.player.strength));
    playerDexterityStats.setText(String.valueOf(gwc.player.dexterity));
    playerDefenseStats.setText(String.valueOf(gwc.player.defense));

    // PLAYER IMAGE
    if (gwc.equipmentManager.current == null || gwc.equipmentManager.current.type.equals("BOW")) {
      playerImgRec.setFill(new ImagePattern(playerBasicImg));
    } else {
      switch (gwc.equipmentManager.current.name) {
        case "AXE":
          playerImgRec.setFill(new ImagePattern(playerAxeImg));
          break;
        case "PICKAXE":
          playerImgRec.setFill(new ImagePattern(playerPickaxeImg));
          break;
        case "SHOVEL":
          playerImgRec.setFill(new ImagePattern(playerShovelImg));
          break;
        case "SABER":
          playerImgRec.setFill(new ImagePattern(playerSaberImg));
          break;
        case "LONGSWORD":
          playerImgRec.setFill(new ImagePattern(playerLongswordImg));
          break;
      }
    }

    getChildren().add(playerImgRec);


    // TOOLS
    if (gwc.equipmentManager.currentTool != null) {
      currentTool.setFill(new ImagePattern(gwc.equipmentManager.currentTool.image));
      getChildren().addAll(currentTool, currentToolCancel);
    }

    for (int i = 0; i < gwc.equipmentManager.getTools().size(); i++) {
      if (gwc.equipmentManager.getTools().get(i) != gwc.equipmentManager.currentTool) {
        toolRecs.get(i).setFill(new ImagePattern(gwc.equipmentManager.getTools().get(i).image));
        getChildren().add(toolRecs.get(i));
      }
    }


    // SWORDS
    if (gwc.equipmentManager.currentSword != null) {
      currentSword.setFill(new ImagePattern(gwc.equipmentManager.currentSword.image));
      getChildren().addAll(currentSword, currentSwordCancel);
    }

    for (int i = 0; i < gwc.equipmentManager.getSwords().size(); i++) {
      if (gwc.equipmentManager.getSwords().get(i) != gwc.equipmentManager.currentSword) {
        swordRecs.get(i).setFill(new ImagePattern(gwc.equipmentManager.getSwords().get(i).image));
        getChildren().add(swordRecs.get(i));
      }
    }


    // BOWS
    if (gwc.equipmentManager.currentBow != null) {
      currentBow.setFill(new ImagePattern(gwc.equipmentManager.currentBow.image));
      getChildren().addAll(currentBow, currentBowCancel);
    }

    for (int i = 0; i < gwc.equipmentManager.getBows().size(); i++) {
      if (gwc.equipmentManager.getBows().get(i) != gwc.equipmentManager.currentBow) {
        bowRecs.get(i).setFill(new ImagePattern(gwc.equipmentManager.getBows().get(i).image));
        getChildren().add(bowRecs.get(i));
      }
    }


    // ARMOUR
    if (gwc.equipmentManager.currentArmour != null) {
      currentArmour.setFill(new ImagePattern(gwc.equipmentManager.currentArmour.image));
      getChildren().addAll(currentArmour, currentArmourCancel);
    }

    for (int i = 0; i < gwc.equipmentManager.getArmour().size(); i++) {
      if (gwc.equipmentManager.getArmour().get(i) != gwc.equipmentManager.currentArmour) {
        armourRecs.get(i).setFill(new ImagePattern(gwc.equipmentManager.getArmour().get(i).image));
        getChildren().add(armourRecs.get(i));
      }
    }

  }


}
