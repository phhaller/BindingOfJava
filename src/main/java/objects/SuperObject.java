package objects;

import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import view.GameWindowController;

public class SuperObject {

  public GameWindowController gwc;
  public int worldX, worldY;
  public String name;
  public String type;
  public Image image;
  public Image inventoryImage;
  public Rectangle rec;
  public Rectangle hitBox = new Rectangle(0, 0, 48, 48);
  public int hitBoxDefaultX = 0;
  public int hitBoxDefaultY = 0;
  public boolean collision = false;

  // EQUIPMENT ATTRIBUTES
  public int damage;
  public int dexterity;
  public int defense;
  public int maxDurability;
  public int durability;
  public int energyUse;


  public SuperObject(GameWindowController gwc) {
    this.gwc = gwc;
    rec = new Rectangle(gwc.tileSize, gwc.tileSize);
  }

  // Used for chests
  public void open() {}

  public void draw() {

    gwc.getBackground().getChildren().remove(rec);

    int screenX = worldX - gwc.player.worldX + gwc.player.screenX;
    int screenY = worldY - gwc.player.worldY + gwc.player.screenY;
    int tileSize = gwc.tileSize;

    if (worldX + tileSize > gwc.player.worldX - gwc.player.screenX &&
        worldX - tileSize < gwc.player.worldX + gwc.player.screenX &&
        worldY + tileSize > gwc.player.worldY - gwc.player.screenY &&
        worldY - tileSize * 2 < gwc.player.worldY + gwc.player.screenY) {

      rec.setTranslateX(screenX + (gwc.tileSize - rec.getWidth()) / 2);
      rec.setTranslateY(screenY + (gwc.tileSize - rec.getHeight()) / 2);
      gwc.getBackground().getChildren().add(rec);

    }

  }

}
