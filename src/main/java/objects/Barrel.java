package objects;

import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Barrel extends SuperObject {

  public Barrel(int x, int y, String type, GameWindowController gwc) {

    super(gwc);

    worldX = x * gwc.tileSize;
    worldY = y * gwc.tileSize;
    name = "BARREL";
    this.type = type;
    collision = true;

    createBarrel();
    createHitBox();

  }


  private void createBarrel() {

    if (type.equals("UP")) {
      image = gwc.imageManager.containerMap.get("BARREL").get(0);
    } else if (type.equals("DOWN")) {
      image = gwc.imageManager.containerMap.get("BARREL").get(1);
    }

    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);
    rec.setFill(new ImagePattern(image));
  }


  private void createHitBox() {
    hitBox.setTranslateX(8);
    hitBox.setTranslateY(4);
    hitBox.setWidth(32);
    hitBox.setHeight(40);
  }


  public void open() {
    int items = 3;
    for (int i = 0; i < items; i++) {
      gwc.objectsManager.getRandomPotion(worldX, worldY);
    }
  }

}
