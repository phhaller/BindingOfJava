package objects;

import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Meat extends SuperObject {

  public Meat(int x, int y, boolean world, String type, boolean cooked, GameWindowController gwc) {

    super(gwc);

    if (!world) {
      worldX = x * gwc.tileSize;
      worldY = y * gwc.tileSize;
    } else {
      worldX = x;
      worldY = y;
    }

    switch (type) {
      case "CHICKENLEG":
        if (cooked) {
          name = "CHICKENLEG_COOKED";
          image = gwc.imageManager.itemMap.get("CHICKENLEG").get(1);
          inventoryImage = gwc.imageManager.inventoryMap.get("CHICKENLEG").get(1);
        } else {
          name = "CHICKENLEG_RAW";
          image = gwc.imageManager.itemMap.get("CHICKENLEG").get(0);
          inventoryImage = gwc.imageManager.inventoryMap.get("CHICKENLEG").get(0);
        }
        break;
      case "PORKCHOP":
        if (cooked) {
          name = "PORKCHOP_COOKED";
          image = gwc.imageManager.itemMap.get("PORKCHOP").get(1);
          inventoryImage = gwc.imageManager.inventoryMap.get("PORKCHOP").get(1);
        } else {
          name = "PORKCHOP_RAW";
          image = gwc.imageManager.itemMap.get("PORKCHOP").get(0);
          inventoryImage = gwc.imageManager.inventoryMap.get("PORKCHOP").get(0);
        }
        break;
      case "RIBS":
        if (cooked) {
          name = "RIBS_COOKED";
          image = gwc.imageManager.itemMap.get("RIBS").get(1);
          inventoryImage = gwc.imageManager.inventoryMap.get("RIBS").get(1);
        } else {
          name = "RIBS_RAW";
          image = gwc.imageManager.itemMap.get("RIBS").get(0);
          inventoryImage = gwc.imageManager.inventoryMap.get("RIBS").get(0);
        }
        break;
      case "STEAK":
        if (cooked) {
          name = "STEAK_COOKED";
          image = gwc.imageManager.itemMap.get("STEAK").get(1);
          inventoryImage = gwc.imageManager.inventoryMap.get("STEAK").get(1);
        } else {
          name = "STEAK_RAW";
          image = gwc.imageManager.itemMap.get("STEAK").get(0);
          inventoryImage = gwc.imageManager.inventoryMap.get("STEAK").get(0);
        }
        break;
    }

    createMeat();
    createHitBox();

  }


  private void createMeat() {

    rec.setWidth(24);
    rec.setHeight(24);
    rec.setTranslateX(worldX + 12);
    rec.setTranslateY(worldY + 12);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(12);
    hitBox.setTranslateY(12);
    hitBox.setWidth(24);
    hitBox.setHeight(24);

  }

}
