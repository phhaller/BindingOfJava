package objects;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Berry extends SuperObject {

  public Berry(int x, int y, boolean world, String type, GameWindowController gwc) {

    super(gwc);

    if (!world) {
      worldX = x * gwc.tileSize;
      worldY = y * gwc.tileSize;
    } else {
      worldX = x;
      worldY = y;
    }

    switch (type) {
      case "RED":
        name = "BERRYRED";
        image = gwc.imageManager.itemMap.get("BERRY").get(0);
        inventoryImage = gwc.imageManager.inventoryMap.get("BERRY").get(0);
        break;
      case "PURPLE":
        name = "BERRYPURPLE";
        image = gwc.imageManager.itemMap.get("BERRY").get(1);
        inventoryImage = gwc.imageManager.inventoryMap.get("BERRY").get(1);
        break;
      case "BLUE":
        name = "BERRYBLUE";
        image = gwc.imageManager.itemMap.get("BERRY").get(2);
        inventoryImage = gwc.imageManager.inventoryMap.get("BERRY").get(2);
        break;
      case "YELLOW":
        name = "BERRYYELLOW";
        image = gwc.imageManager.itemMap.get("BERRY").get(3);
        inventoryImage = gwc.imageManager.inventoryMap.get("BERRY").get(3);
        break;
    }

    createBerry();
    createHitBox();

  }


  private void createBerry() {

    rec.setWidth(32);
    rec.setHeight(32);
    rec.setTranslateX(worldX + 8);
    rec.setTranslateY(worldY + 8);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(8);
    hitBox.setTranslateY(8);
    hitBox.setWidth(32);
    hitBox.setHeight(32);

  }

}
