package objects;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Longsword extends SuperObject {

  public Longsword(int x, int y, boolean world, GameWindowController gwc) {

    super(gwc);

    if (!world) {
      worldX = x * gwc.tileSize;
      worldY = y * gwc.tileSize;
    } else {
      worldX = x;
      worldY = y;
    }

    name = "LONGSWORD";
    type = "SWORD";
    image = gwc.imageManager.swordMap.get("SWORD").get(2);
    // collision = true;

    damage = 15;
    dexterity = 3;
    defense = 0;
    durability = 10;
    maxDurability = durability;
    energyUse = 15;

    createLongsword();
    createHitBox();

  }


  private void createLongsword() {

    rec.setWidth(48);
    rec.setHeight(48);
    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(8);
    hitBox.setTranslateY(8);
    hitBox.setWidth(32);
    hitBox.setHeight(32);

  }

}
