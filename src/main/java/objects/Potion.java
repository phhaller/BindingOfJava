package objects;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Potion extends SuperObject {

  public Potion(int x, int y, boolean world, String type, GameWindowController gwc) {

    super(gwc);

    if (!world) {
      worldX = x * gwc.tileSize;
      worldY = y * gwc.tileSize;
    } else {
      worldX = x;
      worldY = y;
    }

    switch (type) {
      case "RED":
        name = "POTIONRED";
        image = gwc.imageManager.itemMap.get("POTION").get(0);
        inventoryImage = gwc.imageManager.inventoryMap.get("POTION").get(0);
        break;
      case "PURPLE":
        name = "POTIONPURPLE";
        image = gwc.imageManager.itemMap.get("POTION").get(1);
        inventoryImage = gwc.imageManager.inventoryMap.get("POTION").get(1);
        break;
      case "BLUE":
        name = "POTIONBLUE";
        image = gwc.imageManager.itemMap.get("POTION").get(2);
        inventoryImage = gwc.imageManager.inventoryMap.get("POTION").get(2);
        break;
      case "YELLOW":
        name = "POTIONYELLOW";
        image = gwc.imageManager.itemMap.get("POTION").get(3);
        inventoryImage = gwc.imageManager.inventoryMap.get("POTION").get(3);
        break;
    }

    createPotion();
    createHitBox();

  }


  private void createPotion() {

    rec.setWidth(24);
    rec.setHeight(24);
    rec.setTranslateX(worldX + 12);
    rec.setTranslateY(worldY + 12);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(12);
    hitBox.setTranslateY(12);
    hitBox.setWidth(24);
    hitBox.setHeight(24);

  }

}
