package objects;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Bow extends SuperObject {

  public Bow(int x, int y, boolean world, GameWindowController gwc) {

    super(gwc);

    if (!world) {
      worldX = x * gwc.tileSize;
      worldY = y * gwc.tileSize;
    } else {
      worldX = x;
      worldY = y;
    }

    name = "NORMALBOW";
    type = "BOW";
    image = gwc.imageManager.bowMap.get(name).get(0);
    // collision = true;

    damage = 20;
    dexterity = 3;
    defense = 0;
    durability = 10;
    maxDurability = durability;
    energyUse = 20;

    createBow();
    createHitBox();

  }


  private void createBow() {

    rec.setWidth(48);
    rec.setHeight(48);
    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(8);
    hitBox.setTranslateY(8);
    hitBox.setWidth(32);
    hitBox.setHeight(32);

  }

}
