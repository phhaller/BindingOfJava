package objects;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Axe extends SuperObject {

  public Axe(int x, int y, boolean world, GameWindowController gwc) {

    super(gwc);

    if (!world) {
      worldX = x * gwc.tileSize;
      worldY = y * gwc.tileSize;
    } else {
      worldX = x;
      worldY = y;
    }

    name = "AXE";
    type = "TOOL";
    image = gwc.imageManager.toolMap.get(name).get(3);
    // collision = true;

    damage = 5;
    dexterity = 1;
    defense = 0;
    durability = 20;
    maxDurability = durability;
    energyUse = 5;

    createAxe();
    createHitBox();

  }


  private void createAxe() {

    rec.setWidth(48);
    rec.setHeight(48);
    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(8);
    hitBox.setTranslateY(8);
    hitBox.setWidth(32);
    hitBox.setHeight(32);

  }

}
