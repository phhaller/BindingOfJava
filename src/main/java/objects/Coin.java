package objects;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Coin extends SuperObject {

  public Coin(int x, int y, boolean world, GameWindowController gwc) {

    super(gwc);

    if (!world) {
      worldX = x * gwc.tileSize;
      worldY = y * gwc.tileSize;
    } else {
      worldX = x;
      worldY = y;
    }

    name = "COIN";
    image = gwc.imageManager.itemMap.get("COIN").get(0);
    inventoryImage = gwc.imageManager.inventoryMap.get("COIN").get(0);

    createKey();
    createHitBox();

  }


  private void createKey() {

    rec.setWidth(24);
    rec.setHeight(24);
    rec.setTranslateX(worldX + 12);
    rec.setTranslateY(worldY + 12);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(12);
    hitBox.setTranslateY(12);
    hitBox.setWidth(24);
    hitBox.setHeight(24);

  }

}
