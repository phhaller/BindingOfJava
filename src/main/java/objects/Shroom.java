package objects;

import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Shroom extends SuperObject {

  public Shroom(int x, int y, boolean world, String type, GameWindowController gwc) {

    super(gwc);

    if (!world) {
      worldX = x * gwc.tileSize;
      worldY = y * gwc.tileSize;
    } else {
      worldX = x;
      worldY = y;
    }

    switch (type) {
      case "RED":
        name = "SHROOMRED";
        image = gwc.imageManager.itemMap.get("SHROOM").get(0);
        inventoryImage = gwc.imageManager.inventoryMap.get("SHROOM").get(0);
        break;
      case "BROWN":
        name = "SHROOMBROWN";
        image = gwc.imageManager.itemMap.get("SHROOM").get(1);
        inventoryImage = gwc.imageManager.inventoryMap.get("SHROOM").get(1);
        break;
    }

    createShroom();
    createHitBox();

  }


  private void createShroom() {

    rec.setWidth(24);
    rec.setHeight(24);
    rec.setTranslateX(worldX + 12);
    rec.setTranslateY(worldY + 12);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(2);
    hitBox.setTranslateY(2);
    hitBox.setWidth(20);
    hitBox.setHeight(20);

  }

}
