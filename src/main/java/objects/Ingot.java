package objects;

import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Ingot extends SuperObject {

  public Ingot(int x, int y, boolean world, String type, GameWindowController gwc) {

    super(gwc);

    if (!world) {
      worldX = x * gwc.tileSize;
      worldY = y * gwc.tileSize;
    } else {
      worldX = x;
      worldY = y;
    }

    switch (type) {
      case "IRON":
        name = "INGOTIRON";
        image = gwc.imageManager.itemMap.get("INGOT").get(0);
        inventoryImage = gwc.imageManager.inventoryMap.get("INGOT").get(0);
        break;
      case "GOLD":
        name = "INGOTGOLD";
        image = gwc.imageManager.itemMap.get("INGOT").get(1);
        inventoryImage = gwc.imageManager.inventoryMap.get("INGOT").get(1);
        break;
    }

    createIngot();
    createHitBox();

  }


  private void createIngot() {

    rec.setWidth(32);
    rec.setHeight(32);
    rec.setTranslateX(worldX + 8);
    rec.setTranslateY(worldY + 8);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(8);
    hitBox.setTranslateY(8);
    hitBox.setWidth(32);
    hitBox.setHeight(32);

  }

}
