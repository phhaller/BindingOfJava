package objects;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Chest extends SuperObject {

  public Chest(int x, int y, String type, GameWindowController gwc) {

    super(gwc);

    worldX = x * gwc.tileSize;
    worldY = y * gwc.tileSize;
    name = "CHEST";
    this.type = type;
    collision = true;

    createChest();
    createHitBox();

  }


  private void createChest() {

    if (type.equals("SINGLE")) {
      image = gwc.imageManager.containerMap.get("CHEST").get(0);
    } else if (type.equals("DOUBLE")) {
      image = gwc.imageManager.containerMap.get("CHEST").get(1);
    }

    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);
    rec.setFill(new ImagePattern(image));
  }


  private void createHitBox() {
    if (type.equals("SINGLE")) {
      hitBox.setTranslateX(8);
      hitBox.setTranslateY(4);
      hitBox.setWidth(32);
      hitBox.setHeight(40);
    }
  }


  public void open() {
    int items = type.equals("SINGLE") ? 3 : 5;
    for (int i = 0; i < items; i++) {
      gwc.objectsManager.getRandomObject(worldX, worldY);
    }
  }

}
