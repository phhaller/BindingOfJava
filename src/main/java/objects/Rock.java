package objects;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Rock extends SuperObject {

  public Rock(int x, int y, boolean world, GameWindowController gwc) {

    super(gwc);

    if (!world) {
      worldX = x * gwc.tileSize;
      worldY = y * gwc.tileSize;
    } else {
      worldX = x;
      worldY = y;
    }

    name = "ROCK";
    image = gwc.imageManager.itemMap.get("ROCK").get(0);
    inventoryImage = gwc.imageManager.inventoryMap.get("ROCK").get(0);

    createRock();
    createHitBox();

  }


  private void createRock() {

    rec.setWidth(32);
    rec.setHeight(32);
    rec.setTranslateX(worldX + 8);
    rec.setTranslateY(worldY + 8);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(8);
    hitBox.setTranslateY(8);
    hitBox.setWidth(32);
    hitBox.setHeight(32);

  }

}
