package objects;

import enemy.Bear;
import entity.Entity;
import map.Tile;
import view.GameWindowController;

import java.util.ArrayList;
import java.util.Random;

public class ObjectsManager {

  private GameWindowController gwc;
  public ArrayList<SuperObject> objects;

  private String[] potionTypes;
  private String[] toolTypes;
  private String[] swordTypes;
  private String[] allPotions;
  private String[] allObjects;


  public ObjectsManager(GameWindowController gwc) {

    this.gwc = gwc;

    toolTypes = new String[]{"AXE", "PICKAXE", "SHOVEL"};
    swordTypes = new String[]{"SABER", "LONGSWORD"};
    potionTypes = new String[]{"RED", "PURPLE", "BLUE", "YELLOW"};
    allPotions = new String[]{"POTIONRED", "POTIONPURPLE", "POTIONBLUE", "POTIONYELLOW"};
    allObjects = new String[]{"AXE", "PICKAXE", "SHOVEL", "SABER", "LONGSWORD", "BOW", "POTIONRED", "POTIONPURPLE",
        "POTIONBLUE", "POTIONYELLOW", "KEY", "COIN", "COIN"};

    createObjects();

  }


  private void createObjects() {

    objects = new ArrayList<>();

    int worldWidth = gwc.worldWidth;
    int worldHeight = gwc.worldHeight;
    int tileSize = gwc.tileSize;
    Tile[][] tiles = gwc.tileManager.getTiles();

    int axeCounter = 0;
    int pickaxeCounter = 0;
    int shovelCounter = 0;
    int saberCounter = 0;
    int longswordCounter = 0;
    int bowCounter = 0;
    int keyCounter = 0;
    int chestCounter = 0;
    int barrelCounter = 0;
    int potionCounter = 0;
    int coinCounter = 0;
    int meatCounter = 0;
    Random r = new Random();

/*
    // CREATE AXES
    for (int y = 0; y < worldHeight / tileSize; y++) {
      for (int x = 0; x < worldWidth / tileSize; x++) {

        if (tiles[x][y].getNoiseHeight() > .750 && tiles[x][y].getNoiseHeight() < .752 && !tiles[x][y].getOccupied()) {

          SuperObject object = new Axe(x, y, false, gwc);
          objects.add(object);
          axeCounter++;

        }

      }
    }


    // CREATE PICKAXES
    while (pickaxeCounter < 10) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        SuperObject object = new Pickaxe(tmp1, tmp2, false, gwc);
        objects.add(object);
        pickaxeCounter++;
        tiles[tmp1][tmp2].setOccupied(true);
      }

    }


    // CREATE SHOVELS
    while (shovelCounter < 8) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        SuperObject object = new Shovel(tmp1, tmp2, false, gwc);
        objects.add(object);
        shovelCounter++;
        tiles[tmp1][tmp2].setOccupied(true);
      }

    }


    // CREATE SABERS
    while (saberCounter < 8) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        SuperObject object = new Saber(tmp1, tmp2, false, gwc);
        objects.add(object);
        saberCounter++;
        tiles[tmp1][tmp2].setOccupied(true);
      }

    }


    // CREATE LONGSWORDS
    while (longswordCounter < 8) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        SuperObject object = new Longsword(tmp1, tmp2, false, gwc);
        objects.add(object);
        longswordCounter++;
        tiles[tmp1][tmp2].setOccupied(true);
      }

    }


    // CREATE BOWS
    while (bowCounter < 5) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        SuperObject object = new Bow(tmp1, tmp2, false, gwc);
        objects.add(object);
        bowCounter++;
        tiles[tmp1][tmp2].setOccupied(true);
      }

    }*/


    // CREATE KEYS
    while (keyCounter < 10) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        SuperObject object = new Key(tmp1, tmp2, false, gwc);
        objects.add(object);
        keyCounter++;
        tiles[tmp1][tmp2].setOccupied(true);
      }

    }


    // CREATE CHESTS
    while (chestCounter < 8) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;
      int tmp3 = r.nextInt(2);

      if (!tiles[tmp1][tmp2].getOccupied()) {
        String type = tmp3 == 0 ? "SINGLE" : "DOUBLE";
        SuperObject object = new Chest(tmp1, tmp2, type, gwc);
        objects.add(object);
        chestCounter++;
        tiles[tmp1][tmp2].setOccupied(true);
      }

    }


    // CREATE BARRELS
    while (barrelCounter < 4) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;
      int tmp3 = r.nextInt(2);

      if (!tiles[tmp1][tmp2].getOccupied()) {
        String type = tmp3 == 0 ? "UP" : "DOWN";
        SuperObject object = new Barrel(tmp1, tmp2, type, gwc);
        objects.add(object);
        barrelCounter++;
        tiles[tmp1][tmp2].setOccupied(true);
      }

    }


    // CREATE POTIONS
    while (potionCounter < 10) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;
      int tmp3 = r.nextInt(4);

      if (!tiles[tmp1][tmp2].getOccupied()) {
        SuperObject object = new Potion(tmp1, tmp2, false, potionTypes[tmp3], gwc);
        objects.add(object);
        potionCounter++;
        tiles[tmp1][tmp2].setOccupied(true);
      }

    }


    // CREATE COINS
    while (coinCounter < 30) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        SuperObject object = new Coin(tmp1, tmp2, false, gwc);
        objects.add(object);
        coinCounter++;
        tiles[tmp1][tmp2].setOccupied(true);
      }

    }

  }


  public void draw() {

    for (SuperObject object : objects) {
      object.draw();
    }

  }


  public void getRandomObject(int x, int y) {

    SuperObject object = null;

    Random r = new Random();
    int tmp = r.nextInt(allObjects.length);
    int xOffset = r.nextInt(32 + 30) - 30;
    int yOffset = r.nextInt(32 + 30) - 30;

    String s = allObjects[tmp];

    switch (s) {
      case "AXE":
        object = new Axe(x + xOffset, y + yOffset, true, gwc);
        break;
      case "PICKAXE":
        object = new Pickaxe(x + xOffset, y + yOffset, true, gwc);
        break;
      case "SHOVEL":
        object = new Shovel(x + xOffset, y + yOffset, true, gwc);
        break;
      case "SABER":
        object = new Saber(x + xOffset, y + yOffset, true, gwc);
        break;
      case "LONGSWORD":
        object = new Longsword(x + xOffset, y + yOffset, true, gwc);
        break;
      case "BOW":
        object = new Bow(x + xOffset, y + yOffset, true, gwc);
        break;
      case "POTIONRED":
        object = new Potion(x + xOffset, y + yOffset, true, "RED", gwc);
        break;
      case "POTIONPURPLE":
        object = new Potion(x + xOffset, y + yOffset, true, "PURPLE", gwc);
        break;
      case "POTIONBLUE":
        object = new Potion(x + xOffset, y + yOffset, true, "BLUE", gwc);
        break;
      case "POTIONYELLOW":
        object = new Potion(x + xOffset, y + yOffset, true, "YELLOW", gwc);
        break;
      case "KEY":
        object = new Key(x + xOffset, y + yOffset, true, gwc);
        break;
      case "COIN":
        object = new Coin(x + xOffset, y + yOffset, true, gwc);
        break;
    }

    if (object != null) {
      objects.add(object);
    }

  }


  public void getRandomPotion(int x, int y) {

    SuperObject object = null;

    Random r = new Random();
    int tmp = r.nextInt(allPotions.length);
    int xOffset = r.nextInt(32 + 30) - 30;
    int yOffset = r.nextInt(32 + 30) - 30;

    String s = allPotions[tmp];

    switch (s) {
      case "POTIONRED":
        object = new Potion(x + xOffset, y + yOffset, true, "RED", gwc);
        break;
      case "POTIONPURPLE":
        object = new Potion(x + xOffset, y + yOffset, true, "PURPLE", gwc);
        break;
      case "POTIONBLUE":
        object = new Potion(x + xOffset, y + yOffset, true, "BLUE", gwc);
        break;
      case "POTIONYELLOW":
        object = new Potion(x + xOffset, y + yOffset, true, "YELLOW", gwc);
        break;
    }

    if (object != null) {
      objects.add(object);
    }

  }


  public void getRandomMobDrops(int x, int y, ArrayList<String> drops) {

    Random r = new Random();

    for (String d : drops) {
      int amount = r.nextInt(3);

      for (int i = 0; i < amount; i++) {

        SuperObject object = null;

        int xOffset = r.nextInt(32 + 30) - 30;
        int yOffset = r.nextInt(32 + 30) - 30;

        switch (d) {
          case "CHICKENLEG":
          case "PORKCHOP":
          case "RIBS":
          case "STEAK":
            object = new Meat(x + xOffset, y + yOffset, true, d, false, gwc);
            break;
          case "FEATHER":
            object = new Feather(x + xOffset, y + yOffset, true, gwc);
            break;
          case "BONE":
            object = new Bone(x + xOffset, y + yOffset, true, gwc);
            break;
          case "ACORN":
            object = new Acorn(x + xOffset, y + yOffset, true, gwc);
            break;
        }

        if (object != null) {
          objects.add(object);
        }

      }

    }

  }


  public ArrayList<SuperObject> getObjects() {
    return objects;
  }

}
