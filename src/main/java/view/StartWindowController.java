package view;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import util.JSONManager;

import java.io.IOException;

public class StartWindowController {

  @FXML
  private AnchorPane background;

  @FXML
  private Label newGameLabel, loadGameLabel, helpLabel, quitLabel;


  private Scene scene;
  private JSONManager jsonManager = new JSONManager();
  private AnchorPane loadMenu;
  private final int screenWidth = 1280;
  private final int screenHeight = 800;

  public void initialize(Scene scene) {

    this.scene = scene;
    createHandlers();

  }

  private void createHandlers() {

    newGameLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        switchToGameWindow("");
      }
    });

    loadGameLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        switchToLoadGameWindow();
      }
    });

    helpLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        switchToHelpWindow();
      }
    });

    quitLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        Platform.exit();
      }
    });

  }


  private void switchToGameWindow(String filePath) {

    try {

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/gameWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) background.getScene().getWindow();

      GameWindowController controller = loader.getController();
      controller.initialize(scene, filePath);

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }


  private void switchToLoadGameWindow() {

    try {

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/loadGameWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) background.getScene().getWindow();

      LoadGameWindowController controller = loader.getController();
      controller.initialize(scene);

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }


  private void switchToHelpWindow() {

    try {

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/helpWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) background.getScene().getWindow();

      HelpWindowController controller = loader.getController();
      controller.initialize(scene);

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

}
