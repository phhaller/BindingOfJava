package view;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.io.IOException;

public class HelpWindowController {

  @FXML
  private AnchorPane background;

  private Scene scene;
  private Rectangle backRec;
  private final int screenWidth = 1280;
  private final int screenHeight = 800;

  public void initialize(Scene scene) {

    this.scene = scene;

    createBackRec();
    createHandlers();

  }


  private void createBackRec() {

    backRec = new Rectangle(30, 30, 32, 26);
    backRec.getStyleClass().add("backRec");

    background.getChildren().add(backRec);

  }


  private void createHandlers() {

    backRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        switchToStartWindow();
      }
    });

  }


  private void switchToStartWindow() {

    try {

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/startWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) background.getScene().getWindow();

      StartWindowController controller = loader.getController();
      controller.initialize(scene);

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

}
