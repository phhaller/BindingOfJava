package view;

import building.BuildingManager;
import collision.CollisionManager;
import enemy.EnemyManager;
import entity.NPCManager;
import javafx.animation.AnimationTimer;
import javafx.animation.FadeTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import map.*;
import objects.*;
import entity.Player;
import pathfinding.PathfindingManager;
import projectiles.ProjectileManager;
import ui.UIManager;
import uiControllers.EquipmentManager;
import uiControllers.InventoryManager;
import uiControllers.ScrollingMessageManager;
import util.*;

import java.io.IOException;
import java.util.ArrayList;

public class GameWindowController {

  @FXML
  private AnchorPane background;

  public Scene scene;
  public JSONManager jsonManager = new JSONManager();
  public String worldName = "NewWorld";

  public final int worldWidth = 7680;
  public final int worldHeight = 4800;
  public final int screenWidth = 1280;
  public final int screenHeight = 800;
  public int tileSize = 48;

  public KeyHandler keyHandler;
  public ImageManager imageManager;
  public ScrollingMessageManager scrollingMessageManager;
  public EquipmentManager equipmentManager;
  public InventoryManager inventoryManager;
  public ProjectileManager projectileManager;
  public PathfindingManager pathfindingManager;

  public TileManager tileManager;
  public VegetationManager vegetationManager;
  public Player player;

  public ObjectsManager objectsManager;
  public ArrayList<SuperObject> objects;

  public NPCManager npcManager;
  public EnemyManager enemyManager;

  public CollisionManager collisionManager;

  public AnimationTimer animationTimer;

  public BuildingManager buildingManager;

  // UI
  public UIManager uiManager;

  // GAME STATE
  public int gameState;
  public final int titleState = 0;
  public final int playState = 1;
  public final int pauseState = 2;
  public final int dialogState = 3;
  public final int equipmentState = 4;
  public final int inventoryState = 5;
  public final int craftingState = 6;
  public final int buildingState = 7;

  public int machineState;
  public final int anvilState = 0;
  public final int lumberState = 1;
  public final int brewerState = 2;
  public final int campfireState = 3;

  public int npcState;
  public final int merchantState = 0;
  public final int blacksmithState = 1;


  public void initialize(Scene scene, String filePath) {

    this.scene = scene;

    if (filePath.isEmpty()) {

      setUpGame();

      FadeTransition ft = new FadeTransition(Duration.millis(1000), background);
      ft.setFromValue(0);
      ft.setToValue(1);
      ft.play();

      gameState = playState;
      createGameLoop();

    } else {
      loadGame(filePath, worldWidth/tileSize, worldHeight/tileSize);
    }

  }


  private void setUpGame() {

    createKeyHandler();
    createImageManager();
    createScrollingMessages();
    createEquipment();
    createInventory();
    createProjectiles();
    createPathfinding();
    createCollision();
    createTiles();
    createPlayer();
    createVegetation();
    createObjects();
    createNPCs();
    createEnemies();
    createBuilding();
    createUI();
    createHandlers();

  }


  private void loadGame(String filePath, int cols, int rows) {

    // jsonManager.loadFile(filePath, cols, rows);

    // loadTiles(jsonManager.getTiles());

  }


  private void createKeyHandler() {
    keyHandler = new KeyHandler(this);
  }

  private void createImageManager() {
    imageManager = new ImageManager();
  }

  private void createScrollingMessages() {
    scrollingMessageManager = new ScrollingMessageManager();
  }

  private void createEquipment() {
    equipmentManager = new EquipmentManager(this);
    equipmentManager.addTool(new Axe(worldWidth/2, worldHeight/2, false, this));
    equipmentManager.addTool(new Pickaxe(worldWidth/2, worldHeight/2, false, this));
    equipmentManager.addTool(new Shovel(worldWidth/2, worldHeight/2, false, this));
    equipmentManager.addSword(new Longsword(worldWidth/2, worldHeight/2, false, this));
    equipmentManager.addBow(new Bow(worldWidth/2, worldHeight/2, false, this));
  }

  private void createInventory() {
    inventoryManager = new InventoryManager(this);
  }

  private void createProjectiles() {
    projectileManager = new ProjectileManager(this);
  }

  private void createPathfinding() {
    pathfindingManager = new PathfindingManager(this);
  }

  private void createCollision() {
    collisionManager = new CollisionManager(this);
  }

  private void createTiles() {
    tileManager = new TileManager(this, null);
    background.getChildren().add(tileManager.getCanvas());
  }

  private void loadTiles(Tile[][] tiles) {
    tileManager = new TileManager(this, tiles);
    background.getChildren().add(tileManager.getCanvas());
  }


  private void createPlayer() {
    player = new Player(this, scene);
  }


  private void createVegetation() {
    vegetationManager = new VegetationManager(this, tileManager.getTiles());
  }


  private void createObjects() {
    objectsManager = new ObjectsManager(this);
    objects = objectsManager.objects;
  }


  private void createNPCs() {
    npcManager = new NPCManager(this);
  }


  private void createEnemies() {
    enemyManager = new EnemyManager(this);
  }


  private void createUI() {
    uiManager = new UIManager(this);
  }


  private void createBuilding() {
    buildingManager = new BuildingManager(this);
  }


  private void createHandlers() {

    /*scene.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        int mouseX = (int) event.getX();
        int mouseY = (int) event.getY();
        int playerScreenX = player.screenX;
        int playerScreenY = player.screenY;
        int playerX = player.worldX;
        int playerY = player.worldY;

        int mouseScreenX = mouseX < playerScreenX ? (playerX - (playerScreenX - mouseX)) / tileSize : (playerX + (mouseX - playerScreenX)) / tileSize;
        int mouseScreenY = mouseY < playerScreenY ? (playerY - (playerScreenY - mouseY)) / tileSize : (playerY + (mouseY - playerScreenY)) / tileSize;

        System.out.println(mouseScreenX + " _ " + mouseScreenY);

      }
    });*/

  }


  private void createGameLoop() {

    animationTimer = new AnimationTimer() {
      @Override
      public void handle(long now) {

        update();
        draw();

      }
    };

    animationTimer.start();

  }


  private void update() {

    if (gameState == playState) {

      // Player
      player.update();

      // Machines
      buildingManager.update();

      // Projectiles
      projectileManager.update();

      // NPCs
      npcManager.update();

      // Enemies
      enemyManager.update();

      // UI
      uiManager.update();

    }

    if (gameState == pauseState) {}

    if (gameState == dialogState) {}

    if (gameState == equipmentState) {}

    if (gameState == inventoryState) {}

    if (gameState == craftingState) {}

    if (gameState == buildingState) {}

  }


  private void draw() {

    background.getChildren().remove(uiManager.scrollingMessageWindow);

    if (gameState == titleState) {}

    if (gameState == playState) {
      // Remove
      background.getChildren().removeAll(player.getPlayer(), uiManager.pauseWindow, uiManager.dialogWindow,
          uiManager.titleWindow, uiManager.currentEquipmentDisplay, uiManager.healthBar, uiManager.energyBar,
          uiManager.equipmentWindow, uiManager.inventoryWindow, uiManager.buildingWindow);

      // Tiles
      tileManager.draw();

      // Vegetation
      vegetationManager.draw();

      // Building
      buildingManager.draw();

      // Objects
      objectsManager.draw();

      // NPCs
      npcManager.draw();

      // Enemies
      enemyManager.draw();

      // Projectiles
      projectileManager.draw();

      // Add entity and trees
      background.getChildren().addAll(player.getPlayer());
      vegetationManager.drawTress();

    }


    if (gameState == pauseState) {}

    if (gameState == dialogState) {}

    if (gameState == equipmentState) {}

    if (gameState == inventoryState) {}

    if (gameState == craftingState) {}

    if (gameState == buildingState) {}

    uiManager.draw();

  }


  public void gameOver() {

    stopCurrentGame();
    switchToGameOverWindow();

  }


  public void stopCurrentGame() {

    animationTimer.stop();
    background.getChildren().clear();
    System.gc();

  }


  private void switchToGameOverWindow() {

    try {

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/gameOverWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) background.getScene().getWindow();

      GameOverWindowController controller = loader.getController();
      controller.initialize(scene);

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }


  public AnchorPane getBackground() {
    return background;
  }

}
