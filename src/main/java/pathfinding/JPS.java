package pathfinding;

import javafx.scene.layout.AnchorPane;
import map.Tile;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JPS {

  private Tile[][] tiles;
  private Tile start;
  private Tile end;
  private ArrayList<Tile> closedList = new ArrayList<>();
  private ArrayList<Tile> openList = new ArrayList<>();


  public JPS(Tile[][] tiles, Tile start, Tile end) {

    this.tiles = tiles;
    this.start = start;
    this.end = end;

  }


  // Start a search without animation.
  public LinkedList<Tile> start() {

    LinkedList<Tile> path = null;

    openList.add(start);
    start.setParent(null);

    while (!openList.isEmpty()) {

      Tile curTile = getTileWithLowestFCost();
      openList.remove(curTile);
      closedList.add(curTile);

      if (curTile == end) {
        path = retracePath(tiles, curTile);
      }

      identifySuccessors(curTile);

    }

    return path;

  }


  // Get the node with the lowest f cost from the open list.
  private Tile getTileWithLowestFCost() {
    Tile lowestTile = openList.get(0);

    for (Tile n : openList) {
      if (n.getfCost() < lowestTile.getfCost() || n.getfCost() == lowestTile.getfCost() && n.gethCost() < lowestTile.gethCost()) {
        lowestTile = n;
      }
    }
    return lowestTile;
  }


  // Identify jump points, set their costs, and add them to the open list.
  private void identifySuccessors(Tile tile) {

    ArrayList<Tile> neighbours = getNeighboursPruned(tile);

    for (Tile n : neighbours) {

      Tile jumpPoint = jump(n.getPathX(), n.getPathY(), tile.getPathX(), tile.getPathY());

      if (jumpPoint != null) {

        if (closedList.contains(jumpPoint)) {
          continue;
        }

        double distance = getDistance(jumpPoint, tile);
        double newCostToJumpPoint = tile.getgCost() + distance;

        if (!openList.contains(jumpPoint) || newCostToJumpPoint < jumpPoint.getgCost()) {

          jumpPoint.setgCost(newCostToJumpPoint);
          jumpPoint.sethCost(getDistance(jumpPoint, end));
          jumpPoint.setParent(tile);

          if (!openList.contains(jumpPoint)) {
            openList.add(jumpPoint);
          }

        }

      }

    }

  }


  // Gets the neighbours of a node according to the diagonal strategy.
  private ArrayList<Tile> getNeighboursPruned(Tile tile) {

    ArrayList<Tile> neighbours = new ArrayList<>();

    Tile parent = tile.getParent();
    int x = tile.getPathX();
    int y = tile.getPathY();
    int px, py, dx, dy;

    if (parent != null) {

      px = parent.getPathX();
      py = parent.getPathY();

      dx = (x - px) / Math.max(Math.abs(x - px), 1);
      dy = (y - py) / Math.max(Math.abs(y - py), 1);

      if (dx != 0) {
        if (isWalkable(x + dx, y)) {
          neighbours.add(tiles[x + dx][y]);
        }

        if (isWalkable(x, y + 1)) {
          neighbours.add(tiles[x][y + 1]);
        }

        if (isWalkable(x, y - 1)) {
          neighbours.add(tiles[x][y - 1]);
        }

      } else if (dy != 0) {
        if (isWalkable(x, y + dy)) {
          neighbours.add(tiles[x][y + dy]);
        }

        if (isWalkable(x + 1, y)) {
          neighbours.add(tiles[x + 1][y]);
        }

        if (isWalkable(x - 1, y)) {
          neighbours.add(tiles[x - 1][y]);
        }

      }

    } else {
      return getNeighbours(tiles, tile);
    }

    return neighbours;

  }

  // Recursively searches for the next jump point according to the diagonal strategy.
  private Tile jump(int x, int y, int px, int py) {

    int dx = (x - px) / Math.max(Math.abs(x - px), 1);
    int dy = (y - py) / Math.max(Math.abs(y - py), 1);

    if (!isWalkable(x, y)) {
      return null;
    }

    if (x == end.getPathX() && y == end.getPathY()) {
      return tiles[x][y];
    }

    if (dx != 0) {
      if ((isWalkable(x, y + 1) && !isWalkable(x - dx, y + 1)) ||
          (isWalkable(x, y - 1) && !isWalkable(x - dx, y - 1))) {
        return tiles[x][y];
      }
    } else if (dy != 0) {
      if ((isWalkable(x + 1, y) && !isWalkable(x + 1, y - dy)) ||
          (isWalkable(x - 1, y) && !isWalkable(x - 1, y - dy))) {
        return tiles[x][y];
      }

      if (jump(x + 1, y, x, y) != null ||
          jump(x - 1, y, x, y) != null) {
        return tiles[x][y];
      }
    } else {
      return null;
    }

    return jump(x + dx, y + dy, x, y);

  }


  protected boolean isWalkable(int x, int y) {
    return x >= 0 && y >= 0 && x < tiles.length && y < tiles[0].length && !tiles[x][y].getOccupied();
  }

  // Get the distance between two nodes according to the set heuristics
  private double getDistance(Tile a, Tile b) {

    int dx = Math.abs(a.getPathX() - b.getPathX());
    int dy = Math.abs(a.getPathY() - b.getPathY());
    return dx + dy;

  }

  private ArrayList<Tile> getNeighbours(Tile[][] tiles, Tile tile) {

    ArrayList<Tile> neighbours = new ArrayList<>();
    int x = tile.getPathX();
    int y = tile.getPathY();

    if (isWalkable(x, y - 1)) {
      neighbours.add(tiles[x][y - 1]);
    }

    if (isWalkable(x + 1, y)) {
      neighbours.add(tiles[x + 1][y]);
    }

    if (isWalkable(x, y + 1)) {
      neighbours.add(tiles[x][y+1]);
    }

    if (isWalkable(x - 1, y)) {
      neighbours.add(tiles[x-1][y]);
    }

    return neighbours;

  }

  private LinkedList<Tile> retracePath(Tile[][] tiles, Tile node) {
    LinkedList<Tile> path = new LinkedList<>();
    Tile current;
    Tile parent;
    while (node.getParent() != null) {
      current = node;
      parent = node.getParent();

      if (!path.contains(node)) {
        path.addFirst(node);
      }

      if (parent.getPathX() == current.getPathX()) {
        int dist = parent.getPathY() - current.getPathY();
        int sign = dist > 0 ? 1 : -1;
        dist = Math.abs(dist);
        for (int i = 0; i < dist; i++) {
          if (!path.contains(tiles[current.getPathX()][current.getPathY() + i * sign])) {
            path.addFirst(tiles[current.getPathX()][current.getPathY() + i * sign]);
          }
        }
      } else if (parent.getPathY() == current.getPathY()) {
        int dist = parent.getPathX() - current.getPathX();
        int sign = dist > 0 ? 1 : -1;
        dist = Math.abs(dist);
        for (int i = 0; i < dist; i++) {
          if (!path.contains(tiles[current.getPathX() + i * sign][current.getPathY()])) {
            path.addFirst(tiles[current.getPathX() + i * sign][current.getPathY()]);
          }
        }
      }

      node = parent;
    }

    return path;
  }


}
