package pathfinding;

import entity.Entity;
import map.Tile;
import view.GameWindowController;

import java.util.LinkedList;

public class PathfindingManager {

  private GameWindowController gwc;

  public PathfindingManager(GameWindowController gwc) {
    this.gwc = gwc;
  }


  public LinkedList<Tile> findPath(Entity entity, Entity target) {

    int entityX = (int) Math.ceil(entity.worldX / gwc.tileSize);
    int entityY = (int) Math.ceil(entity.worldY / gwc.tileSize);
    int targetX = (int) Math.ceil(target.worldX / gwc.tileSize);
    int targetY = (int) Math.ceil(target.worldY / gwc.tileSize);

    Tile[][] tiles = gwc.tileManager.getTiles();
    Tile start = null;
    Tile end = null;

    Tile[][] tilesScreen = new Tile[28][18];

    for (int j = 0; j < 18; j++) {
      for (int i = 0; i < 28; i++) {

        int xIndex = gwc.player.worldX/gwc.tileSize - 13 + i;
        if (xIndex < 0) { xIndex = 0; }
        if (xIndex > tiles.length - 1) { xIndex = tiles.length - 1; }

        int yIndex = gwc.player.worldY/gwc.tileSize - 8 + j;
        if (yIndex < 0) { yIndex = 0; }
        if (yIndex > tiles[0].length - 1) { yIndex = tiles[0].length - 1; }

        tilesScreen[i][j] = tiles[xIndex][yIndex];
        tilesScreen[i][j].setPathX(i * gwc.tileSize);
        tilesScreen[i][j].setPathY(j * gwc.tileSize);
        if (tilesScreen[i][j].getX() == entityX && tilesScreen[i][j].getY() == entityY) {
          start = tilesScreen[i][j];
        }
        if (tilesScreen[i][j].getX() == targetX && tilesScreen[i][j].getY() == targetY) {
          end = tilesScreen[i][j];
        }

      }
    }

    JPS jps = new JPS(tilesScreen, start, end);
    return jps.start();

  }

}
