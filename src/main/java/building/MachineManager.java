package building;

import view.GameWindowController;

public class MachineManager {

  private GameWindowController gwc;
  public Building[][] machines;


  public MachineManager(GameWindowController gwc) {
    this.gwc = gwc;
    machines = new Building[gwc.worldWidth/gwc.tileSize][gwc.worldHeight/gwc.tileSize];
  }


  public void addMachine(int x, int y, String type) {

    Building machine = null;

    switch (type) {
      case "ANVIL":
        machine = new Anvil(x, y, gwc);
        break;
      case "LUMBER":
        machine = new Lumber(x, y, gwc);
        break;
      case "BREWER":
        machine = new Brewer(x, y, gwc);
        break;
      case "CAMPFIRE":
        machine = new Campfire(x, y, gwc);
        break;
    }

    machines[x][y] = machine;
    gwc.tileManager.getTiles()[x][y].setOccupied(true);

  }


  public void removeMachine(int x, int y) {
    gwc.getBackground().getChildren().remove(machines[x][y].rec);
    machines[x][y] = null;
  }


  public void update() {

    for (int y = 0; y < gwc.worldHeight / gwc.tileSize; y++) {
      for (int x = 0; x < gwc.worldWidth / gwc.tileSize; x++) {

        Building machine = machines[x][y];

        if (machine != null) {
          machine.update();
        }

      }
    }

  }


  public void draw() {

    for (int y = 0; y < gwc.worldHeight / gwc.tileSize; y++) {
      for (int x = 0; x < gwc.worldWidth / gwc.tileSize; x++) {

        Building machine = machines[x][y];

        if (machine != null) {
          machine.draw();
        }

      }
    }

  }



  public Building[][] getMachines() {
    return machines;
  }

}
