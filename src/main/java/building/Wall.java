package building;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Wall extends Building {

  public Wall(int x, int y, String type, GameWindowController gwc) {

    super(gwc);

    worldX = x * gwc.tileSize;
    worldY = y * gwc.tileSize;
    name = "WALL";
    this.type = type;
    life = 25;
    maxLife = life;
    collision = true;

    createWall();
    createHitBox();

  }


  private void createWall() {
    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);
  }


  private void createHitBox() {
    hitBox.setTranslateX(0);
    hitBox.setTranslateY(0);
    hitBox.setWidth(48);
    hitBox.setHeight(48);
  }

  public void setImage(Image image) {
    this.image = image;
    rec.setFill(new ImagePattern(image));
  }

}
