package building;

import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Glow;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Anvil extends Building {

  private Glow glow;
  private DropShadow dropShadow;

  public Anvil(int x, int y, GameWindowController gwc) {

    super(gwc);

    worldX = x * gwc.tileSize;
    worldY = y * gwc.tileSize;

    name = "ANVIL";
    life = 15;
    maxLife = life;
    collision = true;

    image = gwc.imageManager.machineMap.get("ANVIL").get(0);
    inventoryImage = gwc.imageManager.inventoryMap.get("MACHINE").get(0);

    createMachine();
    createHitBox();
    createEffects();

  }


  private void createMachine() {

    rec.setWidth(48);
    rec.setHeight(48);
    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(8);
    hitBox.setTranslateY(0);
    hitBox.setWidth(32);
    hitBox.setHeight(32);

  }


  private void createEffects() {

    glow = new Glow();
    glow.setLevel(.2);

    dropShadow = new DropShadow();
    dropShadow.setRadius(5.0);
    dropShadow.setOffsetX(3.0);
    dropShadow.setOffsetY(3.0);
    dropShadow.setColor(Color.color(0.4, 0.5, 0.5));

  }


  public void update() {

    boolean inRange = gwc.collisionManager.checkInRange(this, gwc.player, 100);

    if (inRange) {

      rec.setEffect(dropShadow);

    } else {

      rec.setEffect(null);

    }

  }

}
