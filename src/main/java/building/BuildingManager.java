package building;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import view.GameWindowController;


public class BuildingManager {

  private GameWindowController gwc;

  public String currentType = "";

  public WallManager wallManager;
  public MachineManager machineManager;
  private int curX = 0;
  private int curY = 0;

  private Rectangle buildingChecker;
  public boolean placeable = false;

  public BuildingManager(GameWindowController gwc) {
    this.gwc = gwc;
    wallManager = new WallManager(gwc);
    machineManager = new MachineManager(gwc);

    createBuildingChecker();
    createHandler();
  }


  private void createBuildingChecker() {
    buildingChecker = new Rectangle(gwc.tileSize, gwc.tileSize);
  }


  private void createHandler() {

    gwc.getBackground().addEventHandler(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        int mouseX = (int) event.getX();
        int mouseY = (int) event.getY();
        int playerScreenX = gwc.player.screenX;
        int playerScreenY = gwc.player.screenY;
        int playerX = gwc.player.worldX;
        int playerY = gwc.player.worldY;

        int mouseScreenX = mouseX < playerScreenX ? (playerX - (playerScreenX - mouseX)) / gwc.tileSize : (playerX + (mouseX - playerScreenX)) / gwc.tileSize;
        int mouseScreenY = mouseY < playerScreenY ? (playerY - (playerScreenY - mouseY)) / gwc.tileSize : (playerY + (mouseY - playerScreenY)) / gwc.tileSize;

        if (mouseScreenX != curX) {
          curX = mouseScreenX;
        }

        if (mouseScreenY != curY) {
          curY = mouseScreenY;
        }

      }
    });

    gwc.getBackground().addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (placeable) {

          if (currentType.equals("WALL")) {
            wallManager.addWall(curX, curY);
          }

          if (currentType.equals("ANVIL")) {
            machineManager.addMachine(curX, curY, currentType);
          }

          if (currentType.equals("LUMBER")) {
            machineManager.addMachine(curX, curY, currentType);
          }

          if (currentType.equals("BREWER")) {
            machineManager.addMachine(curX, curY, currentType);
          }

          if (currentType.equals("CAMPFIRE")) {
            machineManager.addMachine(curX, curY, currentType);
          }
        }
      }
    });

  }


  public void update() {

    machineManager.update();

  }


  public void draw() {

    gwc.getBackground().getChildren().remove(buildingChecker);
    wallManager.draw();
    machineManager.draw();

    if (gwc.player.building) {

      int screenX = curX * gwc.tileSize - gwc.player.worldX + gwc.player.screenX;
      int screenY = curY * gwc.tileSize - gwc.player.worldY + gwc.player.screenY;

      buildingChecker.setTranslateX(screenX);
      buildingChecker.setTranslateY(screenY);

      if (curX >= 0 && curX < gwc.tileManager.getTiles().length &&
          curY >= 0 && curY < gwc.tileManager.getTiles()[0].length) {
        if (gwc.tileManager.getTiles()[curX][curY].getOccupied()) {
          buildingChecker.setStyle("-fx-fill: rgba(255, 0, 0, .1); -fx-stroke: rgba(255, 0, 0, .4)");
          placeable = false;
        } else {
          buildingChecker.setStyle("-fx-fill: rgba(0, 255, 0, .1); -fx-stroke: rgba(0, 255, 0, .4)");
          placeable = true;
        }
      } else {
        placeable = false;
      }

      gwc.getBackground().getChildren().add(buildingChecker);

    }

  }


}
