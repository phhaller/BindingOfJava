package building;

import view.GameWindowController;

public class WallManager {

  private GameWindowController gwc;
  public Building[][] walls;


  public WallManager(GameWindowController gwc) {
    this.gwc = gwc;
    walls = new Building[gwc.worldWidth/gwc.tileSize][gwc.worldHeight/gwc.tileSize];
  }


  public void addWall(int x, int y) {

    Building wall = new Wall(x, y, "WOOD", gwc);

    Building top = walls[x][y-1];
    Building left = walls[x-1][y];
    Building bottom = walls[x][y+1];
    Building right = walls[x+1][y];

    if (top == null && left == null && bottom == null && right == null) {
      wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(0));
    }


    // LEFT and RIGHT
    if (left != null) {

      if (right != null) {
        wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(1));
        if (walls[x+2][y] == null) {
          if (walls[x+1][y+1] == null) {
            right.setImage(gwc.imageManager.wallMap.get("WOOD").get(2));
          }
        }
      } else {
        wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(2));
      }

      if (walls[x-2][y] != null) {
        if (walls[x-1][y+1] == null && walls[x-1][y-1] == null) {
          left.setImage(gwc.imageManager.wallMap.get("WOOD").get(1));
        }
      }

      if (walls[x+2][y] != null) {
        if (walls[x+1][y+1] == null && walls[x+1][y-1] == null) {
          if (right != null) {
            right.setImage(gwc.imageManager.wallMap.get("WOOD").get(1));
          }
        }
      }

    } else {

      wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(0));
      if (right != null) {
        if (walls[x+2][y] == null) {
          if (walls[x+1][y-1] == null && walls[x+1][y+1] == null) {
            right.setImage(gwc.imageManager.wallMap.get("WOOD").get(2));
          } else {
            if (walls[x+1][y+1] == null) {
              right.setImage(gwc.imageManager.wallMap.get("WOOD").get(2));
            } else {
              right.setImage(gwc.imageManager.wallMap.get("WOOD").get(5));
            }
          }
        } else {
          if (walls[x+1][y+1] == null && walls[x+1][y-1] == null) {
            right.setImage(gwc.imageManager.wallMap.get("WOOD").get(1));
          }
        }
      }

    }


    // TOP and BOTTOM
    if (top != null) {

      if (bottom != null) {

        if (right == null && left == null) {
          wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(3));
        } else if (right == null) {
          wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(5));
        } else {
          wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(4));
        }

      } else {
        if (walls[x+1][y+1] == null && walls[x-1][y+1] == null) {
          wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(0));
        } else if (walls[x+1][y+1] == null) {
          wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(2));
        } else {
          wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(0));
        }
      }

      if (walls[x-1][y-1] == null && walls[x+1][y-1] == null) {
        top.setImage(gwc.imageManager.wallMap.get("WOOD").get(4));
      } else if (walls[x+1][y-1] == null) {
        top.setImage(gwc.imageManager.wallMap.get("WOOD").get(5));
      } else {
        top.setImage(gwc.imageManager.wallMap.get("WOOD").get(4));
      }

      if (walls[x][y-2] != null) {
        if (walls[x-1][y-1] == null && walls[x+1][y-1] == null) {
          top.setImage(gwc.imageManager.wallMap.get("WOOD").get(3));
        }
      }

      if (walls[x][y+2] == null) {
        if (walls[x+1][y+1] == null && walls[x-1][y+1] == null) {
          if (bottom != null) {
            bottom.setImage(gwc.imageManager.wallMap.get("WOOD").get(1));
          }
        }
      } else {
        if (walls[x+1][y+1] == null && walls[x-1][y+1] == null) {
          if (bottom != null) {
            bottom.setImage(gwc.imageManager.wallMap.get("WOOD").get(3));
          }
        }
      }

    } else {

      if (bottom != null) {

        if (left == null && right == null) {
          wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(4));
        } else if (right == null) {
          wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(5));
        } else {
          wall.setImage(gwc.imageManager.wallMap.get("WOOD").get(4));
        }

        if (walls[x][y+2] == null) {
          if (walls[x+1][y+1] == null && walls[x-1][y+1] == null) {
            bottom.setImage(gwc.imageManager.wallMap.get("WOOD").get(0));
          } else if (walls[x+1][y+1] == null) {
            bottom.setImage(gwc.imageManager.wallMap.get("WOOD").get(2));
          } else {
            bottom.setImage(gwc.imageManager.wallMap.get("WOOD").get(0));
          }
        } else {
          if (walls[x+1][y+1] == null && walls[x-1][y+1] == null) {
            bottom.setImage(gwc.imageManager.wallMap.get("WOOD").get(3));
          }
        }
      }

    }

    walls[x][y] = wall;
    gwc.tileManager.getTiles()[x][y].setOccupied(true);

  }


  public void removeWall(int x, int y) {
    gwc.getBackground().getChildren().remove(walls[x][y].rec);
    walls[x][y] = null;
  }


  public void draw() {

    for (int y = 0; y < gwc.worldHeight / gwc.tileSize; y++) {
      for (int x = 0; x < gwc.worldWidth / gwc.tileSize; x++) {

        Building wall = walls[x][y];

        if (wall != null) {
          wall.draw();
        }

      }
    }

  }



  public Building[][] getWalls() {
    return walls;
  }

}
