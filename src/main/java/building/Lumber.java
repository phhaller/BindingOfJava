package building;

import javafx.scene.paint.ImagePattern;
import view.GameWindowController;

public class Lumber extends Building {

  public Lumber(int x, int y, GameWindowController gwc) {

    super(gwc);

    worldX = x * gwc.tileSize;
    worldY = y * gwc.tileSize;

    name = "LUMBER";
    life = 15;
    maxLife = life;
    collision = true;

    image = gwc.imageManager.machineMap.get("LUMBER").get(0);
    inventoryImage = gwc.imageManager.inventoryMap.get("MACHINE").get(1);

    createMachine();
    createHitBox();

  }


  private void createMachine() {

    rec.setWidth(48);
    rec.setHeight(48);
    rec.setTranslateX(worldX);
    rec.setTranslateY(worldY);
    rec.setFill(new ImagePattern(image));

  }


  private void createHitBox() {

    hitBox.setTranslateX(8);
    hitBox.setTranslateY(0);
    hitBox.setWidth(32);
    hitBox.setHeight(32);

  }

}
