package entity;

import javafx.animation.Animation;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import view.GameWindowController;

import java.util.Random;

public class Merchant extends Entity {

  private EntityAnimation wandererAnimation;

  public Merchant(int x, int y, GameWindowController gwc) {
    super(gwc);

    worldX = x * gwc.tileSize;
    worldY = y * gwc.tileSize;
    name = "MERCHANT";
    type = 1;
    speed = 1;
    direction = "W";

    createWanderer();
    createHitBox();

  }


  private void createWanderer() {

    Image spriteImg = new Image("/sprites/npc/wanderer.png");
    ImageView imageView = new ImageView(spriteImg);

    entityAnimation = new EntityAnimation(imageView, Animation.INDEFINITE, 64, 64, 6, 800);
    entityAnimation.setTranslateX(worldX - 8);
    entityAnimation.setTranslateY(worldY - 16);

  }


  private void createHitBox() {
    hitBox.setWidth(32);
    hitBox.setHeight(40);
    hitBox.setX(8);
    hitBox.setY(12);
    hitBoxDefaultX = (int) hitBox.getX();
    hitBoxDefaultY = (int) hitBox.getY();
  }


  public void setAction() {

    actionLockCounter++;

    if (actionLockCounter >= 200) {

      Random r = new Random();
      int i = r.nextInt(125) + 1;

      if (i <= 25) {
        direction = "W";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(0);
      } else if (i <= 50) {
        direction = "A";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(64);
      } else if (i <= 75) {
        direction = "S";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(128);
      } else if (i <= 100) {
        direction = "D";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(192);
      } else {
        direction = "";
        entityAnimation.animation.stop();
      }

      actionLockCounter = 0;

    }


  }


}
