package entity;

import building.Building;
import building.Wall;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.util.Duration;
import map.Vegetation;
import objects.*;
import projectiles.Arrow;
import util.AnimationManager;
import util.KeyHandler;
import view.GameWindowController;

import java.util.Random;

public class Player extends Entity {

  private Scene scene;
  private KeyHandler keyHandler;
  public AnimationManager animationManager;

  public int screenX;
  public int screenY;

  private int hasKey = 0;

  public boolean usingTool = false;
  public boolean cutting = false;
  public boolean mining = false;
  public boolean digging = false;

  public boolean usingSword = false;
  public boolean usingBow = false;
  public boolean shooting = false;

  public boolean building = false;


  public Player(GameWindowController gwc, Scene scene) {
    super(gwc);

    this.scene = scene;
    keyHandler = gwc.keyHandler;
    animationManager = new AnimationManager(this);
    worldX = gwc.worldWidth / 2 - 8;
    worldY = gwc.worldHeight / 2;
    screenX = gwc.screenWidth / 2 - 64 / 2;
    screenY = gwc.screenHeight / 2 - 64 / 2;
    name = "PLAYER";
    type = 0;
    speed = 3;
    direction = "W";
    maxLife = 100;
    life = maxLife;
    damage = 0;
    strength = 0;
    dexterity = 0;
    defense = 0;

    createPlayer();
    createHitBox();
    createAttackArea();

  }


  private void createPlayer() {

    animationManager.update("WALK", "BASIC");
    entityAnimation.setTranslateX(screenX);
    entityAnimation.setTranslateY(screenY - 16);
  }


  private void createHitBox() {
    hitBox.setWidth(32);
    hitBox.setHeight(32);
    hitBox.setX(16);
    hitBox.setY(16);
    hitBoxDefaultX = (int) hitBox.getX();
    hitBoxDefaultY = (int) hitBox.getY();
  }


  private void createAttackArea() {
    attackBox.setWidth(32);
    attackBox.setHeight(32);
    attackBox.setX(16);
    attackBox.setY(16);
  }


  public void update() {

    if (usingTool) {

      entityAnimation.setTranslateX(screenX);
      entityAnimation.setTranslateY(screenY - 16);

      int currentWorldX = worldX;
      int currentWorldY = worldY;
      int hitBoxWidth = (int) hitBox.getWidth();
      int hitBoxHeight = (int) hitBox.getHeight();

      switch (direction) {
        case "W":
          worldY -= attackBox.getHeight();
          break;
        case "A":
          worldX -= attackBox.getWidth();
          break;
        case "S":
          worldY += attackBox.getHeight();
          break;
        case "D":
          worldX += attackBox.getWidth();
          break;
      }

      hitBox.setWidth(attackBox.getWidth());
      hitBox.setHeight(attackBox.getHeight());

      Entity enemy = gwc.collisionManager.checkEntity(this, gwc.enemyManager.enemies);
      damageEnemy(enemy);

      Vegetation veg = gwc.collisionManager.checkVegetation(this);
      cutVegetation(veg);

      Building building = gwc.collisionManager.checkBuilding(this);
      destroyBuilding(building);

      worldX = currentWorldX;
      worldY = currentWorldY;
      hitBox.setWidth(hitBoxWidth);
      hitBox.setHeight(hitBoxHeight);

    } else if (usingSword) {

      entityAnimation.setTranslateX(screenX - 64);
      entityAnimation.setTranslateY(screenY - 64 - 16);

      int currentWorldX = worldX;
      int currentWorldY = worldY;
      int hitBoxWidth = (int) hitBox.getWidth();
      int hitBoxHeight = (int) hitBox.getHeight();

      switch (direction) {
        case "W":
          worldY -= attackBox.getHeight();
          break;
        case "A":
          worldX -= attackBox.getWidth();
          break;
        case "S":
          worldY += attackBox.getHeight();
          break;
        case "D":
          worldX += attackBox.getWidth();
          break;
      }

      hitBox.setWidth(attackBox.getWidth());
      hitBox.setHeight(attackBox.getHeight());

      Entity enemy = gwc.collisionManager.checkEntity(this, gwc.enemyManager.enemies);
      damageEnemy(enemy);

      worldX = currentWorldX;
      worldY = currentWorldY;
      hitBox.setWidth(hitBoxWidth);
      hitBox.setHeight(hitBoxHeight);

    } else if (usingBow) {

      entityAnimation.setTranslateX(screenX);
      entityAnimation.setTranslateY(screenY - 16);

    } else {

      if (keyHandler.W) {
        direction = "W";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(animationManager.walkOffsetY);
      } else if (keyHandler.A) {
        direction = "A";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(animationManager.walkOffsetY + entityAnimation.getSpriteHeight());
      } else if (keyHandler.S) {
        direction = "S";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(animationManager.walkOffsetY + entityAnimation.getSpriteHeight() * 2);
      } else if (keyHandler.D) {
        direction = "D";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(animationManager.walkOffsetY + entityAnimation.getSpriteHeight() * 3);
      } else {
        entityAnimation.animation.stop();
      }

      collided = false;
      gwc.collisionManager.checkWorldBorder(this);

      Vegetation veg = gwc.collisionManager.checkVegetation(this);

      Building building = gwc.collisionManager.checkBuilding(this);
      interactMachine(building);

      SuperObject obj = gwc.collisionManager.checkObject(this, true);
      pickUpObject(obj);

      Entity npc = gwc.collisionManager.checkEntity(this, gwc.npcManager.npcs);
      interactNPC(npc);

      Entity enemy = gwc.collisionManager.checkEntity(this, gwc.enemyManager.enemies);
      intersectEnemy(enemy);

      if (!collided) {

        if (keyHandler.W || keyHandler.A || keyHandler.S || keyHandler.D) {

          switch (direction) {
            case "W":
              worldY -= speed;
              break;
            case "A":
              worldX -= speed;
              break;
            case "S":
              worldY += speed;
              break;
            case "D":
              worldX += speed;
              break;
          }

        } else {
          entityAnimation.animation.stop();
        }

      }

    }

    // Set invincible to avoid constant enemy player hitting
    if (invincible) {
      invincibleCounter++;
      if (invincibleCounter > 50) {
        invincible = false;
        invincibleCounter = 0;
      }
    }

  }


  // HANDLE TOOL / SWORD / BOW / ARMOUR ACTION WHEN KEY OR MOUSE EVENT OCCURRED
  public void createAction() {

    if (gwc.gameState == gwc.playState) {
      SuperObject current = gwc.equipmentManager.current;
      if (current != null) {

        if (gwc.uiManager.energyBar.energy >= current.energyUse) {
          if (!gwc.player.usingTool && !gwc.player.usingSword && !gwc.player.usingBow) {

            gwc.getBackground().getChildren().remove(gwc.player.entityAnimation);

            // Update the animation and energy bar
            gwc.player.animationManager.update(current.type, current.name);
            gwc.uiManager.energyBar.use(current.energyUse);

            // Update durability
            current.durability -= 1;

          }
        }

      }

    }

  }


  private void cutVegetation(Vegetation vegetation) {

    if (vegetation != null) {

      String veq = vegetation.type;
      String curEquip = gwc.equipmentManager.current.name;

      // TREES
      if (veq.equals("TREE") && curEquip.equals("AXE") && !cutting) {
        cutting = true;
        vegetation.life -= damage;
        gwc.inventoryManager.addItem(new Log(vegetation.worldX, vegetation.worldY, true, gwc));
        gwc.scrollingMessageManager.addMessage("You picked up a LOG");
        if (vegetation.life <= 0) {
          gwc.vegetationManager.removeVegetation(vegetation.worldX / gwc.tileSize, vegetation.worldY / gwc.tileSize);
          gwc.tileManager.getTiles()[vegetation.worldX/gwc.tileSize][vegetation.worldY/gwc.tileSize].setOccupied(false);
          gwc.scrollingMessageManager.addMessage("You've cut down a TREE");
        }
      }

      // BUSHES
      if (veq.equals("BUSH") && curEquip.equals("AXE") && !cutting) {
        cutting = true;
        vegetation.life -= damage;
        gwc.inventoryManager.addItem(new Log(vegetation.worldX, vegetation.worldY, true, gwc));
        gwc.scrollingMessageManager.addMessage("You picked up a LOG");
        if (vegetation.life <= 0) {
          if (vegetation.hasBerry) {
            Random r = new Random();
            int random = r.nextInt(3 - 1) + 1;
            switch (vegetation.name) {
              case "BUSHRED":
                for (int i = 0; i < random; i++) {
                  gwc.inventoryManager.addItem(new Berry(vegetation.worldX, vegetation.worldY, true, "RED", gwc));
                  gwc.scrollingMessageManager.addMessage("You picked up a BERRYRED");
                }
                break;
              case "BUSHPURPLE":
                for (int i = 0; i < random; i++) {
                  gwc.inventoryManager.addItem(new Berry(vegetation.worldX, vegetation.worldY, true, "PURPLE", gwc));
                  gwc.scrollingMessageManager.addMessage("You picked up a BERRYPURPLE");
                }
                break;
              case "BUSHBLUE":
                for (int i = 0; i < random; i++) {
                  gwc.inventoryManager.addItem(new Berry(vegetation.worldX, vegetation.worldY, true, "BLUE", gwc));
                  gwc.scrollingMessageManager.addMessage("You picked up a BERRYBLUE");
                }
                break;
              case "BUSHYELLOW":
                for (int i = 0; i < random; i++) {
                  gwc.inventoryManager.addItem(new Berry(vegetation.worldX, vegetation.worldY, true, "YELLOW", gwc));
                  gwc.scrollingMessageManager.addMessage("You picked up a BERRYYELLOW");
                }
                break;
            }
            // gwc.inventoryManager.addItem(new Apple(vegetation.worldX, vegetation.worldY, true, gwc));
          }
          gwc.vegetationManager.removeVegetation(vegetation.worldX / gwc.tileSize, vegetation.worldY / gwc.tileSize);
          gwc.tileManager.getTiles()[vegetation.worldX/gwc.tileSize][vegetation.worldY/gwc.tileSize].setOccupied(false);
          gwc.scrollingMessageManager.addMessage("You've cut down a BUSH");
        }
      }

      // ROCKS
      if (veq.equals("ROCK") && curEquip.equals("PICKAXE") && !mining) {
        mining = true;
        vegetation.life -= damage;
        gwc.inventoryManager.addItem(new Rock(vegetation.worldX, vegetation.worldY, true, gwc));
        gwc.scrollingMessageManager.addMessage("You picked up a ROCK");
        if (vegetation.life <= 0) {
          if (vegetation.hasIngot) {
            Random r = new Random();
            int random = r.nextInt(4 - 1) + 1;
            switch (vegetation.name) {
              case "ROCKIRON":
                for (int i = 0; i < random; i++) {
                  gwc.inventoryManager.addItem(new Ingot(vegetation.worldX, vegetation.worldY, true, "IRON", gwc));
                  gwc.scrollingMessageManager.addMessage("You picked up an INGOTIRON");
                }
                break;
              case "ROCKGOLD":
                for (int i = 0; i < random; i++) {
                  gwc.inventoryManager.addItem(new Ingot(vegetation.worldX, vegetation.worldY, true, "GOLD", gwc));
                  gwc.scrollingMessageManager.addMessage("You picked up an INGOTGOLD");
                }
                break;
            }
          }
          gwc.vegetationManager.removeVegetation(vegetation.worldX / gwc.tileSize, vegetation.worldY / gwc.tileSize);
          gwc.tileManager.getTiles()[vegetation.worldX/gwc.tileSize][vegetation.worldY/gwc.tileSize].setOccupied(false);
          gwc.scrollingMessageManager.addMessage("You've mined a ROCK");
        }
      }

      // SHROOMS
      if (veq.equals("SHROOM") && curEquip.equals("SHOVEL") && !digging) {
        digging = true;
        vegetation.life -= damage;
        if (vegetation.life <= 0) {
          switch (vegetation.name) {
            case "SHROOMRED":
              gwc.inventoryManager.addItem(new Shroom(vegetation.worldX, vegetation.worldY, true, "RED", gwc));
              break;
            case "SHROOMBROWN":
              gwc.inventoryManager.addItem(new Shroom(vegetation.worldX, vegetation.worldY, true, "BROWN", gwc));
              break;
          }
          gwc.vegetationManager.removeVegetation(vegetation.worldX / gwc.tileSize, vegetation.worldY / gwc.tileSize);
          gwc.tileManager.getTiles()[vegetation.worldX/gwc.tileSize][vegetation.worldY/gwc.tileSize].setOccupied(false);
          gwc.scrollingMessageManager.addMessage("You've dug up a SHROOM");
        }
      }

    }

  }


  private void destroyBuilding(Building building) {

    if (building != null) {

      String buildingName = building.name;
      String buildingType = building.type;
      String curEquip = gwc.equipmentManager.current.name;

      // WOOD WALL
      if (buildingName.equals("WALL") && buildingType.equals("WOOD") && curEquip.equals("AXE") && !cutting) {
        cutting = true;
        building.life -= damage;
        gwc.inventoryManager.addItem(new Log(building.worldX, building.worldY, true, gwc));
        if (building.life <= 0) {
          gwc.buildingManager.wallManager.removeWall(building.worldX / gwc.tileSize, building.worldY / gwc.tileSize);
          gwc.tileManager.getTiles()[building.worldX/gwc.tileSize][building.worldY/gwc.tileSize].setOccupied(false);
          gwc.scrollingMessageManager.addMessage("You've destroyed a " + building.type + " WALL!");
        }
      }

      // ANVIL
      if (buildingName.equals("ANVIL") && curEquip.equals("PICKAXE") && !mining) {
        mining = true;
        building.life -= damage;
        gwc.inventoryManager.addItem(new Ingot(building.worldX, building.worldY, true, "IRON", gwc));
        if (building.life <= 0) {
          gwc.buildingManager.machineManager.removeMachine(building.worldX / gwc.tileSize, building.worldY / gwc.tileSize);
          gwc.tileManager.getTiles()[building.worldX/gwc.tileSize][building.worldY/gwc.tileSize].setOccupied(false);
          gwc.scrollingMessageManager.addMessage("You've destroyed an ANVIL!");
        }
      }

      // LUMBER
      if (buildingName.equals("LUMBER") && curEquip.equals("AXE") && !cutting) {
        cutting = true;
        building.life -= damage;
        gwc.inventoryManager.addItem(new Log(building.worldX, building.worldY, true, gwc));
        if (building.life <= 0) {
          gwc.buildingManager.machineManager.removeMachine(building.worldX / gwc.tileSize, building.worldY / gwc.tileSize);
          gwc.tileManager.getTiles()[building.worldX/gwc.tileSize][building.worldY/gwc.tileSize].setOccupied(false);
          gwc.scrollingMessageManager.addMessage("You've destroyed a LUMBER STAND!");
        }
      }

      // BREWER
      if (buildingName.equals("BREWER") && curEquip.equals("AXE") && !cutting) {
        cutting = true;
        building.life -= damage;
        gwc.inventoryManager.addItem(new Log(building.worldX, building.worldY, true, gwc));
        if (building.life <= 0) {
          gwc.buildingManager.machineManager.removeMachine(building.worldX / gwc.tileSize, building.worldY / gwc.tileSize);
          gwc.tileManager.getTiles()[building.worldX/gwc.tileSize][building.worldY/gwc.tileSize].setOccupied(false);
          gwc.scrollingMessageManager.addMessage("You've destroyed a BREWER!");
        }
      }

    }

  }


  private void pickUpObject(SuperObject object) {

    if (object != null) {

      switch (object.name) {

        case "AXE":
          if (keyHandler.E) {
            if (gwc.equipmentManager.addTool(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up an AXE");
            }
          }
          break;
        case "PICKAXE":
          if (keyHandler.E) {
            if (gwc.equipmentManager.addTool(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a PICKAXE");
            }
          }
          break;
        case "SHOVEL":
          if (keyHandler.E) {
            if (gwc.equipmentManager.addTool(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a SHOVEL");
            }
          }
          break;
        case "SABER":
          if (keyHandler.E) {
            if (gwc.equipmentManager.addSword(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a SABER");
            }
          }
          break;
        case "LONGSWORD":
          if (keyHandler.E) {
            if (gwc.equipmentManager.addSword(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a LONGSWORD");
            }
          }
          break;
        case "NORMALBOW":
          if (keyHandler.E) {
            if (gwc.equipmentManager.addBow(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a NORMALBOW");
            }
          }
          break;
        case "KEY":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              hasKey += 1;
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a KEY");
            }
          }
          break;
        case "COIN":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a COIN");
              // animateItemPickup(object);
            }
          }
          break;
        case "CHEST":
          if (keyHandler.E) {
            if (hasKey > 0) {
              hasKey -= 1;
              gwc.inventoryManager.removeItem("KEY", true);
              object.open();
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You opened a CHEST");
            }
          }
          break;
        case "BARREL":
          if (keyHandler.E) {
            if (hasKey > 0) {
              hasKey -= 1;
              gwc.inventoryManager.removeItem("KEY", true);
              object.open();
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You opened a BARREL");
            }
          }
          break;
        case "POTIONRED":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a POTIONRED");
            }
          }
          break;
        case "POTIONPURPLE":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a POTIONPURPLE");
            }
            }
          break;
        case "POTIONBLUE":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a POTIONBLUE");
            }
          }
          break;
        case "POTIONYELLOW":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a POTIONYELLOW");
            }
          }
          break;
        case "CHICKENLEG_RAW":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a CHICKENLEG");
            }
          }
          break;
        case "PORKCHOP_RAW":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a PORKCHOP");
            }
          }
          break;
        case "RIBS_RAW":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up some RIBS");
            }
          }
          break;
        case "STEAK_RAW":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a STEAK");
            }
          }
          break;
        case "FEATHER":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a FEATHER");
            }
          }
          break;
        case "BONE":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up a BONE");
            }
          }
          break;
        case "ACORN":
          if (keyHandler.E) {
            if (gwc.inventoryManager.addItem(object)) {
              gwc.objects.remove(object);
              gwc.getBackground().getChildren().remove(object.rec);
              gwc.scrollingMessageManager.addMessage("You picked up an ACORN");
            }
          }
          break;

      }

      int objectX = object.worldX / gwc.tileSize;
      int objectY = object.worldY / gwc.tileSize;
      gwc.tileManager.getTiles()[objectX][objectY].setOccupied(false);

    }

  }


  private void animateItemPickup(SuperObject object) {

    gwc.getBackground().getChildren().add(object.rec);
    int objectX = object.worldX - gwc.player.worldX + gwc.player.screenX;
    int objectY = object.worldY - gwc.player.worldY + gwc.player.screenY;

    TranslateTransition tt = new TranslateTransition();

    tt.setFromX(objectX);
    tt.setFromY(objectY - 70);

    tt.setToX(screenX + 16);
    tt.setToY(screenY);

    tt.setDuration(Duration.millis(300));

    tt.setCycleCount(1);
    tt.setAutoReverse(false);
    tt.setInterpolator(Interpolator.EASE_IN);

    tt.setNode(object.rec);

    tt.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        gwc.getBackground().getChildren().remove(object.rec);
        System.out.println("FINISHED");
      }
    });

    tt.play();

  }


  private void interactMachine(Building building) {

    if (building != null) {

      switch (building.name) {

        case "ANVIL":
          if (keyHandler.E) {
            gwc.machineState = gwc.anvilState;
            gwc.gameState = gwc.craftingState;
            gwc.uiManager.craftingWindow.update();
          }
          break;
        case "LUMBER":
          if (keyHandler.E) {
            gwc.machineState = gwc.lumberState;
            gwc.gameState = gwc.craftingState;
            gwc.uiManager.craftingWindow.update();
          }
          break;
        case "BREWER":
          if (keyHandler.E) {
            gwc.machineState = gwc.brewerState;
            gwc.gameState = gwc.craftingState;
            gwc.uiManager.craftingWindow.update();
          }
          break;
        case "CAMPFIRE":
          if (keyHandler.E) {
            gwc.machineState = gwc.campfireState;
            gwc.gameState = gwc.craftingState;
            gwc.uiManager.craftingWindow.update();
          }
          break;

      }

    }

  }


  private void interactNPC(Entity entity) {


    if (entity != null) {

      switch (entity.name) {

        case "MERCHANT":
          if (keyHandler.E) {
            gwc.npcState = gwc.merchantState;
            gwc.gameState = gwc.dialogState;
            entityAnimation.animation.stop();
            entity.entityAnimation.animation.stop();
            entity.direction = "";
            gwc.uiManager.dialogWindow.update();
          }
          break;
        case "BLACKSMITH":
          if (keyHandler.E) {
            gwc.npcState = gwc.blacksmithState;
            gwc.gameState = gwc.dialogState;
            entityAnimation.animation.stop();
            entity.entityAnimation.animation.stop();
            entity.direction = "";
            gwc.uiManager.dialogWindow.update();
          }
          break;

      }

    }

  }


  private void intersectEnemy(Entity entity) {

    if (entity != null) {

      if (!invincible) {
        System.out.println(life);
        if (life > entity.damage - defense) {
          life -= (entity.damage - defense);
          gwc.uiManager.healthBar.hit(life);
        } else {
          gwc.gameOver();
        }
        invincible = true;
      }

    }

  }


  public void damageEnemy(Entity entity) {

    if (entity != null) {

      if (!entity.invincible && entity.life > 0) {

        FadeTransition ft = new FadeTransition();
        ft.setDuration(Duration.millis(2000));
        ft.setFromValue(1);
        ft.setToValue(0);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.setNode(entity.healthBar);
        ft.setInterpolator(Interpolator.EASE_IN);
        ft.play();

        entity.life -= damage;
        entity.invincible = true;
        entity.healthBar.hit(entity.life);
        entity.damageReaction();
        gwc.scrollingMessageManager.addMessage("You dealt " + damage + " damage to " + entity.name);

        if (entity.life <= 0) {
          entity.alive = false;
          gwc.enemyManager.kill(entity);
          gwc.scrollingMessageManager.addMessage("You killed a " + entity.name);
        }

      }

    }

  }


  public EntityAnimation getPlayer() {
    return entityAnimation;
  }


}
