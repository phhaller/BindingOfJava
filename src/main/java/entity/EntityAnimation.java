package entity;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import util.SpriteAnimation;

public class EntityAnimation extends Pane {

  private ImageView imageView;
  private int count;
  private int columns;
  private int offsetX = 0;
  private int offsetY = 0;
  private int width;
  private int height;

   public SpriteAnimation animation;


  public EntityAnimation(ImageView imageView, int cycleCount, int width, int height, int columns, int millis) {
    this.imageView = imageView;
    this.width = width;
    this.height = height;
    this.count = columns;
    this.columns = columns;
    this.imageView.setViewport(new Rectangle2D(offsetX, offsetY, width, height));
    animation = new SpriteAnimation(imageView, Duration.millis(millis), cycleCount, count, columns, offsetX, offsetY, width, height);
    getChildren().add(imageView);
  }


  public int getSpriteWidth() {
    return width;
  }

  public int getSpriteHeight() {
    return height;
  }

}