package entity;

import map.Tile;
import view.GameWindowController;

import java.util.ArrayList;
import java.util.Random;

public class NPCManager {

  private GameWindowController gwc;
  public ArrayList<Entity> npcs;


  public NPCManager(GameWindowController gwc) {

    this.gwc = gwc;
    createNPCs();

  }


  private void createNPCs() {

    npcs = new ArrayList<>();
    Tile[][] tiles = gwc.tileManager.getTiles();

    int wandererCounter = 0;
    int blacksmithCounter = 0;

    Random r = new Random();

    while (wandererCounter < 3) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Merchant(tmp1, tmp2, gwc);
        npcs.add(entity);
        wandererCounter++;
      }

    }


    while (blacksmithCounter < 3) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Blacksmith(tmp1, tmp2, gwc);
        npcs.add(entity);
        blacksmithCounter++;
      }

    }

  }


  public void update() {
    for (Entity entity : npcs) {
      entity.update();
    }
  }


  public void draw() {
    for (Entity entity : npcs) {
      entity.draw();
    }
  }


}
