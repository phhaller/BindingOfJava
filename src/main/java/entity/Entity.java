package entity;

import javafx.scene.shape.Rectangle;
import objects.SuperObject;
import ui.HealthBar;
import view.GameWindowController;

import java.util.ArrayList;

public class Entity {

  public GameWindowController gwc;
  public int worldX, worldY;
  public HealthBar healthBar;
  public EntityAnimation entityAnimation;

  // CHARACTER ATTRIBUTES
  public String direction = "W";
  public int type; // 0 = player, 1 = npc, 2 = enemy
  public String name;
  public int speed;
  public int maxLife;
  public int life;
  public int damage;
  public int strength;
  public int dexterity;
  public int defense;
  public int experience;

  // HITBOX
  public Rectangle hitBox;
  public Rectangle attackBox;
  public int hitBoxDefaultX, hitBoxDefaultY;

  // COUNTER
  public int actionLockCounter = 0;
  public int invincibleCounter = 0;

  // BOOLEAN
  public boolean collided = false;
  public boolean invincible = false;
  public boolean alive = true;

  public ArrayList<String> entityDrops;



  public Entity(GameWindowController gwc) {
    this.gwc = gwc;
    hitBox = new Rectangle(0, 0, gwc.tileSize, gwc.tileSize);
    attackBox = new Rectangle(0, 0, 0, 0);
  }

  public void setAction() {}

  public void damageReaction() {}

  public void update() {

    setAction();
    collided = false;

    gwc.collisionManager.checkWorldBorder(this);
    gwc.collisionManager.checkVegetation(this);
    gwc.collisionManager.checkBuilding(this);
    gwc.collisionManager.checkObject(this, false);
    gwc.collisionManager.checkEntity(this, gwc.npcManager.npcs);
    gwc.collisionManager.checkEntity(this, gwc.enemyManager.enemies);
    boolean contactPlayer = gwc.collisionManager.checkPlayer(this);

    // If entity is enemy and there was contact between player and enemy, deal damage
    if (type == 2 && contactPlayer) {
      if (!gwc.player.invincible) {
        if (gwc.player.life > damage - gwc.player.defense) {
          gwc.player.life -= (damage - gwc.player.defense);
          gwc.uiManager.healthBar.hit(gwc.player.life);
        } else {
          gwc.player.life = 0;
          gwc.gameOver();
        }
        gwc.player.invincible = true;
      }
    }

    if (!collided && alive) {

      switch (direction) {
        case "W":
          worldY -= speed;
          break;
        case "A":
          worldX -= speed;
          break;
        case "S":
          worldY += speed;
          break;
        case "D":
          worldX += speed;
          break;
      }

    }

    // Set invincible to avoid constant enemy player hitting
    if (invincible) {
      invincibleCounter++;
      if (invincibleCounter > 40) {
        invincible = false;
        invincibleCounter = 0;
      }
    }

  }


  public void draw() {

    gwc.getBackground().getChildren().removeAll(entityAnimation, healthBar);

    int screenX = worldX - gwc.player.worldX + gwc.player.screenX;
    int screenY = worldY - gwc.player.worldY + gwc.player.screenY;
    int tileSize = gwc.tileSize;

    if (worldX + tileSize > gwc.player.worldX - gwc.player.screenX &&
        worldX - tileSize < gwc.player.worldX + gwc.player.screenX &&
        worldY + tileSize > gwc.player.worldY - gwc.player.screenY &&
        worldY - tileSize * 2 < gwc.player.worldY + gwc.player.screenY) {

      entityAnimation.setTranslateX(screenX - 8);
      entityAnimation.setTranslateY(screenY - 12);

      gwc.getBackground().getChildren().add(entityAnimation);

      if (healthBar != null) {
        healthBar.setTranslateX(screenX - 8);
        healthBar.setTranslateY(screenY - 20);
        gwc.getBackground().getChildren().add(healthBar);
      }

    }

  }

}
