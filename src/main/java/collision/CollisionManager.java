package collision;

import building.Building;
import building.Wall;
import map.Vegetation;
import objects.SuperObject;
import entity.Entity;
import projectiles.Projectile;
import view.GameWindowController;

import java.util.ArrayList;

public class CollisionManager {

  GameWindowController gwc;


  public CollisionManager(GameWindowController gwc) {
    this.gwc = gwc;
  }


  public void checkWorldBorder(Entity entity) {

    int entityLeftWorldX = entity.worldX + (int) entity.hitBox.getX();
    int entityRightWorldX = entity.worldX + (int) entity.hitBox.getX() + (int) entity.hitBox.getWidth();
    int entityTopWorldY = entity.worldY + (int) entity.hitBox.getY();
    int entityBottomWorldY = entity.worldY + (int) entity.hitBox.getY() + (int) entity.hitBox.getHeight();

    int entityLeftCol = entityLeftWorldX / gwc.tileSize;
    int entityRightCol = entityRightWorldX / gwc.tileSize;
    int entityTopRow = entityTopWorldY / gwc.tileSize;
    int entityBottomRow = entityBottomWorldY / gwc.tileSize;

    switch (entity.direction) {
      case "W":
        entityTopRow = (entityTopWorldY - entity.speed) / gwc.tileSize;
        if (entityTopRow <= 0) {
          entity.collided = true;
        }
        break;
      case "A":
        entityLeftCol = (entityLeftWorldX - entity.speed) / gwc.tileSize;
        if (entityLeftCol <= 0) {
          entity.collided = true;
        }
        break;
      case "S":
        entityBottomRow = (entityBottomWorldY + entity.speed) / gwc.tileSize;
        if (entityBottomRow >= gwc.worldHeight / gwc.tileSize - 1) {
          entity.collided = true;
        }
        break;
      case "D":
        entityRightCol = (entityRightWorldX + entity.speed) / gwc.tileSize;
        if (entityRightCol >= gwc.worldWidth / gwc.tileSize - 1) {
          entity.collided = true;
        }
        break;
    }

  }


  public void checkWorldBorder(Projectile projectile) {

    int entityLeftWorldX = projectile.worldX + (int) projectile.hitBox.getX();
    int entityRightWorldX = projectile.worldX + (int) projectile.hitBox.getX() + (int) projectile.hitBox.getWidth();
    int entityTopWorldY = projectile.worldY + (int) projectile.hitBox.getY();
    int entityBottomWorldY = projectile.worldY + (int) projectile.hitBox.getY() + (int) projectile.hitBox.getHeight();

    int entityLeftCol = entityLeftWorldX / gwc.tileSize;
    int entityRightCol = entityRightWorldX / gwc.tileSize;
    int entityTopRow = entityTopWorldY / gwc.tileSize;
    int entityBottomRow = entityBottomWorldY / gwc.tileSize;

    switch (projectile.direction) {
      case "W":
        entityTopRow = (entityTopWorldY - projectile.speed) / gwc.tileSize;
        if (entityTopRow <= 0) {
          projectile.collided = true;
        }
        break;
      case "A":
        entityLeftCol = (entityLeftWorldX - projectile.speed) / gwc.tileSize;
        if (entityLeftCol <= 0) {
          projectile.collided = true;
        }
        break;
      case "S":
        entityBottomRow = (entityBottomWorldY + projectile.speed) / gwc.tileSize;
        if (entityBottomRow >= gwc.worldHeight / gwc.tileSize - 1) {
          projectile.collided = true;
        }
        break;
      case "D":
        entityRightCol = (entityRightWorldX + projectile.speed) / gwc.tileSize;
        if (entityRightCol >= gwc.worldWidth / gwc.tileSize - 1) {
          projectile.collided = true;
        }
        break;
    }

  }


  // CHECK ENTITY - VEGETATION INTERSECTION
  public Vegetation checkVegetation(Entity entity) {

    Vegetation veg = null;

    int entityLeftWorldX = entity.worldX + (int) entity.hitBox.getX();
    int entityRightWorldX = entity.worldX + (int) entity.hitBox.getX() + (int) entity.hitBox.getWidth();
    int entityTopWorldY = entity.worldY + (int) entity.hitBox.getY();
    int entityBottomWorldY = entity.worldY + (int) entity.hitBox.getY() + (int) entity.hitBox.getHeight();

    int entityLeftCol = entityLeftWorldX / gwc.tileSize;
    int entityRightCol = entityRightWorldX / gwc.tileSize;
    int entityTopRow = entityTopWorldY / gwc.tileSize;
    int entityBottomRow = entityBottomWorldY / gwc.tileSize;

    Vegetation veg1 = null;
    Vegetation veg2 = null;

    switch (entity.direction) {
      case "W":
        entityTopRow = (entityTopWorldY - entity.speed) / gwc.tileSize;
        veg1 = gwc.vegetationManager.getVegetation()[entityLeftCol][entityTopRow];
        veg2 = gwc.vegetationManager.getVegetation()[entityRightCol][entityTopRow];
        if ((veg1 != null && (veg1.type.equals("TREE") || veg1.type.equals("BUSH") || veg1.type.equals("ROCK"))) ||
            (veg2 != null && (veg2.type.equals("TREE") || veg2.type.equals("BUSH") || veg2.type.equals("ROCK")))) {
          entity.collided = true;
        }
        break;
      case "A":
        entityLeftCol = (entityLeftWorldX - entity.speed) / gwc.tileSize;
        veg1 = gwc.vegetationManager.getVegetation()[entityLeftCol][entityTopRow];
        veg2 = gwc.vegetationManager.getVegetation()[entityLeftCol][entityBottomRow];
        if ((veg1 != null && (veg1.type.equals("TREE") || veg1.type.equals("BUSH") || veg1.type.equals("ROCK"))) ||
            (veg2 != null && (veg2.type.equals("TREE") || veg2.type.equals("BUSH") || veg2.type.equals("ROCK")))) {
          entity.collided = true;
        }
        break;
      case "S":
        entityBottomRow = (entityBottomWorldY + entity.speed) / gwc.tileSize;
        veg1 = gwc.vegetationManager.getVegetation()[entityLeftCol][entityBottomRow];
        veg2 = gwc.vegetationManager.getVegetation()[entityRightCol][entityBottomRow];
        if ((veg1 != null && (veg1.type.equals("TREE") || veg1.type.equals("BUSH") || veg1.type.equals("ROCK"))) ||
            (veg2 != null && (veg2.type.equals("TREE") || veg2.type.equals("BUSH") || veg2.type.equals("ROCK")))) {
          entity.collided = true;
        }
        break;
      case "D":
        entityRightCol = (entityRightWorldX + entity.speed) / gwc.tileSize;
        veg1 = gwc.vegetationManager.getVegetation()[entityRightCol][entityTopRow];
        veg2 = gwc.vegetationManager.getVegetation()[entityRightCol][entityBottomRow];
        if ((veg1 != null && (veg1.type.equals("TREE") || veg1.type.equals("BUSH") || veg1.type.equals("ROCK"))) ||
            (veg2 != null && (veg2.type.equals("TREE") || veg2.type.equals("BUSH") || veg2.type.equals("ROCK")))) {
          entity.collided = true;
        }
        break;
    }

    if (veg1 != null) {
      veg = veg1;
    }

    if (veg2 != null) {
      veg = veg2;
    }

    return veg;

  }


  // CHECK PROJECTILE - VEGETATION INTERSECTION
  public Vegetation checkVegetation(Projectile projectile) {

    Vegetation veg = null;

    int projectileLeftWorldX = projectile.worldX + (int) projectile.hitBox.getX();
    int projectileRightWorldX = projectile.worldX + (int) projectile.hitBox.getX() + (int) projectile.hitBox.getWidth();
    int projectileTopWorldY = projectile.worldY + (int) projectile.hitBox.getY();
    int projectileBottomWorldY = projectile.worldY + (int) projectile.hitBox.getY() + (int) projectile.hitBox.getHeight();

    int projectileLeftCol = projectileLeftWorldX / gwc.tileSize;
    int projectileRightCol = projectileRightWorldX / gwc.tileSize;
    int projectileTopRow = projectileTopWorldY / gwc.tileSize;
    int projectileBottomRow = projectileBottomWorldY / gwc.tileSize;

    Vegetation veg1 = null;
    Vegetation veg2 = null;

    switch (projectile.direction) {
      case "W":
        projectileTopRow = (projectileTopWorldY - projectile.speed) / gwc.tileSize;
        veg1 = gwc.vegetationManager.getVegetation()[projectileLeftCol][projectileTopRow];
        veg2 = gwc.vegetationManager.getVegetation()[projectileRightCol][projectileTopRow];
        if ((veg1 != null && (veg1.type.equals("TREE") || veg1.type.equals("BUSH") || veg1.type.equals("ROCK"))) ||
            (veg2 != null && (veg2.type.equals("TREE") || veg2.type.equals("BUSH") || veg2.type.equals("ROCK")))) {
          projectile.collided = true;
        }
        break;
      case "A":
        projectileLeftCol = (projectileLeftWorldX - projectile.speed) / gwc.tileSize;
        veg1 = gwc.vegetationManager.getVegetation()[projectileLeftCol][projectileTopRow];
        veg2 = gwc.vegetationManager.getVegetation()[projectileLeftCol][projectileBottomRow];
        if ((veg1 != null && (veg1.type.equals("TREE") || veg1.type.equals("BUSH") || veg1.type.equals("ROCK"))) ||
            (veg2 != null && (veg2.type.equals("TREE") || veg2.type.equals("BUSH") || veg2.type.equals("ROCK")))) {
          projectile.collided = true;
        }
        break;
      case "S":
        projectileBottomRow = (projectileBottomWorldY + projectile.speed) / gwc.tileSize;
        veg1 = gwc.vegetationManager.getVegetation()[projectileLeftCol][projectileBottomRow];
        veg2 = gwc.vegetationManager.getVegetation()[projectileRightCol][projectileBottomRow];
        if ((veg1 != null && (veg1.type.equals("TREE") || veg1.type.equals("BUSH") || veg1.type.equals("ROCK"))) ||
            (veg2 != null && (veg2.type.equals("TREE") || veg2.type.equals("BUSH") || veg2.type.equals("ROCK")))) {
          projectile.collided = true;
        }
        break;
      case "D":
        projectileRightCol = (projectileRightWorldX + projectile.speed) / gwc.tileSize;
        veg1 = gwc.vegetationManager.getVegetation()[projectileRightCol][projectileTopRow];
        veg2 = gwc.vegetationManager.getVegetation()[projectileRightCol][projectileBottomRow];
        if ((veg1 != null && (veg1.type.equals("TREE") || veg1.type.equals("BUSH") || veg1.type.equals("ROCK"))) ||
            (veg2 != null && (veg2.type.equals("TREE") || veg2.type.equals("BUSH") || veg2.type.equals("ROCK")))) {
          projectile.collided = true;
        }
        break;
    }

    if (veg1 != null) {
      veg = veg1;
    }

    if (veg2 != null) {
      veg = veg2;
    }

    return veg;

  }



  public SuperObject checkObject(Entity entity, boolean player) {

    SuperObject object = null;

    for (int i = 0; i < gwc.objectsManager.objects.size(); i++) {

      if (gwc.objectsManager.objects.get(i) != null) {

        entity.hitBox.setX(entity.worldX + entity.hitBox.getX());
        entity.hitBox.setY(entity.worldY + entity.hitBox.getY());

        gwc.objects.get(i).hitBox.setX(gwc.objects.get(i).worldX + gwc.objects.get(i).hitBox.getX());
        gwc.objects.get(i).hitBox.setY(gwc.objects.get(i).worldY + gwc.objects.get(i).hitBox.getY());

        switch (entity.direction) {
          case "W":
            entity.hitBox.setY(entity.hitBox.getY() - entity.speed);
            break;
          case "A":
            entity.hitBox.setX(entity.hitBox.getX() - entity.speed);
            break;
          case "S":
            entity.hitBox.setY(entity.hitBox.getY() + entity.speed);
            break;
          case "D":
            entity.hitBox.setX(entity.hitBox.getX() + entity.speed);
            break;
        }

        if (entity.hitBox.getBoundsInParent().intersects(gwc.objects.get(i).hitBox.getBoundsInParent())) {
          if (gwc.objects.get(i).collision) {
            entity.collided = true;
          }
          if (player) {
            object = gwc.objects.get(i);
          }
        }

        entity.hitBox.setX(entity.hitBoxDefaultX);
        entity.hitBox.setY(entity.hitBoxDefaultY);
        gwc.objects.get(i).hitBox.setX(gwc.objects.get(i).hitBoxDefaultX);
        gwc.objects.get(i).hitBox.setY(gwc.objects.get(i).hitBoxDefaultY);

      }

    }

    return object;

  }


  // CHECK ENTITY - ENTITY INTERSECTION
  public Entity checkEntity(Entity entity, ArrayList<Entity> target) {

    Entity e = null;

    for (int i = 0; i < target.size(); i++) {

      if (target.get(i) != null) {

        entity.hitBox.setX(entity.worldX + entity.hitBox.getX());
        entity.hitBox.setY(entity.worldY + entity.hitBox.getY());

        target.get(i).hitBox.setX(target.get(i).worldX + target.get(i).hitBox.getX());
        target.get(i).hitBox.setY(target.get(i).worldY + target.get(i).hitBox.getY());

        switch (entity.direction) {
          case "W":
            entity.hitBox.setY(entity.hitBox.getY() - entity.speed);
            break;
          case "A":
            entity.hitBox.setX(entity.hitBox.getX() - entity.speed);
            break;
          case "S":
            entity.hitBox.setY(entity.hitBox.getY() + entity.speed);
            break;
          case "D":
            entity.hitBox.setX(entity.hitBox.getX() + entity.speed);
            break;
        }

        if (entity.hitBox.getBoundsInParent().intersects(target.get(i).hitBox.getBoundsInParent())) {
          if (target.get(i) != entity) {
            entity.collided = true;
            e = target.get(i);
          }
        }

        entity.hitBox.setX(entity.hitBoxDefaultX);
        entity.hitBox.setY(entity.hitBoxDefaultY);
        target.get(i).hitBox.setX(target.get(i).hitBoxDefaultX);
        target.get(i).hitBox.setY(target.get(i).hitBoxDefaultY);

      }

    }


    return e;

  }


  // CHECK PROJECTILE - ENTITY INTERSECTION
  public Entity checkEntity(Projectile projectile, ArrayList<Entity> target) {

    Entity e = null;

    for (int i = 0; i < target.size(); i++) {

      if (target.get(i) != null) {

        projectile.hitBox.setX(projectile.worldX + projectile.hitBox.getX());
        projectile.hitBox.setY(projectile.worldY + projectile.hitBox.getY());

        target.get(i).hitBox.setX(target.get(i).worldX + target.get(i).hitBox.getX());
        target.get(i).hitBox.setY(target.get(i).worldY + target.get(i).hitBox.getY());

        switch (projectile.direction) {
          case "W":
            projectile.hitBox.setY(projectile.hitBox.getY() - projectile.speed);
            break;
          case "A":
            projectile.hitBox.setX(projectile.hitBox.getX() - projectile.speed);
            break;
          case "S":
            projectile.hitBox.setY(projectile.hitBox.getY() + projectile.speed);
            break;
          case "D":
            projectile.hitBox.setX(projectile.hitBox.getX() + projectile.speed);
            break;
        }

        if (projectile.hitBox.getBoundsInParent().intersects(target.get(i).hitBox.getBoundsInParent())) {
          projectile.collided = true;
          e = target.get(i);
        }

        projectile.hitBox.setX(projectile.hitBoxDefaultX);
        projectile.hitBox.setY(projectile.hitBoxDefaultY);
        target.get(i).hitBox.setX(target.get(i).hitBoxDefaultX);
        target.get(i).hitBox.setY(target.get(i).hitBoxDefaultY);

      }

    }

    return e;

  }


  public boolean checkPlayer(Entity entity) {

    boolean contactPlayer = false;

    entity.hitBox.setX(entity.worldX + entity.hitBox.getX());
    entity.hitBox.setY(entity.worldY + entity.hitBox.getY());

    gwc.player.hitBox.setX(gwc.player.worldX + gwc.player.hitBox.getX());
    gwc.player.hitBox.setY(gwc.player.worldY + gwc.player.hitBox.getY());

    switch (entity.direction) {
      case "W":
        entity.hitBox.setY(entity.hitBox.getY() - entity.speed);
        break;
      case "A":
        entity.hitBox.setX(entity.hitBox.getX() - entity.speed);
        break;
      case "S":
        entity.hitBox.setY(entity.hitBox.getY() + entity.speed);
        break;
      case "D":
        entity.hitBox.setX(entity.hitBox.getX() + entity.speed);
        break;
    }

    if (entity.hitBox.getBoundsInParent().intersects(gwc.player.hitBox.getBoundsInParent())) {
      entity.collided = true;
      contactPlayer = true;
    }

    entity.hitBox.setX(entity.hitBoxDefaultX);
    entity.hitBox.setY(entity.hitBoxDefaultY);
    gwc.player.hitBox.setX(gwc.player.hitBoxDefaultX);
    gwc.player.hitBox.setY(gwc.player.hitBoxDefaultY);

    return contactPlayer;

  }


  // CHECK ENTITY - WALL INTERSECTION
  public Building checkBuilding(Entity entity) {

    Building building = null;

    int entityLeftWorldX = entity.worldX + (int) entity.hitBox.getX();
    int entityRightWorldX = entity.worldX + (int) entity.hitBox.getX() + (int) entity.hitBox.getWidth();
    int entityTopWorldY = entity.worldY + (int) entity.hitBox.getY();
    int entityBottomWorldY = entity.worldY + (int) entity.hitBox.getY() + (int) entity.hitBox.getHeight();

    int entityLeftCol = entityLeftWorldX / gwc.tileSize;
    int entityRightCol = entityRightWorldX / gwc.tileSize;
    int entityTopRow = entityTopWorldY / gwc.tileSize;
    int entityBottomRow = entityBottomWorldY / gwc.tileSize;

    Building building1 = null;
    Building building2 = null;
    Building building3 = null;
    Building building4 = null;

    switch (entity.direction) {
      case "W":
        entityTopRow = (entityTopWorldY - entity.speed) / gwc.tileSize;
        building1 = gwc.buildingManager.wallManager.walls[entityLeftCol][entityTopRow];
        building2 = gwc.buildingManager.wallManager.walls[entityRightCol][entityTopRow];
        building3 = gwc.buildingManager.machineManager.machines[entityLeftCol][entityTopRow];
        building4 = gwc.buildingManager.machineManager.machines[entityRightCol][entityTopRow];
        if (building1 != null || building2 != null || building3 != null || building4 != null) {
          entity.collided = true;
        }
        break;
      case "A":
        entityLeftCol = (entityLeftWorldX - entity.speed) / gwc.tileSize;
        building1 = gwc.buildingManager.wallManager.walls[entityLeftCol][entityTopRow];
        building2 = gwc.buildingManager.wallManager.walls[entityLeftCol][entityBottomRow];
        building3 = gwc.buildingManager.machineManager.machines[entityLeftCol][entityTopRow];
        building4 = gwc.buildingManager.machineManager.machines[entityLeftCol][entityBottomRow];
        if (building1 != null || building2 != null || building3 != null || building4 != null) {
          entity.collided = true;
        }
        break;
      case "S":
        entityBottomRow = (entityBottomWorldY + entity.speed) / gwc.tileSize;
        building1 = gwc.buildingManager.wallManager.walls[entityLeftCol][entityBottomRow];
        building2 = gwc.buildingManager.wallManager.walls[entityRightCol][entityBottomRow];
        building3 = gwc.buildingManager.machineManager.machines[entityLeftCol][entityBottomRow];
        building4 = gwc.buildingManager.machineManager.machines[entityRightCol][entityBottomRow];
        if (building1 != null || building2 != null || building3 != null || building4 != null) {
          entity.collided = true;
        }
        break;
      case "D":
        entityRightCol = (entityRightWorldX + entity.speed) / gwc.tileSize;
        building1 = gwc.buildingManager.wallManager.walls[entityRightCol][entityTopRow];
        building2 = gwc.buildingManager.wallManager.walls[entityRightCol][entityBottomRow];
        building3 = gwc.buildingManager.machineManager.machines[entityRightCol][entityTopRow];
        building4 = gwc.buildingManager.machineManager.machines[entityRightCol][entityBottomRow];
        if (building1 != null || building2 != null || building3 != null || building4 != null) {
          entity.collided = true;
        }
        break;
    }

    if (building1 != null) {
      building = building1;
    }

    if (building2 != null) {
      building = building2;
    }

    if (building3 != null) {
      building = building3;
    }

    if (building4 != null) {
      building = building4;
    }

    return building;

  }


  public boolean checkInRange(Entity entity, Entity target) {
    boolean inRange = false;
    double distance = Math.sqrt(Math.pow(target.worldX - entity.worldX, 2) + Math.pow(target.worldY - entity.worldY, 2));
    if (distance < 300) {
      inRange = true;
    }
    return inRange;
  }


  public boolean checkInRange(Building machine, Entity target, double range) {
    boolean inRange = false;
    double distance = Math.sqrt(Math.pow(target.worldX - machine.worldX, 2) + Math.pow(target.worldY - machine.worldY, 2));
    if (distance < range) {
      inRange = true;
    }
    return inRange;
  }

}
