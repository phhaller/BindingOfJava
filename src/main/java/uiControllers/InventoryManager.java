package uiControllers;

import javafx.animation.TranslateTransition;
import objects.SuperObject;
import view.GameWindowController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class InventoryManager {

  private GameWindowController gwc;

  private HashMap<String, ArrayList<SuperObject>> inventoryMap;


  public InventoryManager(GameWindowController gwc) {
    this.gwc = gwc;
    setup();
  }


  private void setup() {
    inventoryMap = new HashMap<>();
  }


  // ADD
  public boolean addItem(SuperObject item) {
    boolean added = true;
    if (!inventoryMap.keySet().contains(item.name)) {
      if (inventoryMap.size() < 36) {
        ArrayList<SuperObject> tmp = new ArrayList<>();
        tmp.add(item);
        inventoryMap.put(item.name, tmp);
      } else {
        added = false;
      }
    } else {
      ArrayList<SuperObject> tmp = inventoryMap.get(item.name);
      if (tmp.size() < 100) {
        tmp.add(item);
        inventoryMap.put(item.name, tmp);
      }
    }
    return added;
  }


  // REMOVE
  public void removeItem(String item, boolean use) {
    if (inventoryMap.keySet().contains(item)) {
      if (use) {
        if (useItem(item)) {
          ArrayList<SuperObject> tmp = inventoryMap.get(item);
          if (tmp.size() - 1 <= 0) {
            inventoryMap.remove(item);
          } else {
            tmp.remove(tmp.get(0));
            inventoryMap.put(item, tmp);
          }
        }
      } else {
        ArrayList<SuperObject> tmp = inventoryMap.get(item);
        if (tmp.size() - 1 <= 0) {
          inventoryMap.remove(item);
        } else {
          tmp.remove(tmp.get(0));
          inventoryMap.put(item, tmp);
        }
      }
    }
  }


  private boolean useItem(String item) {

    boolean used = false;

    switch (item) {
      case "APPLE":
        if (gwc.player.life < gwc.player.maxLife) {
          gwc.uiManager.healthBar.heal(gwc.player.life += 5);
          used = true;
        }
        break;
      case "POTIONRED":
        if (gwc.player.life < gwc.player.maxLife) {
          gwc.uiManager.healthBar.heal(gwc.player.life += 10);
          used = true;
        }
        break;
      case "POTIONPURPLE":
        break;
      case "POTIONBLUE":
        if (gwc.uiManager.energyBar.energy < gwc.uiManager.energyBar.maxEnergy) {
          gwc.uiManager.energyBar.fill(20);
          used = true;
        }
        break;
      case "POTIONYELLOW":
        break;
      case "KEY":
        used = true;
        break;
      case "CHICKENLEG_COOKED":
      case "PORKCHOP_COOKED":
      case "RIBS_COOKED":
      case "STEAK_COOKED":
        if (gwc.player.life < gwc.player.maxLife) {
          gwc.uiManager.healthBar.heal(gwc.player.life += 20);
          used = true;
        }
        break;
    }

    return used;

  }



  // GET
  public HashMap<String, ArrayList<SuperObject>> getInventoryMap() {

    HashMap<String, ArrayList<SuperObject>> tmp = inventoryMap.entrySet()
        .stream()
        .sorted(Map.Entry.<String, ArrayList<SuperObject>>comparingByKey())
        .collect(Collectors.toMap(
            Map.Entry::getKey,
            Map.Entry::getValue,
            (oldValue, newValue) -> oldValue, LinkedHashMap::new
        ));

    return tmp;
  }


}
