package uiControllers;

import objects.SuperObject;
import view.GameWindowController;

import java.util.ArrayList;

public class EquipmentManager {

  private GameWindowController gwc;

  private ArrayList<SuperObject> tools;
  private ArrayList<SuperObject> swords;
  private ArrayList<SuperObject> bows;
  private ArrayList<SuperObject> armours;

  public SuperObject current;
  public SuperObject currentTool;
  public SuperObject currentSword;
  public SuperObject currentBow;
  public SuperObject currentArmour;


  public EquipmentManager(GameWindowController gwc) {

    this.gwc = gwc;
    setup();

  }


  private void setup() {

    tools = new ArrayList<>();
    swords = new ArrayList<>();
    bows = new ArrayList<>();
    armours = new ArrayList<>();

    current = null;
    currentTool = null;
    currentSword = null;
    currentBow = null;
    currentArmour = null;

  }


  // ADD
  public boolean addTool(SuperObject tool) {
    boolean added = true;
    if (tools.size() < 7) {
      tools.add(tool);
    } else {
      added = false;
    }
    return added;
  }

  public boolean addSword(SuperObject sword) {
    boolean added = true;
    if (swords.size() < 7) {
      swords.add(sword);
    } else {
      added = false;
    }
    return added;
  }

  public boolean addBow(SuperObject bow) {
    boolean added = true;
    if (bows.size() < 7) {
      bows.add(bow);
    } else {
      added = false;
    }
    return added;
  }

  public boolean addArmour(SuperObject armour) {
    boolean added = true;
    if (armours.size() < 7) {
      armours.add(armour);
    } else {
      added = false;
    }
    return added;
  }


  // REMOVE
  public void removeTool(SuperObject tool) {
    tools.remove(tool);
  }

  public void removeSword(SuperObject sword) {
    swords.remove(sword);
  }

  public void removeBow(SuperObject bow) {
    bows.remove(bow);
  }

  public void removeArmour(SuperObject armour) {
    armours.remove(armour);
  }


  // GET
  public ArrayList<SuperObject> getTools() {
    return tools;
  }
  public ArrayList<SuperObject> getSwords() {
    return swords;
  }
  public ArrayList<SuperObject> getBows() {
    return bows;
  }
  public ArrayList<SuperObject> getArmour() {
    return armours;
  }


  // SET CURRENT
  public void setCurrent(SuperObject object) {

    if (object == null) {
      current = null;
      gwc.player.damage = 0;
      gwc.player.dexterity = 0;
      gwc.player.defense = 0;
      gwc.player.animationManager.update("WALK", "BASIC");
    } else {
      current = object;
      gwc.player.damage = current.damage;
      gwc.player.dexterity = current.dexterity;
      gwc.player.defense = current.defense;
      if (object.type.equals("BOW")) {
        gwc.player.animationManager.update("WALK", "BASIC");
      }
      if (object.type.equals("TOOL") || object.type.equals("SWORD")) {
        gwc.player.animationManager.update("WALK", object.name);
      }
    }

  }

  public void setCurrentTool(SuperObject tool) {
    currentTool = tool;
  }

  public void setCurrentSword(SuperObject sword) {
    currentSword = sword;
  }

  public void setCurrentBow(SuperObject bow) {
    currentBow = bow;
  }

  public void setCurrentArmour(SuperObject armour) {
    currentArmour = armour;
  }

}
