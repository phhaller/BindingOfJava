package uiControllers;

import java.util.ArrayList;

public class ScrollingMessageManager {

  public ArrayList<String> messages;
  public ArrayList<Integer> messagesCounter;

  public ScrollingMessageManager() {
    messages = new ArrayList<>();
    messagesCounter = new ArrayList<>();
  }


  public void addMessage(String message) {
    if (message != null && !message.isEmpty()) {
      messages.add(message);
      messagesCounter.add(0);
    }
  }

}
