package uiControllers;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import objects.*;
import view.GameWindowController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CraftingManager {

  private GameWindowController gwc;

  private AnchorPane shovelPane;
  private AnchorPane axePane;
  private AnchorPane pickaxePane;
  private AnchorPane saberPane;
  private AnchorPane longswordPane;
  private AnchorPane potionRedPane;
  private AnchorPane potionPurplePane;
  private AnchorPane potionBluePane;
  private AnchorPane potionYellowPane;
  private AnchorPane chickenLegPane;
  private AnchorPane porkChopPane;
  private AnchorPane ribsPane;
  private AnchorPane steakPane;

  private AnchorPane wallWoodPane;
  private AnchorPane anvilPane;

  private ArrayList<AnchorPane> panes;


  public CraftingManager(GameWindowController gwc) {
    this.gwc = gwc;
    createRecipePanes();
  }


  private void createRecipePanes() {

    panes = new ArrayList<>();

    shovelPane = createPane("SHOVEL", new HashMap<String, Integer>(Map.ofEntries(Map.entry("LOG", 2), Map.entry("ROCK", 1))));
    axePane = createPane("AXE", new HashMap<String, Integer>(Map.ofEntries(Map.entry("LOG", 2), Map.entry("ROCK", 3))));
    pickaxePane = createPane("PICKAXE", new HashMap<String, Integer>(Map.ofEntries(Map.entry("LOG", 2), Map.entry("ROCK", 3))));
    saberPane = createPane("SABER", new HashMap<String, Integer>(Map.ofEntries(Map.entry("LOG", 1), Map.entry("ROCK", 3))));
    longswordPane = createPane("LONGSWORD", new HashMap<String, Integer>(Map.ofEntries(Map.entry("LOG", 1), Map.entry("ROCK", 5))));
    potionRedPane = createPane("POTIONRED", new HashMap<String, Integer>(Map.ofEntries(Map.entry("BERRYRED", 3))));
    potionPurplePane = createPane("POTIONPURPLE", new HashMap<String, Integer>(Map.ofEntries(Map.entry("BERRYPURPLE", 3))));
    potionBluePane = createPane("POTIONBLUE", new HashMap<String, Integer>(Map.ofEntries(Map.entry("BERRYBLUE", 3))));
    potionYellowPane = createPane("POTIONYELLOW", new HashMap<String, Integer>(Map.ofEntries(Map.entry("BERRYYELLOW", 3))));
    chickenLegPane = createPane("CHICKENLEG_COOKED", new HashMap<String, Integer>(Map.ofEntries(Map.entry("CHICKENLEG_RAW", 1))));
    porkChopPane = createPane("PORKCHOP_COOKED", new HashMap<String, Integer>(Map.ofEntries(Map.entry("PORKCHOP_RAW", 1))));
    ribsPane = createPane("RIBS_COOKED", new HashMap<String, Integer>(Map.ofEntries(Map.entry("RIBS_RAW", 1))));
    steakPane = createPane("STEAK_COOKED", new HashMap<String, Integer>(Map.ofEntries(Map.entry("STEAK_RAW", 1))));

    wallWoodPane = createPane("WALLWOOD", new HashMap<String, Integer>(Map.ofEntries(Map.entry("LOG", 3))));
    anvilPane = createPane("ANVIL", new HashMap<String, Integer>(Map.ofEntries(Map.entry("INGOTIRON", 5))));

  }


  private AnchorPane createPane(String result, HashMap<String, Integer> ingredients) {

    AnchorPane anchorPane = new AnchorPane();
    anchorPane.setPrefSize(200, 50);

    Rectangle resultRec = new Rectangle(48, 48);
    resultRec.setTranslateX(2);
    resultRec.setTranslateY(2);
    resultRec.getStyleClass().add("resultRec");

    switch (result) {
      case "SHOVEL":
      case "AXE":
      case "PICKAXE":
        resultRec.setFill(new ImagePattern(gwc.imageManager.toolMap.get(result).get(3)));
        break;
      case "LONGSWORD":
        resultRec.setFill(new ImagePattern(gwc.imageManager.swordMap.get("SWORD").get(2)));
        break;
      case "SABER":
        resultRec.setFill(new ImagePattern(gwc.imageManager.swordMap.get(result).get(0)));
        break;
      case "POTIONRED":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(0)));
        break;
      case "POTIONPURPLE":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(1)));
        break;
      case "POTIONBLUE":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(2)));
        break;
      case "POTIONYELLOW":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(3)));
        break;
      case "CHICKENLEG_COOKED":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("CHICKENLEG").get(1)));
        break;
      case "PORKCHOP_COOKED":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("PORKCHOP").get(1)));
        break;
      case "RIBS_COOKED":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("RIBS").get(1)));
        break;
      case "STEAK_COOKED":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("STEAK").get(1)));
        break;

    }

    Random r = new Random();
    resultRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
        int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

        switch (result) {
          case "SHOVEL":
            gwc.equipmentManager.addTool(new Shovel(tmp1, tmp2, false, gwc));
            break;
          case "AXE":
            gwc.equipmentManager.addTool(new Axe(tmp1, tmp2, false, gwc));
            break;
          case "PICKAXE":
            gwc.equipmentManager.addTool(new Pickaxe(tmp1, tmp2, false, gwc));
            break;
          case "SABER":
            gwc.equipmentManager.addSword(new Saber(tmp1, tmp2, false, gwc));
            break;
          case "LONGSWORD":
            gwc.equipmentManager.addSword(new Longsword(tmp1, tmp2, false, gwc));
            break;
          case "POTIONRED":
            gwc.inventoryManager.addItem(new Potion(tmp1, tmp2, false, "RED", gwc));
            break;
          case "POTIONPURPLE":
            gwc.inventoryManager.addItem(new Potion(tmp1, tmp2, false, "PURPLE", gwc));
            break;
          case "POTIONBLUE":
            gwc.inventoryManager.addItem(new Potion(tmp1, tmp2, false, "BLUE", gwc));
            break;
          case "POTIONYELLOW":
            gwc.inventoryManager.addItem(new Potion(tmp1, tmp2, false, "YELLOW", gwc));
            break;
          case "CHICKENLEG_COOKED":
            gwc.inventoryManager.addItem(new Meat(tmp1, tmp2, false, "CHICKENLEG", true, gwc));
            break;
          case "PORKCHOP_COOKED":
            gwc.inventoryManager.addItem(new Meat(tmp1, tmp2, false, "PORKCHOP", true, gwc));
            break;
          case "RIBS_COOKED":
            gwc.inventoryManager.addItem(new Meat(tmp1, tmp2, false, "RIBS", true, gwc));
            break;
          case "STEAK_COOKED":
            gwc.inventoryManager.addItem(new Meat(tmp1, tmp2, false, "STEAK", true, gwc));
            break;
        }

        for (Map.Entry<String, Integer> entry : ingredients.entrySet()) {
          for (int i = 0; i < entry.getValue(); i++) {
            gwc.inventoryManager.removeItem(entry.getKey(), false);
          }
        }

        gwc.scrollingMessageManager.addMessage("You crafted a " + result);
        gwc.uiManager.inventoryWindow.update();
        gwc.uiManager.craftingWindow.update();

      }
    });

    anchorPane.getChildren().add(resultRec);

    int counter = 0;
    for (String key : ingredients.keySet()) {

      Rectangle ingRec = new Rectangle(32, 32);
      ingRec.setTranslateX(100 + counter * 50);
      ingRec.setTranslateY(9);
      ingRec.getStyleClass().add("ingredientRec");

      Label amountLabel = new Label();
      amountLabel.setPrefSize(30, 20);
      amountLabel.translateXProperty().bind(ingRec.translateXProperty().add(22));
      amountLabel.translateYProperty().bind(ingRec.translateYProperty().add(22));
      amountLabel.getStyleClass().add("statsLabel");
      amountLabel.setText(String.valueOf(ingredients.get(key)));

      switch (key) {
        case "LOG":
        case "ROCK":
          ingRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get(key).get(0)));
          break;
        case "BERRYRED":
          ingRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(0)));
          break;
        case "BERRYPURPLE":
          ingRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(1)));
          break;
        case "BERRYBLUE":
          ingRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(2)));
          break;
        case "BERRYYELLOW":
          ingRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(3)));
          break;
        case "CHICKENLEG_RAW":
          ingRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("CHICKENLEG").get(0)));
          break;
        case "PORKCHOP_RAW":
          ingRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("PORKCHOP").get(0)));
          break;
        case "RIBS_RAW":
          ingRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("RIBS").get(0)));
          break;
        case "STEAK_RAW":
          ingRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("STEAK").get(0)));
          break;
      }

      anchorPane.getChildren().addAll(ingRec, amountLabel);

      counter++;

    }

    return anchorPane;

  }


  public void update(int LOGS, int ROCKS, int BERRYREDS, int BERRYPURPLES, int BERRYBLUES, int BERRYYELLOWS,
                     int CHICKENLEGS, int PORKCHOPS, int RIBS, int STEAKS) {

    panes.clear();

    if (gwc.equipmentManager.getTools().size() < 7) {

      if (LOGS >= 2 && ROCKS >= 1) {
        panes.add(shovelPane);
      }

      if (LOGS >= 2 && ROCKS >= 3) {
        panes.add(axePane);
        panes.add(pickaxePane);
      }

    }


    if (gwc.equipmentManager.getSwords().size() < 7) {

      if (LOGS >= 1 && ROCKS >= 3) {
        panes.add(saberPane);
      }

      if (LOGS >= 1 && ROCKS >= 5) {
        panes.add(longswordPane);
      }

    }


    if (BERRYREDS >= 3) {
      panes.add(potionRedPane);
    }

    if (BERRYPURPLES >= 3) {
      panes.add(potionPurplePane);
    }

    if (BERRYBLUES >= 3) {
      panes.add(potionBluePane);
    }

    if (BERRYYELLOWS >= 3) {
      panes.add(potionYellowPane);
    }


    if (CHICKENLEGS >= 1) {
      panes.add(chickenLegPane);
    }

    if (PORKCHOPS >= 1) {
      panes.add(porkChopPane);
    }

    if (RIBS >= 1) {
      panes.add(ribsPane);
    }

    if (STEAKS >= 1) {
      panes.add(steakPane);
    }


  }


  public void update(String machine, int LOGS, int ROCKS, int BERRYREDS, int BERRYPURPLES, int BERRYBLUES, int BERRYYELLOWS,
                     int CHICKENLEGS, int PORKCHOPS, int RIBS, int STEAKS) {

    panes.clear();


    //ANVIL
    if (machine.equals("ANVIL")) {

      if (gwc.equipmentManager.getTools().size() < 7) {

        if (LOGS >= 2 && ROCKS >= 1) {
          panes.add(shovelPane);
        }

        if (LOGS >= 2 && ROCKS >= 3) {
          panes.add(axePane);
          panes.add(pickaxePane);
        }

      }

      if (gwc.equipmentManager.getSwords().size() < 7) {

        if (LOGS >= 1 && ROCKS >= 3) {
          panes.add(saberPane);
        }

        if (LOGS >= 1 && ROCKS >= 5) {
          panes.add(longswordPane);
        }

      }

    }


    // LUMBER
    if (machine.equals("LUMBER")) {



    }


    // BREWER
    if (machine.equals("BREWER")) {

      if (BERRYREDS >= 3) {
        panes.add(potionRedPane);
      }

      if (BERRYPURPLES >= 3) {
        panes.add(potionPurplePane);
      }

      if (BERRYBLUES >= 3) {
        panes.add(potionBluePane);
      }

      if (BERRYYELLOWS >= 3) {
        panes.add(potionYellowPane);
      }

    }


    // CAMPFIRE
    if (machine.equals("CAMPFIRE")) {

      if (CHICKENLEGS >= 1) {
        panes.add(chickenLegPane);
      }

      if (PORKCHOPS >= 1) {
        panes.add(porkChopPane);
      }

      if (RIBS >= 1) {
        panes.add(ribsPane);
      }

      if (STEAKS >= 1) {
        panes.add(steakPane);
      }

    }

  }


  public ArrayList<AnchorPane> getPanes() {
    return panes;
  }

}
