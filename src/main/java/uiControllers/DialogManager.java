package uiControllers;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import objects.*;
import view.GameWindowController;

import java.util.ArrayList;
import java.util.Random;

public class DialogManager {

  private GameWindowController gwc;
  public boolean buying;
  public boolean selling;

  // BUY PANES
  private ArrayList<AnchorPane> buyPanes;
  private AnchorPane buyAcornPane;
  private AnchorPane buyAxePane;
  private AnchorPane buyBerryRedPane;
  private AnchorPane buyBerryPurplePane;
  private AnchorPane buyBerryBluePane;
  private AnchorPane buyBerryYellowPane;
  private AnchorPane buyBonePane;
  private AnchorPane buyBowPane;
  private AnchorPane buyFeatherPane;
  private AnchorPane buyIngotIronPane;
  private AnchorPane buyIngotGoldPane;
  private AnchorPane buyLogPane;
  private AnchorPane buyLongswordPane;
  private AnchorPane buyChickenLegPane;
  private AnchorPane buyPorkChopPane;
  private AnchorPane buyRibsPane;
  private AnchorPane buySteakPane;
  private AnchorPane buyPickaxePane;
  private AnchorPane buyPotionRedPane;
  private AnchorPane buyPotionPurplePane;
  private AnchorPane buyPotionBluePane;
  private AnchorPane buyPotionYellowPane;
  private AnchorPane buyRockPane;
  private AnchorPane buySaberPane;
  private AnchorPane buyShovelPane;
  private AnchorPane buyShroomRedPane;
  private AnchorPane buyShroomBrownPane;

  // SELL PANES
  private ArrayList<AnchorPane> sellPanes;
  private AnchorPane sellAcornPane;
  private AnchorPane sellAxePane;
  private AnchorPane sellBerryRedPane;
  private AnchorPane sellBerryPurplePane;
  private AnchorPane sellBerryBluePane;
  private AnchorPane sellBerryYellowPane;
  private AnchorPane sellBonePane;
  private AnchorPane sellBowPane;
  private AnchorPane sellFeatherPane;
  private AnchorPane sellIngotIronPane;
  private AnchorPane sellIngotGoldPane;
  private AnchorPane sellKeyPane;
  private AnchorPane sellLongswordPane;
  private AnchorPane sellChickenLegPane;
  private AnchorPane sellPorkChopPane;
  private AnchorPane sellRibsPane;
  private AnchorPane sellSteakPane;
  private AnchorPane sellPickaxePane;
  private AnchorPane sellPotionRedPane;
  private AnchorPane sellPotionPurplePane;
  private AnchorPane sellPotionBluePane;
  private AnchorPane sellPotionYellowPane;
  private AnchorPane sellSaberPane;
  private AnchorPane sellShovelPane;
  private AnchorPane sellShroomRedPane;
  private AnchorPane sellShroomBrownPane;

  private SimpleIntegerProperty acornsProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty axesProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty berryRedProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty berryPurpleProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty berryBlueProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty berryYellowProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty bonesProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty bowsProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty feathersProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty ingotIronProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty ingotGoldProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty keysProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty longswordsProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty chickenLegsProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty porkChopsProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty ribsProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty steaksProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty pickaxesProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty potionRedProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty potionPurpleProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty potionBlueProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty potionYellowProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty sabersProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty shovelsProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty shroomRedProperty = new SimpleIntegerProperty(0);
  private SimpleIntegerProperty shroomBrownProperty = new SimpleIntegerProperty(0);


  public DialogManager(GameWindowController gwc) {
    this.gwc = gwc;
    buying = true;
    selling = false;
    createRecipePanes();
  }


  private void createRecipePanes() {

    buyPanes = new ArrayList<>();
    sellPanes = new ArrayList<>();

    buyAcornPane = createBuyPane("ACORN", 2);
    buyAxePane = createBuyPane("AXE", 6);
    buyBerryRedPane = createBuyPane("BERRYRED", 2);
    buyBerryPurplePane = createBuyPane("BERRYPURPLE", 2);
    buyBerryBluePane = createBuyPane("BERRYBLUE", 2);
    buyBerryYellowPane = createBuyPane("BERRYYELLOW", 2);
    buyBonePane = createBuyPane("BONE", 2);
    buyBowPane = createBuyPane("NORMALBOW", 10);
    buyFeatherPane = createBuyPane("FEATHER", 2);
    buyIngotIronPane = createBuyPane("INGOTIRON", 4);
    buyIngotGoldPane = createBuyPane("INGOTGOLD", 4);
    buyLogPane = createBuyPane("LOG", 1);
    buyLongswordPane = createBuyPane("LONGSWORD", 8);
    buyChickenLegPane = createBuyPane("CHICKENLEG", 4);
    buyPorkChopPane = createBuyPane("PORKCHOP", 4);
    buyRibsPane = createBuyPane("RIBS", 4);
    buySteakPane = createBuyPane("STEAK", 4);
    buyPickaxePane = createBuyPane("PICKAXE", 6);
    buyPotionRedPane = createBuyPane("POTIONRED", 7);
    buyPotionPurplePane = createBuyPane("POTIONPURPLE", 7);
    buyPotionBluePane = createBuyPane("POTIONBLUE", 7);
    buyPotionYellowPane = createBuyPane("POTIONYELLOW", 7);
    buyRockPane = createBuyPane("ROCK", 1);
    buySaberPane = createBuyPane("SABER", 6);
    buyShovelPane = createBuyPane("SHOVEL", 4);
    buyShroomRedPane = createBuyPane("SHROOMRED", 2);
    buyShroomBrownPane = createBuyPane("SHROOMBROWN", 2);


    sellAcornPane = createSellPane("ACORN", 1);
    sellAxePane = createSellPane("AXE", 3);
    sellBerryRedPane = createSellPane("BERRYRED", 1);
    sellBerryPurplePane = createSellPane("BERRYPURPLE", 1);
    sellBerryBluePane = createSellPane("BERRYBLUE", 1);
    sellBerryYellowPane = createSellPane("BERRYYELLOW", 1);
    sellBonePane = createSellPane("BONE", 1);
    sellBowPane = createSellPane("NORMALBOW", 5);
    sellFeatherPane = createSellPane("FEATHER", 1);
    sellIngotIronPane = createSellPane("INGOTIRON", 2);
    sellIngotGoldPane = createSellPane("INGOTGOLD", 2);
    sellKeyPane = createSellPane("KEY", 3);
    sellLongswordPane = createSellPane("LONGSWORD", 4);
    sellChickenLegPane = createSellPane("CHICKENLEG", 2);
    sellPorkChopPane = createSellPane("PORKCHOP", 2);
    sellRibsPane = createSellPane("RIBS", 2);
    sellSteakPane = createSellPane("STEAK", 2);
    sellPickaxePane = createSellPane("PICKAXE", 3);
    sellPotionRedPane = createSellPane("POTIONRED", 3);
    sellPotionPurplePane = createSellPane("POTIONPURPLE", 3);
    sellPotionBluePane = createSellPane("POTIONBLUE", 3);
    sellPotionYellowPane = createSellPane("POTIONYELLOW", 3);
    sellSaberPane = createSellPane("SABER", 3);
    sellShovelPane = createSellPane("SHOVEL", 2);
    sellShroomRedPane = createSellPane("SHROOMRED", 1);
    sellShroomBrownPane = createSellPane("SHROOMBROWN", 1);


  }


  private AnchorPane createBuyPane(String result, int coins) {

    AnchorPane anchorPane = new AnchorPane();
    anchorPane.setPrefSize(310, 50);

    Rectangle resultRec = new Rectangle(48, 48);
    resultRec.setTranslateX(2);
    resultRec.setTranslateY(2);
    resultRec.getStyleClass().add("resultRec");

    switch (result) {
      case "ACORN":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("ACORN").get(0)));
        break;
      case "AXE":
        resultRec.setFill(new ImagePattern(gwc.imageManager.toolMap.get("AXE").get(3)));
        break;
      case "BERRYRED":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(0)));
        break;
      case "BERRYPURPLE":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(1)));
        break;
      case "BERRYBLUE":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(2)));
        break;
      case "BERRYYELLOW":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(3)));
        break;
      case "BONE":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BONE").get(0)));
        break;
      case "NORMALBOW":
        resultRec.setFill(new ImagePattern(gwc.imageManager.bowMap.get("NORMALBOW").get(0)));
        break;
      case "FEATHER":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("FEATHER").get(0)));
        break;
      case "INGOTIRON":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("INGOT").get(0)));
        break;
      case "INGOTGOLD":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("INGOT").get(1)));
        break;
      case "LOG":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("LOG").get(0)));
        break;
      case "LONGSWORD":
        resultRec.setFill(new ImagePattern(gwc.imageManager.swordMap.get("SWORD").get(2)));
        break;
      case "CHICKENLEG":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("CHICKENLEG").get(0)));
        break;
      case "PORKCHOP":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("PORKCHOP").get(0)));
        break;
      case "RIBS":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("RIBS").get(0)));
        break;
      case "STEAK":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("STEAK").get(0)));
        break;
      case "PICKAXE":
        resultRec.setFill(new ImagePattern(gwc.imageManager.toolMap.get("PICKAXE").get(3)));
        break;
      case "POTIONRED":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(0)));
        break;
      case "POTIONPURPLE":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(1)));
        break;
      case "POTIONBLUE":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(2)));
        break;
      case "POTIONYELLOW":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(3)));
        break;
      case "ROCK":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("ROCK").get(0)));
        break;
      case "SABER":
        resultRec.setFill(new ImagePattern(gwc.imageManager.swordMap.get("SABER").get(0)));
        break;
      case "SHOVEL":
        resultRec.setFill(new ImagePattern(gwc.imageManager.toolMap.get("SHOVEL").get(3)));
        break;
      case "SHROOMRED":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("SHROOM").get(0)));
        break;
      case "SHROOMBROWN":
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("SHROOM").get(1)));
        break;
    }

    Random r = new Random();
    resultRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
        int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;


        switch (result) {
          case "ACORN":
            gwc.inventoryManager.addItem(new Acorn(tmp1, tmp2, false, gwc));
            break;
          case "AXE":
            gwc.equipmentManager.addTool(new Axe(tmp1, tmp2, false, gwc));
            break;
          case "BERRYRED":
            gwc.inventoryManager.addItem(new Berry(tmp1, tmp2, false, "RED", gwc));
            break;
          case "BERRYPURPLE":
            gwc.inventoryManager.addItem(new Berry(tmp1, tmp2, false, "PURPLE", gwc));
            break;
          case "BERRYBLUE":
            gwc.inventoryManager.addItem(new Berry(tmp1, tmp2, false, "BLUE", gwc));
            break;
          case "BERRYYELLOW":
            gwc.inventoryManager.addItem(new Berry(tmp1, tmp2, false, "YELLOW", gwc));
            break;
          case "BONE":
            gwc.inventoryManager.addItem(new Bone(tmp1, tmp2, false, gwc));
            break;
          case "NORMALBOW":
            gwc.equipmentManager.addBow(new Bow(tmp1, tmp2, false, gwc));
            break;
          case "FEATHER":
            gwc.inventoryManager.addItem(new Feather(tmp1, tmp2, false, gwc));
            break;
          case "INGOTIRON":
            gwc.inventoryManager.addItem(new Ingot(tmp1, tmp2, false, "IRON", gwc));
            break;
          case "INGOTGOLD":
            gwc.inventoryManager.addItem(new Ingot(tmp1, tmp2, false, "GOLD", gwc));
            break;
          case "LOG":
            gwc.inventoryManager.addItem(new Log(tmp1, tmp2, false, gwc));
            break;
          case "LONGSWORD":
            gwc.equipmentManager.addSword(new Longsword(tmp1, tmp2, false, gwc));
            break;
          case "CHICKENLEG":
            gwc.inventoryManager.addItem(new Meat(tmp1, tmp2, false, "CHICKENLEG", false, gwc));
            break;
          case "PORKCHOP":
            gwc.inventoryManager.addItem(new Meat(tmp1, tmp2, false, "PORKCHOP", false, gwc));
            break;
          case "RIBS":
            gwc.inventoryManager.addItem(new Meat(tmp1, tmp2, false, "RIBS", false, gwc));
            break;
          case "STEAK":
            gwc.inventoryManager.addItem(new Meat(tmp1, tmp2, false, "STEAK", false, gwc));
            break;
          case "PICKAXE":
            gwc.equipmentManager.addTool(new Pickaxe(tmp1, tmp2, false, gwc));
            break;
          case "POTIONRED":
            gwc.inventoryManager.addItem(new Potion(tmp1, tmp2, false, "RED", gwc));
            break;
          case "POTIONPURPLE":
            gwc.inventoryManager.addItem(new Potion(tmp1, tmp2, false, "PURPLE", gwc));
            break;
          case "POTIONBLUE":
            gwc.inventoryManager.addItem(new Potion(tmp1, tmp2, false, "BLUE", gwc));
            break;
          case "POTIONYELLOW":
            gwc.inventoryManager.addItem(new Potion(tmp1, tmp2, false, "YELLOW", gwc));
            break;
          case "ROCK":
            gwc.inventoryManager.addItem(new Rock(tmp1, tmp2, false, gwc));
            break;
          case "SABER":
            gwc.equipmentManager.addSword(new Saber(tmp1, tmp2, false, gwc));
            break;
          case "SHOVEL":
            gwc.equipmentManager.addTool(new Shovel(tmp1, tmp2, false, gwc));
            break;
          case "SHROOMRED":
            gwc.inventoryManager.addItem(new Shroom(tmp1, tmp2, false, "RED", gwc));
            break;
          case "SHROOMBROWN":
            gwc.inventoryManager.addItem(new Shroom(tmp1, tmp2, false, "BROWN", gwc));
            break;
        }

        for (int i = 0; i < coins; i++) {
          gwc.inventoryManager.removeItem("COIN", false);
        }

        gwc.scrollingMessageManager.addMessage("You bought a " + result);
        gwc.uiManager.dialogWindow.update();

      }
    });

    anchorPane.getChildren().add(resultRec);

    Rectangle coinsRec = new Rectangle(32, 32);
    coinsRec.setTranslateX(100);
    coinsRec.setTranslateY(9);
    coinsRec.getStyleClass().add("ingredientRec");
    coinsRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("COIN").get(0)));

    Label amountLabel = new Label();
    amountLabel.setPrefSize(30, 20);
    amountLabel.translateXProperty().bind(coinsRec.translateXProperty().add(22));
    amountLabel.translateYProperty().bind(coinsRec.translateYProperty().add(22));
    amountLabel.getStyleClass().add("statsLabel");
    amountLabel.setText(String.valueOf(coins));

    anchorPane.getChildren().addAll(coinsRec, amountLabel);

    return anchorPane;

  }


  private AnchorPane createSellPane(String result, int coins) {

    AnchorPane anchorPane = new AnchorPane();
    anchorPane.setPrefSize(310, 50);

    Rectangle resultRec = new Rectangle(48, 48);
    resultRec.setTranslateX(2);
    resultRec.setTranslateY(2);
    resultRec.getStyleClass().add("resultRec");

    SimpleIntegerProperty integerProperty = null;

    switch (result) {
      case "ACORN":
        integerProperty = acornsProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("ACORN").get(0)));
        break;
      case "AXE":
        integerProperty = axesProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.toolMap.get("AXE").get(3)));
        break;
      case "BERRYRED":
        integerProperty = berryRedProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(0)));
        break;
      case "BERRYPURPLE":
        integerProperty = berryPurpleProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(1)));
        break;
      case "BERRYBLUE":
        integerProperty = berryBlueProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(2)));
        break;
      case "BERRYYELLOW":
        integerProperty = berryYellowProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BERRY").get(3)));
        break;
      case "BONE":
        integerProperty = bonesProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("BONE").get(0)));
        break;
      case "NORMALBOW":
        integerProperty = bowsProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.bowMap.get("NORMALBOW").get(0)));
        break;
      case "FEATHER":
        integerProperty = feathersProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("FEATHER").get(0)));
        break;
      case "INGOTIRON":
        integerProperty = ingotIronProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("INGOT").get(0)));
        break;
      case "INGOTGOLD":
        integerProperty = ingotGoldProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("INGOT").get(1)));
        break;
      case "KEY":
        integerProperty = keysProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("KEY").get(0)));
        break;
      case "LONGSWORD":
        integerProperty = longswordsProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.swordMap.get("SWORD").get(2)));
        break;
      case "CHICKENLEG":
        integerProperty = chickenLegsProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("CHICKENLEG").get(1)));
        break;
      case "PORKCHOP":
        integerProperty = porkChopsProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("PORKCHOP").get(1)));
        break;
      case "RIBS":
        integerProperty = ribsProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("RIBS").get(1)));
        break;
      case "STEAK":
        integerProperty = steaksProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("STEAK").get(1)));
        break;
      case "PICKAXE":
        integerProperty = pickaxesProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.toolMap.get("PICKAXE").get(3)));
        break;
      case "POTIONRED":
        integerProperty = potionRedProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(0)));
        break;
      case "POTIONPURPLE":
        integerProperty = potionPurpleProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(1)));
        break;
      case "POTIONBLUE":
        integerProperty = potionBlueProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(2)));
        break;
      case "POTIONYELLOW":
        integerProperty = potionYellowProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("POTION").get(3)));
        break;
      case "SABER":
        integerProperty = sabersProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.swordMap.get("SABER").get(0)));
        break;
      case "SHOVEL":
        integerProperty = shovelsProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.toolMap.get("SHOVEL").get(3)));
        break;
      case "SHROOMRED":
        integerProperty = shroomRedProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("SHROOM").get(0)));
        break;
      case "SHROOMBROWN":
        integerProperty = shroomBrownProperty;
        resultRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("SHROOM").get(1)));
        break;
    }

    Random r = new Random();
    resultRec.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        for (int i = 0; i < coins; i++) {

          int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
          int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

          gwc.inventoryManager.addItem(new Coin(tmp1, tmp2, false, gwc));

        }

        switch (result) {
          case "AXE":
          case "PICKAXE":
          case "SHOVEL":
            for (SuperObject object : gwc.equipmentManager.getTools()) {
              if (object.name.equals(result)) {
                if (object != gwc.equipmentManager.currentTool && object.durability == object.maxDurability) {
                  gwc.equipmentManager.removeTool(object);
                  break;
                }
              }
            }
            break;
          case "LONGSWORD":
          case "SABER":
            for (SuperObject object : gwc.equipmentManager.getSwords()) {
              if (object.name.equals(result)) {
                if (object != gwc.equipmentManager.currentSword && object.durability == object.maxDurability) {
                  gwc.equipmentManager.removeSword(object);
                  break;
                }
              }
            }
            break;
          case "NORMALBOW":
            for (SuperObject object : gwc.equipmentManager.getBows()) {
              if (object.name.equals(result)) {
                if (object != gwc.equipmentManager.currentBow && object.durability == object.maxDurability) {
                  gwc.equipmentManager.removeBow(object);
                  break;
                }
              }
            }
            break;
          case "CHICKENLEG":
          case "PORKCHOP":
          case "RIBS":
          case "STEAK":
            gwc.inventoryManager.removeItem(result + "_COOKED", false);
            break;
          default:
            gwc.inventoryManager.removeItem(result, false);
            break;
        }

        gwc.scrollingMessageManager.addMessage("You sold a " + result);
        gwc.uiManager.dialogWindow.update();

      }
    });

    anchorPane.getChildren().add(resultRec);

    Rectangle coinsRec = new Rectangle(32, 32);
    coinsRec.setTranslateX(100);
    coinsRec.setTranslateY(9);
    coinsRec.getStyleClass().add("ingredientRec");
    coinsRec.setFill(new ImagePattern(gwc.imageManager.inventoryMap.get("COIN").get(0)));

    Label amountLabel = new Label();
    amountLabel.setPrefSize(30, 20);
    amountLabel.translateXProperty().bind(coinsRec.translateXProperty().add(22));
    amountLabel.translateYProperty().bind(coinsRec.translateYProperty().add(22));
    amountLabel.getStyleClass().add("statsLabel");
    amountLabel.setText(String.valueOf(coins));

    Label inStockLabel = new Label("In stock: ");
    inStockLabel.setPrefSize(60, 30);
    inStockLabel.setTranslateX(210);
    inStockLabel.setTranslateY(10);
    inStockLabel.getStyleClass().add("dialogWindowInStockLabel");

    Label inStockAmountLabel = new Label("120");
    inStockAmountLabel.setPrefSize(30, 30);
    inStockAmountLabel.setTranslateX(270);
    inStockAmountLabel.setTranslateY(10);
    if (integerProperty != null) {
      inStockAmountLabel.textProperty().bind(Bindings.convert(integerProperty));
    }
    inStockAmountLabel.getStyleClass().add("dialogWindowInStockLabel");

    anchorPane.getChildren().addAll(coinsRec, amountLabel, inStockLabel, inStockAmountLabel);

    return anchorPane;

  }


  public void updateBuy(String npc) {

    buyPanes.clear();
    int coins = gwc.inventoryManager.getInventoryMap().containsKey("COIN") ? gwc.inventoryManager.getInventoryMap().get("COIN").size() : 0;

    // BLACKSMITH BUYING
    if (npc.equals("BLACKSMITH")) {
      if (gwc.equipmentManager.getTools().size() < 7) {
        if (coins >= 4) {
          buyPanes.add(buyShovelPane);
        }

        if (coins >= 6) {
          buyPanes.add(buyAxePane);
          buyPanes.add(buyPickaxePane);
        }
      }


      if (gwc.equipmentManager.getSwords().size() < 7) {
        if (coins >= 6) {
          buyPanes.add(buySaberPane);
        }

        if (coins >= 8) {
          buyPanes.add(buyLongswordPane);
        }
      }


      if (gwc.equipmentManager.getBows().size() < 7) {
        if (coins >= 10) {
          buyPanes.add(buyBowPane);
        }
      }
    }


    // MERCHANT BUYING
    if (npc.equals("MERCHANT")) {

      if (gwc.inventoryManager.getInventoryMap().size() < 36) {

        if (coins >= 1) {
          buyPanes.add(buyLogPane);
          buyPanes.add(buyRockPane);
        }

        if (coins >= 2) {
          buyPanes.add(buyAcornPane);
          buyPanes.add(buyBerryRedPane);
          buyPanes.add(buyBerryPurplePane);
          buyPanes.add(buyBerryBluePane);
          buyPanes.add(buyBerryYellowPane);
          buyPanes.add(buyBonePane);
          buyPanes.add(buyFeatherPane);
          buyPanes.add(buyShroomRedPane);
          buyPanes.add(buyShroomBrownPane);
        }

        if (coins >= 4) {
          buyPanes.add(buyIngotIronPane);
          buyPanes.add(buyIngotGoldPane);
          buyPanes.add(buyChickenLegPane);
          buyPanes.add(buyPorkChopPane);
          buyPanes.add(buyRibsPane);
          buyPanes.add(buySteakPane);
        }

        if (coins >= 7) {
          buyPanes.add(buyPotionRedPane);
          buyPanes.add(buyPotionPurplePane);
          buyPanes.add(buyPotionBluePane);
          buyPanes.add(buyPotionYellowPane);
        }

      }

    }


  }


  public void updateSell(String npc) {

    sellPanes.clear();

    // BLACKSMITH SELLING
    if (npc.equals("BLACKSMITH")) {

      // SET INTEGER PROPERTIES
      axesProperty.set(0);
      pickaxesProperty.set(0);
      shovelsProperty.set(0);
      longswordsProperty.set(0);
      sabersProperty.set(0);
      bowsProperty.set(0);

      for (SuperObject tool : gwc.equipmentManager.getTools()) {
        if (tool.name.equals("AXE") && tool != gwc.equipmentManager.currentTool) {
          axesProperty.set(axesProperty.getValue()+1);
        }
        if (tool.name.equals("PICKAXE") && tool != gwc.equipmentManager.currentTool) {
          pickaxesProperty.set(pickaxesProperty.getValue()+1);
        }
        if (tool.name.equals("SHOVEL") && tool != gwc.equipmentManager.currentTool) {
          shovelsProperty.set(shovelsProperty.getValue()+1);
        }
      }

      for (SuperObject sword : gwc.equipmentManager.getSwords()) {
        if (sword.name.equals("LONGSWORD") && sword != gwc.equipmentManager.currentSword) {
          longswordsProperty.set(longswordsProperty.getValue()+1);
        }
        if (sword.name.equals("SABER") && sword != gwc.equipmentManager.currentSword) {
          sabersProperty.set(sabersProperty.getValue()+1);
        }
      }

      for (SuperObject bow : gwc.equipmentManager.getBows()) {
        if (bow.name.equals("NORMALBOW") && bow != gwc.equipmentManager.currentBow) {
          bowsProperty.set(bowsProperty.getValue()+1);
        }
      }

      // ADD PANES
      for (SuperObject tool : gwc.equipmentManager.getTools()) {
        if (tool.name.equals("AXE")) {
          if (tool != gwc.equipmentManager.currentTool && tool.durability == tool.maxDurability) {
            sellPanes.add(sellAxePane);
            break;
          }
        }
      }

      for (SuperObject tool : gwc.equipmentManager.getTools()) {
        if (tool.name.equals("PICKAXE")) {
          if (tool != gwc.equipmentManager.currentTool && tool.durability == tool.maxDurability) {
            sellPanes.add(sellPickaxePane);
            break;
          }
        }
      }

      for (SuperObject tool : gwc.equipmentManager.getTools()) {
        if (tool.name.equals("SHOVEL")) {
          if (tool != gwc.equipmentManager.currentTool && tool.durability == tool.maxDurability) {
            sellPanes.add(sellShovelPane);
            break;
          }
        }
      }

      for (SuperObject sword : gwc.equipmentManager.getSwords()) {
        if (sword.name.equals("LONGSWORD")) {
          if (sword != gwc.equipmentManager.currentSword && sword.durability == sword.maxDurability) {
            sellPanes.add(sellLongswordPane);
            break;
          }
        }
      }

      for (SuperObject sword : gwc.equipmentManager.getSwords()) {
        if (sword.name.equals("SABER")) {
          if (sword != gwc.equipmentManager.currentSword && sword.durability == sword.maxDurability) {
            sellPanes.add(sellSaberPane);
            break;
          }
        }
      }

      for (SuperObject bow : gwc.equipmentManager.getBows()) {
        if (bow.name.equals("NORMALBOW")) {
          if (bow != gwc.equipmentManager.currentBow && bow.durability == bow.maxDurability) {
            sellPanes.add(sellBowPane);
            break;
          }
        }
      }
    }


    // MERCHANT BUYING
    if (npc.equals("MERCHANT")) {

      if (gwc.inventoryManager.getInventoryMap().containsKey("ACORN")) {
        acornsProperty.set(gwc.inventoryManager.getInventoryMap().get("ACORN").size());
        sellPanes.add(sellAcornPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("BERRYRED")) {
        berryRedProperty.set(gwc.inventoryManager.getInventoryMap().get("BERRYRED").size());
        sellPanes.add(sellBerryRedPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("BERRYPURPLE")) {
        berryPurpleProperty.set(gwc.inventoryManager.getInventoryMap().get("BERRYPURPLE").size());
        sellPanes.add(sellBerryPurplePane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("BERRYBLUE")) {
        berryBlueProperty.set(gwc.inventoryManager.getInventoryMap().get("BERRYBLUE").size());
        sellPanes.add(sellBerryBluePane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("BERRYYELLOW")) {
        berryYellowProperty.set(gwc.inventoryManager.getInventoryMap().get("BERRYYELLOW").size());
        sellPanes.add(sellBerryYellowPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("BONE")) {
        bonesProperty.set(gwc.inventoryManager.getInventoryMap().get("BONE").size());
        sellPanes.add(sellBonePane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("FEATHER")) {
        feathersProperty.set(gwc.inventoryManager.getInventoryMap().get("FEATHER").size());
        sellPanes.add(sellFeatherPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("INGOTIRON")) {
        ingotIronProperty.set(gwc.inventoryManager.getInventoryMap().get("INGOTIRON").size());
        sellPanes.add(sellIngotIronPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("INGOTGOLD")) {
        ingotGoldProperty.set(gwc.inventoryManager.getInventoryMap().get("INGOTGOLD").size());
        sellPanes.add(sellIngotGoldPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("KEY")) {
        keysProperty.set(gwc.inventoryManager.getInventoryMap().get("KEY").size());
        sellPanes.add(sellKeyPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("CHICKENLEG_COOKED")) {
        chickenLegsProperty.set(gwc.inventoryManager.getInventoryMap().get("CHICKENLEG_COOKED").size());
        sellPanes.add(sellChickenLegPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("PORKCHOP_COOKED")) {
        porkChopsProperty.set(gwc.inventoryManager.getInventoryMap().get("PORKCHOP_COOKED").size());
        sellPanes.add(sellPorkChopPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("RIBS_COOKED")) {
        ribsProperty.set(gwc.inventoryManager.getInventoryMap().get("RIBS_COOKED").size());
        sellPanes.add(sellRibsPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("STEAK_COOKED")) {
        steaksProperty.set(gwc.inventoryManager.getInventoryMap().get("STEAK_COOKED").size());
        sellPanes.add(sellSteakPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("POTIONRED")) {
        potionRedProperty.set(gwc.inventoryManager.getInventoryMap().get("POTIONRED").size());
        sellPanes.add(sellPotionRedPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("POTIONPURPLE")) {
        potionPurpleProperty.set(gwc.inventoryManager.getInventoryMap().get("POTIONPURPLE").size());
        sellPanes.add(sellPotionPurplePane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("POTIONBLUE")) {
        potionBlueProperty.set(gwc.inventoryManager.getInventoryMap().get("POTIONBLUE").size());
        sellPanes.add(sellPotionBluePane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("POTIONYELLOW")) {
        potionYellowProperty.set(gwc.inventoryManager.getInventoryMap().get("POTIONYELLOW").size());
        sellPanes.add(sellPotionYellowPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("SHROOMRED")) {
        shroomRedProperty.set(gwc.inventoryManager.getInventoryMap().get("SHROOMRED").size());
        sellPanes.add(sellShroomRedPane);
      }

      if (gwc.inventoryManager.getInventoryMap().containsKey("SHROOMBROWN")) {
        shroomBrownProperty.set(gwc.inventoryManager.getInventoryMap().get("SHROOMBROWN").size());
        sellPanes.add(sellShroomBrownPane);
      }

    }

  }


  public ArrayList<AnchorPane> getBuyPanes() {
    return buyPanes;
  }

  public ArrayList<AnchorPane> getSellPanes() {
    return sellPanes;
  }

}
