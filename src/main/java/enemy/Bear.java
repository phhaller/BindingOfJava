package enemy;

import entity.Entity;
import entity.EntityAnimation;
import javafx.animation.Animation;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import map.Tile;
import ui.HealthBar;
import view.GameWindowController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

public class Bear extends Entity {

  private LinkedList<Tile> path = null;
  private boolean reached = true;
  private Tile next = null;

  public Bear(int x, int y, GameWindowController gwc) {
    super(gwc);

    worldX = x * gwc.tileSize;
    worldY = y * gwc.tileSize;
    name = "BEAR";
    direction = "";
    type = 2;
    speed = 2;
    maxLife = 70;
    life = maxLife;
    damage = 15;

    createBear();
    createDrops();
    createHitBox();
    createHealthBar();

  }


  private void createBear() {

    Image spriteImg = new Image("/sprites/enemy/bear_brown.png");
    ImageView imageView = new ImageView(spriteImg);

    entityAnimation = new EntityAnimation(imageView, Animation.INDEFINITE, 48, 48, 3, 600);
    entityAnimation.setTranslateX(worldX);
    entityAnimation.setTranslateY(worldY);

  }


  private void createDrops() {

    entityDrops = new ArrayList<>();
    entityDrops.add("STEAK");
    entityDrops.add("BONE");

  }


  private void createHitBox() {

    hitBox.setX(8);
    hitBox.setY(8);
    hitBox.setWidth(32);
    hitBox.setHeight(32);
    hitBoxDefaultX = (int) hitBox.getX();
    hitBoxDefaultY = (int) hitBox.getY();

  }


  private void createHealthBar() {

    healthBar = new HealthBar(gwc, worldX, worldY - 20, 48, 12, maxLife, false);
    healthBar.setOpacity(0);

  }


  public void setAction() {

    if (gwc.collisionManager.checkInRange(this, gwc.player)) {
      actionLockCounter++;

      if (actionLockCounter >= 60) {
        path = gwc.pathfindingManager.findPath(this, gwc.player);
        reached = true;
        actionLockCounter = 0;
      }

      if (path != null && !path.isEmpty()) {
        if (reached) {

          reached = false;
          next = path.removeFirst();

        } else {

          if (next != null) {
            if (next.getY() * gwc.tileSize < worldY) {
              direction = "W";
              entityAnimation.animation.play();
              entityAnimation.animation.setOffsetY(144);
            } else if (next.getX() * gwc.tileSize < worldX) {
              direction = "A";
              entityAnimation.animation.play();
              entityAnimation.animation.setOffsetY(48);
            } else if (next.getY() * gwc.tileSize > worldY) {
              direction = "S";
              entityAnimation.animation.play();
              entityAnimation.animation.setOffsetY(0);
            } else if (next.getX() * gwc.tileSize > worldX) {
              direction = "D";
              entityAnimation.animation.play();
              entityAnimation.animation.setOffsetY(96);
            }

            switch (direction) {
              case "W":
                if (worldY <= next.getY() * gwc.tileSize) {
                  worldY = next.getY() * gwc.tileSize;
                  reached = true;
                }
                break;
              case "A":
                if (worldX <= next.getX() * gwc.tileSize) {
                  worldX = next.getX() * gwc.tileSize;
                  reached = true;
                }
                break;
              case "S":
                if (worldY >= next.getY() * gwc.tileSize) {
                  worldY = next.getY() * gwc.tileSize;
                  reached = true;
                }
                break;
              case "D":
                if (worldX >= next.getX() * gwc.tileSize) {
                  worldX = next.getX() * gwc.tileSize;
                  reached = true;
                }
                break;
            }

          }

        }
      }

    } else {
      actionLockCounter++;

      if (actionLockCounter >= 100) {

        Random r = new Random();
        int i = r.nextInt(110) + 1;

        if (i <= 25) {
          direction = "W";
          entityAnimation.animation.play();
          entityAnimation.animation.setOffsetY(144);
        } else if (i <= 50) {
          direction = "A";
          entityAnimation.animation.play();
          entityAnimation.animation.setOffsetY(48);
        } else if (i <= 75) {
          direction = "S";
          entityAnimation.animation.play();
          entityAnimation.animation.setOffsetY(0);
        } else if (i <= 100) {
          direction = "D";
          entityAnimation.animation.play();
          entityAnimation.animation.setOffsetY(96);
        }

        actionLockCounter = 0;

      }
    }

  }


  public void damageReaction() {

    actionLockCounter = 0;
    switch (gwc.player.direction) {
      case "W":
        direction = "S";
        entityAnimation.animation.setOffsetY(0);
        break;
      case "A":
        direction = "D";
        entityAnimation.animation.setOffsetY(96);
        break;
      case "S":
        direction = "W";
        entityAnimation.animation.setOffsetY(144);
        break;
      case "D":
        direction = "A";
        entityAnimation.animation.setOffsetY(48);
        break;
    }

  }

}
