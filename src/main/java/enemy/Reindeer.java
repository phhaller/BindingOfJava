package enemy;

import entity.Entity;
import entity.EntityAnimation;
import javafx.animation.Animation;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ui.HealthBar;
import view.GameWindowController;

import java.util.ArrayList;
import java.util.Random;

public class Reindeer extends Entity {

  public Reindeer(int x, int y, GameWindowController gwc) {
    super(gwc);

    worldX = x * gwc.tileSize;
    worldY = y * gwc.tileSize;
    name = "REINDEER";
    direction = "";
    type = 2;
    speed = 1;
    maxLife = 60;
    life = maxLife;
    damage = 10;

    createReindeer();
    createDrops();
    createHitBox();
    createHealthBar();

  }


  private void createReindeer() {

    Image spriteImg = new Image("/sprites/enemy/reindeer_grey.png");
    ImageView imageView = new ImageView(spriteImg);

    entityAnimation = new EntityAnimation(imageView, Animation.INDEFINITE, 48, 56, 3, 500);
    entityAnimation.setTranslateX(worldX);
    entityAnimation.setTranslateY(worldY);

  }


  private void createDrops() {

    entityDrops = new ArrayList<>();
    entityDrops.add("RIBS");

  }


  private void createHitBox() {

    hitBox.setX(4);
    hitBox.setY(12);
    hitBox.setWidth(32);
    hitBox.setHeight(40);
    hitBoxDefaultX = (int) hitBox.getX();
    hitBoxDefaultY = (int) hitBox.getY();

  }


  private void createHealthBar() {

    healthBar = new HealthBar(gwc, worldX, worldY - 20, 48, 12, maxLife, false);
    healthBar.setOpacity(0);

  }


  public void setAction() {

    actionLockCounter++;

    if (actionLockCounter >= 130) {

      Random r = new Random();
      int i = r.nextInt(110) + 1;

      if (i <= 25) {
        direction = "W";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(168);
      } else if (i <= 50) {
        direction = "A";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(56);
      } else if (i <= 75) {
        direction = "S";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(0);
      } else if (i <= 100) {
        direction = "D";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(112);
      } else {
        direction = "";
        entityAnimation.animation.stop();
      }

      actionLockCounter = 0;

    }

  }


  public void damageReaction() {

    actionLockCounter = 0;
    direction = gwc.player.direction;
    switch (direction) {
      case "W":
        entityAnimation.animation.setOffsetY(168);
        break;
      case "A":
        entityAnimation.animation.setOffsetY(56);
        break;
      case "S":
        entityAnimation.animation.setOffsetY(0);
        break;
      case "D":
        entityAnimation.animation.setOffsetY(112);
        break;
    }

    entityAnimation.animation.play();

  }

}
