package enemy;

import entity.Entity;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;
import map.Tile;
import view.GameWindowController;

import java.util.ArrayList;
import java.util.Random;

public class EnemyManager {

  GameWindowController gwc;
  public ArrayList<Entity> enemies;


  public EnemyManager(GameWindowController gwc) {

    this.gwc = gwc;
    createEnemies();

  }


  private void createEnemies() {

    enemies = new ArrayList<>();
    Tile[][] tiles = gwc.tileManager.getTiles();

    int squirrelCounter = 0;
    int foxCounter = 0;
    int goatCounter = 0;
    int reindeerCounter = 0;
    int bearCounter = 0;
    int boarCounter = 0;
    int bisonCounter = 0;
    int sheepCounter = 0;
    int roosterCounter = 0;

    Random r = new Random();

    // Create Squirrels
    while (squirrelCounter < 4) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Squirrel(tmp1, tmp2, gwc);
        enemies.add(entity);
        squirrelCounter++;
      }

    }

    // Create Foxes
    while (foxCounter < 5) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Fox(tmp1, tmp2, gwc);
        enemies.add(entity);
        foxCounter++;
      }

    }

    // Create Goats
    while (goatCounter < 2) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Goat(tmp1, tmp2, gwc);
        enemies.add(entity);
        goatCounter++;
      }

    }

    // Create Reindeers
    while (reindeerCounter < 4) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Reindeer(tmp1, tmp2, gwc);
        enemies.add(entity);
        reindeerCounter++;
      }

    }

    // Create Bears
    while (bearCounter < 3) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Bear(tmp1, tmp2, gwc);
        enemies.add(entity);
        bearCounter++;
      }

    }

    // Create Boars
    while (boarCounter < 3) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Boar(tmp1, tmp2, gwc);
        enemies.add(entity);
        boarCounter++;
      }

    }

    // Create Bisons
    while (bisonCounter < 2) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Bison(tmp1, tmp2, gwc);
        enemies.add(entity);
        bisonCounter++;
      }

    }

    // Create Sheep
    while (sheepCounter < 3) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Sheep(tmp1, tmp2, gwc);
        enemies.add(entity);
        sheepCounter++;
      }

    }

    // Create Roosters
    while (roosterCounter < 5) {

      int tmp1 = r.nextInt((gwc.worldWidth / gwc.tileSize) - 2) + 1;
      int tmp2 = r.nextInt((gwc.worldHeight / gwc.tileSize) - 2) + 1;

      if (!tiles[tmp1][tmp2].getOccupied()) {
        Entity entity = new Rooster(tmp1, tmp2, gwc);
        enemies.add(entity);
        roosterCounter++;
      }

    }

  }


  public void kill(Entity entity) {

    FadeTransition ft = new FadeTransition();
    ft.setDuration(Duration.millis(800));
    ft.setFromValue(1);
    ft.setToValue(0);
    ft.setCycleCount(1);
    ft.setAutoReverse(false);
    ft.setNode(entity.entityAnimation);

    ft.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        enemies.remove(entity);
        entity.entityAnimation.animation.stop();
        gwc.getBackground().getChildren().removeAll(entity.entityAnimation, entity.healthBar);
        gwc.objectsManager.getRandomMobDrops(entity.worldX, entity.worldY, entity.entityDrops);
      }
    });

    ft.play();

  }


  public void update() {
    for (Entity entity : enemies) {
      entity.update();
    }
  }


  public void draw() {
    for (Entity entity : enemies) {
      entity.draw();
    }
  }


}
