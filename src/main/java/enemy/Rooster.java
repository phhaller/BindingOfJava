package enemy;

import entity.Entity;
import entity.EntityAnimation;
import javafx.animation.Animation;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ui.HealthBar;
import view.GameWindowController;

import java.util.ArrayList;
import java.util.Random;

public class Rooster extends Entity {

  public Rooster(int x, int y, GameWindowController gwc) {
    super(gwc);

    worldX = x * gwc.tileSize;
    worldY = y * gwc.tileSize;
    name = "ROOSTER";
    direction = "";
    type = 2;
    speed = 1;
    maxLife = 30;
    life = maxLife;
    damage = 5;

    createRooster();
    createDrops();
    createHitBox();
    createHealthBar();

  }


  private void createRooster() {

    Image spriteImg = new Image("/sprites/enemy/rooster_white.png");
    ImageView imageView = new ImageView(spriteImg);

    entityAnimation = new EntityAnimation(imageView, Animation.INDEFINITE, 48, 48, 3, 600);
    entityAnimation.setTranslateX(worldX);
    entityAnimation.setTranslateY(worldY);

  }


  private void createDrops() {

    entityDrops = new ArrayList<>();
    entityDrops.add("CHICKENLEG");
    entityDrops.add("FEATHER");

  }


  private void createHitBox() {

    hitBox.setX(8);
    hitBox.setY(10);
    hitBox.setWidth(16);
    hitBox.setHeight(24);
    hitBoxDefaultX = (int) hitBox.getX();
    hitBoxDefaultY = (int) hitBox.getY();

  }


  private void createHealthBar() {

    healthBar = new HealthBar(gwc, worldX, worldY - 20, 48, 12, maxLife, false);
    healthBar.setOpacity(0);

  }


  public void setAction() {

    actionLockCounter++;

    if (actionLockCounter >= 130) {

      Random r = new Random();
      int i = r.nextInt(110) + 1;

      if (i <= 25) {
        direction = "W";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(144);
      } else if (i <= 50) {
        direction = "A";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(48);
      } else if (i <= 75) {
        direction = "S";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(0);
      } else if (i <= 100) {
        direction = "D";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(96);
      } else {
        direction = "";
        entityAnimation.animation.stop();
      }

      actionLockCounter = 0;

    }

  }


  public void damageReaction() {

    actionLockCounter = 0;
    direction = gwc.player.direction;
    switch (direction) {
      case "W":
        entityAnimation.animation.setOffsetY(144);
        break;
      case "A":
        entityAnimation.animation.setOffsetY(48);
        break;
      case "S":
        entityAnimation.animation.setOffsetY(0);
        break;
      case "D":
        entityAnimation.animation.setOffsetY(96);
        break;
    }

    entityAnimation.animation.play();

  }

}
