package enemy;

import entity.Entity;
import entity.EntityAnimation;
import javafx.animation.Animation;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ui.HealthBar;
import view.GameWindowController;

import java.util.ArrayList;
import java.util.Random;

public class Boar extends Entity {

  public Boar(int x, int y, GameWindowController gwc) {
    super(gwc);

    worldX = x * gwc.tileSize;
    worldY = y * gwc.tileSize;
    name = "BOAR";
    direction = "";
    type = 2;
    speed = 2;
    maxLife = 70;
    life = maxLife;
    damage = 10;

    createBoar();
    createDrops();
    createHitBox();
    createHealthBar();

  }


  private void createBoar() {

    Image spriteImg = new Image("/sprites/enemy/boar_black.png");
    ImageView imageView = new ImageView(spriteImg);

    entityAnimation = new EntityAnimation(imageView, Animation.INDEFINITE, 48, 48, 3, 600);
    entityAnimation.setTranslateX(worldX);
    entityAnimation.setTranslateY(worldY);

  }


  private void createDrops() {

    entityDrops = new ArrayList<>();
    entityDrops.add("PORKCHOP");

  }


  private void createHitBox() {

    hitBox.setX(0);
    hitBox.setY(4);
    hitBox.setWidth(32);
    hitBox.setHeight(32);
    hitBoxDefaultX = (int) hitBox.getX();
    hitBoxDefaultY = (int) hitBox.getY();

  }


  private void createHealthBar() {

    healthBar = new HealthBar(gwc, worldX, worldY - 20, 48, 12, maxLife, false);
    healthBar.setOpacity(0);

  }


  public void setAction() {

    actionLockCounter++;

    if (actionLockCounter >= 130) {

      Random r = new Random();
      int i = r.nextInt(110) + 1;

      if (i <= 25) {
        direction = "W";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(144);
      } else if (i <= 50) {
        direction = "A";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(48);
      } else if (i <= 75) {
        direction = "S";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(0);
      } else if (i <= 100) {
        direction = "D";
        entityAnimation.animation.play();
        entityAnimation.animation.setOffsetY(96);
      } else {
        direction = "";
        entityAnimation.animation.stop();
      }

      actionLockCounter = 0;

    }

  }


  public void damageReaction() {

    actionLockCounter = 0;
    switch (gwc.player.direction) {
      case "W":
        direction = "S";
        entityAnimation.animation.setOffsetY(0);
        break;
      case "A":
        direction = "D";
        entityAnimation.animation.setOffsetY(96);
        break;
      case "S":
        direction = "W";
        entityAnimation.animation.setOffsetY(144);
        break;
      case "D":
        direction = "A";
        entityAnimation.animation.setOffsetY(48);
        break;
    }

    entityAnimation.animation.play();

  }

}
