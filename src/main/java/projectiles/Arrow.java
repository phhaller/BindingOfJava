package projectiles;

import javafx.scene.shape.Rectangle;
import view.GameWindowController;

public class Arrow extends Projectile {


  public Arrow(int x, int y, String direction, int damage, String name, GameWindowController gwc) {
    super(gwc);

    worldX = x;
    worldY = y;
    type = "ARROW";
    speed = 8;
    this.name = name;
    this.direction = direction;
    this.damage = damage;

    createArrow();
    createHitBox();

  }


  private void createArrow() {

    projectileRec = new Rectangle(24, 24);

    switch (direction) {
      case "W":
        projectileRec.setRotate(0);
        worldX += 20;
        break;
      case "A":
        projectileRec.setRotate(270);
        worldY += 10;
        break;
      case "S":
        projectileRec.setRotate(180);
        worldX += 20;
        break;
      case "D":
        projectileRec.setRotate(90);
        worldX += 50;
        worldY += 10;
        break;
    }

    projectileRec.setTranslateX(worldX);
    projectileRec.setTranslateY(worldY);
    projectileRec.getStyleClass().add(name);

  }


  private void createHitBox() {

    hitBox.setX(0);
    hitBox.setY(0);
    hitBox.setWidth(24);
    hitBox.setHeight(24);
    hitBoxDefaultX = (int) hitBox.getX();
    hitBoxDefaultY = (int) hitBox.getY();

  }


  public void collisionReaction() {



  }



}
