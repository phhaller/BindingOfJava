package projectiles;

import view.GameWindowController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ProjectileManager {

  GameWindowController gwc;
  public List<Projectile> projectiles;


  public ProjectileManager(GameWindowController gwc) {
    this.gwc = gwc;
    projectiles = new ArrayList<>();
  }


  public void createProjectile(String name, int x, int y, String direction, int damage) {

    switch (name) {

      case "ARROW":
        Arrow arrow = new Arrow(x, y, direction, damage, "arrowRich", gwc);
        projectiles.add(arrow);
        break;

    }

  }


  public void update() {
    for (Iterator<Projectile> iterator = projectiles.iterator(); iterator.hasNext();) {
      Projectile projectile = iterator.next();
      if (projectile.flying) {
        projectile.update();
      } else {
        iterator.remove();
        gwc.getBackground().getChildren().remove(projectile.projectileRec);
      }
    }
  }


  public void draw() {
    for (Projectile projectile : projectiles) {
      projectile.draw();
    }
  }

}
