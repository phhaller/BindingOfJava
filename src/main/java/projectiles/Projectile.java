package projectiles;

import entity.Entity;
import javafx.scene.shape.Rectangle;
import view.GameWindowController;

public class Projectile {

  public GameWindowController gwc;
  public int worldX, worldY;
  public Rectangle projectileRec;

  // CHARACTER ATTRIBUTES
  public String direction = "W";
  public String type;
  public String name;
  public int speed;
  public int damage;

  // HITBOX
  public Rectangle hitBox;
  public int hitBoxDefaultX, hitBoxDefaultY;

  // COUNTER
  public int flyingCounter = 0;

  // BOOLEAN
  public boolean collided = false;
  public boolean flying = true;


  public Projectile(GameWindowController gwc) {
    this.gwc = gwc;
    hitBox = new Rectangle(0, 0, gwc.tileSize, gwc.tileSize);
  }

  public void collisionReaction() {}

  public void update() {

    collided = false;
    gwc.collisionManager.checkWorldBorder(this);
    Entity enemy = gwc.collisionManager.checkEntity(this, gwc.enemyManager.enemies);
    gwc.player.damageEnemy(enemy);
    gwc.collisionManager.checkVegetation(this);

    if (!collided) {
      if (flying) {

        switch (direction) {
          case "W":
            worldY -= speed;
            break;
          case "A":
            worldX -= speed;
            break;
          case "S":
            worldY += speed;
            break;
          case "D":
            worldX += speed;
            break;
        }

      }
    } else {
      flying = false;
    }

    if (flying) {
      flyingCounter++;
      if (flyingCounter > 120) {
        flying = false;
      }
    }

  }


  public void draw() {

    gwc.getBackground().getChildren().remove(projectileRec);

    int screenX = worldX - gwc.player.worldX + gwc.player.screenX;
    int screenY = worldY - gwc.player.worldY + gwc.player.screenY;
    int tileSize = gwc.tileSize;

    if (worldX + tileSize > gwc.player.worldX - gwc.player.screenX &&
        worldX - tileSize < gwc.player.worldX + gwc.player.screenX &&
        worldY + tileSize > gwc.player.worldY - gwc.player.screenY &&
        worldY - tileSize * 2 < gwc.player.worldY + gwc.player.screenY) {

      projectileRec.setTranslateX(screenX);
      projectileRec.setTranslateY(screenY);

      gwc.getBackground().getChildren().add(projectileRec);

    }

  }

}
